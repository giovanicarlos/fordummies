﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class ItemPedido
    {
        public int ItemPedidoId { get; set; }
        public decimal Valor { get; set; }

        public int PedidoId { get; set; }
        public virtual Pedido Pedido { get; set; }

        public int CursoId { get; set; }
        public Curso Curso { get; set; }

        public ItemPedido() { }

        public ItemPedido(decimal valor, Pedido pedido, int cursoId)
        {
            this.Valor = valor;
            this.Pedido = pedido;
            this.CursoId = cursoId;
        }
    }
}
