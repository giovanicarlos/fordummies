﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class Pedido
    {
        public int PedidoId { get; set; }

        public int AlunoId { get; set; }
        public virtual Aluno Aluno { get; set; }

        public virtual ICollection<ItemPedido> Itens { get; set; }

        public byte Tipo { get; set; }
        public virtual enmTipoPedido enmTipo
        {
            get { return (enmTipoPedido)Status; }
            set { Status = (byte)value; }
        }

        public DateTime DataCadastro { get; set; }

        public byte Status { get; set; }
        public virtual enmStatusPedido enmStatus
        {
            get { return (enmStatusPedido)Status; }
            set { Status = (byte)value; }
        }

        public DateTime? DataPagamento { get; set; }

        public DateTime? DataCancelamento { get; set; }

        public Pedido()
        {
            this.Itens = new List<ItemPedido>();
            this.enmStatus = enmStatusPedido.AguardandoPagamento;
            this.DataCadastro = DateTime.Now;
        }

        public Pedido(int alunoId, enmTipoPedido tipoPedido)
            : this()
        {
            this.AlunoId = alunoId;
            this.enmTipo = tipoPedido;
        }

        public void AddItens(ItemPedido item)
        {
            this.Itens.Add(item);
        }
    }

    public enum enmStatusPedido
    {        
        AguardandoPagamento = 0,
        PagamentoConfirmado = 1,
        Cancelado = 2,
    }

    public enum enmTipoPedido
    {
        Assinatura = 0,
        OneShot = 1,
    }
}
