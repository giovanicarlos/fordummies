﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class Conteudo
    {
        public int ConteudoId { get; set; }
        public string Nome { get; set; }
        public int Ordem { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool ComPreview { get; set; }

        public int CursoId { get; set; }
        public virtual Curso Curso { get; set; }

        public byte Status { get; set; }
        public virtual enmStatusConteudo enmStatus
        {
            get { return (enmStatusConteudo)Status; }
            set { Status = (byte)value; }
        }

        public Conteudo()
        {
            this.DataCadastro = DateTime.Now;
        }
    }

    public enum enmStatusConteudo
    {
        Ativo = 1,
        Bloqueado = 0
    }
}
