﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public abstract class Pessoa
    {
        public int PessoaId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public DateTime DataCadastro { get; set; }
        
        public byte Status { get; set; }
        public virtual enmStatusPessoa enmStatus
        {
            get { return (enmStatusPessoa)Status; }
            set { Status = (byte)value; }
        }

        public Pessoa()
        {
            this.enmStatus = enmStatusPessoa.Ativo;
            this.DataCadastro = DateTime.Now;
        }

    }

    public enum enmStatusPessoa
    {
        Ativo = 1,
        Bloqueado = 0
    }
}
