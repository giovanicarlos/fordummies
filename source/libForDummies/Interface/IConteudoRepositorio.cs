﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio.Interface
{
    public interface IConteudoRepositorio : IBaseRepositorio<Conteudo>
    {
        IEnumerable<Conteudo> ListarConteudos(int cursoId);
    }
}
