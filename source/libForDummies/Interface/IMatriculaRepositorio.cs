﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio.Interface
{
    public interface IMatriculaRepositorio : IBaseRepositorio<Matricula>
    {
        IEnumerable<Matricula> ConsultarMatriculas(int alunoId);
        IEnumerable<Matricula> ConsultarMatriculasVisiveisCliente(int alunoId, enmStatusMatricula? statusMatricula);
        IEnumerable<Matricula> ConsultarMatriculas(int alunoId, enmStatusMatricula statusMatricula);
        IEnumerable<Matricula> ConsultarMatriculas(int[] matriculaIdCB);
    }
}
