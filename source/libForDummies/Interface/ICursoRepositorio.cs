﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio.Interface
{
    public interface ICursoRepositorio : IBaseRepositorio<Curso>
    {
        IEnumerable<Curso> ConsultarCursos(int classificacaoId, int formatoId, bool destaque, int paginaAtual, int totalRegistrosPagina);
        IEnumerable<Curso> Consultarcursos(int formatoId, bool destaque, int paginaAtual, int totalRegistrosPagina);
        IEnumerable<Curso> Consultarcursos(bool destaque, int paginaAtual, int totalRegistrosPagina);
        IQueryable<Curso> ConsultarTop(int quantidade, int? classificacaoId);
        IEnumerable<Curso> ConsultarCursos(string textoBusca, int? formato, int paginaAtual, int totalRegistrosPagina, out int total);
        Curso Consultar(int matriculaId);
    }
}
