﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class Aluno : Pessoa
    {
        public string Codigo { get; set; }

        public virtual ICollection<Pedido> Pedidos { get; set; }
        public virtual ICollection<Matricula> Matriculas { get; set; }

        public Aluno()
        {
            this.Pedidos = new List<Pedido>();
            this.Matriculas = new List<Matricula>();
        }
    }
}
