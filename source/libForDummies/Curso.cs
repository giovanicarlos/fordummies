﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class Curso
    {
        public int CursoId { get; set; }
        
        public int CodigoCursoCB { get; set; }
        public string Nome { get; set; }
        public string CargaHoraria { get; set; }
        public string Descricao { get; set; }
        public bool Destaque { get; set; } 
        public string Requisitos { get; set; }
        public string UrlSeo { get; set; }
        public string AuthorSeo { get; set; }
        public string H1Seo { get; set; }
        public string H2Seo { get; set; }
        public string MetadescriptionSeo { get; set; }
        public string MetakeywordsSeo { get; set; }
        public DateTime DataAtivacao { get; set; }
        public DateTime? DataBloqueio { get; set; }
        public int? PrazoExpiracao { get; set; }
        public int QuantidadePreview { get; set; }
        public decimal Valor { get; set; }
        public string Idioma { get; set; }
        public bool PermiteVendaAssinatura { get; set; }
        public bool PermiteVendaOneShot { get; set; }

        public int FormatoId { get; set; }
        public virtual Formato Formato { get; set; }

        public byte Status { get; set; }
        public virtual enmStatusCurso enmStatusCurso
        {
            get { return (enmStatusCurso)Status; }
            set { Status = (byte)value; }
        }

        public virtual ICollection<Conteudo> Conteudo { get; set; }

        public virtual ICollection<Classificacao> Classificacoes { get; set; }

        public Curso() { }

        public Curso(int cursoId)
        {
            this.CursoId = cursoId;
        }
    }

    public enum enmStatusCurso
    {
        Ativo = 1,
        Bloqueado = 0
    }
}
