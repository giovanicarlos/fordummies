﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class Classificacao
    {
        public int ClassificacaoId { get; set; }

        public string Nome { get; set; }
        public string TitleSeo { get; set; }
        public string MetadescriptionSeo { get; set; }
        public string MetakeywordsSeo { get; set; }
        public string UrlSeo { get; set; }
        public string AuthorSeo { get; set; }
        public string H1Seo { get; set; }
        public string H2Seo { get; set; }
        public string BreveDescricao { get; set; }

        public virtual ICollection<Curso> Cursos { get; set; }

        public byte Status { get; set; }
        public virtual enmStatusClassificacao enmStatusClassificacao
        {
            get { return (enmStatusClassificacao)Status; }
            set { Status = (byte)value; }
        }
    }

    public enum enmStatusClassificacao
    {
        Ativo = 1,
        Bloqueado = 0
    }
}
