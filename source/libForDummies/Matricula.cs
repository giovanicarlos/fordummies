﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class Matricula
    {
        public int MatriculaId { get; set; }
        public int MatriculaIdCB { get; set; }

        public int AlunoId { get; set; }
        public virtual Aluno Aluno { get; set; }
        public int AlunoIdCB { get; set; }

        public decimal PorcentagemConclusao { get; set; }
        public bool Aprovado { get; set; }
        public bool CertificadoDisponivel { get; set; }
        public string UrlCertificadoCB { get; set; }
        public string UrlAvaCB { get; set; }
        public string UrlPesquisaCB { get; set; }
        public bool PesquisaRespondida { get; set; }

        public DateTime? DataExpiracao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime? DataInicioCurso { get; set; }

        public byte Status { get; set; }
        public virtual enmStatusMatricula enmStatus
        {
            get { return (enmStatusMatricula)Status; }
            set { Status = (byte)value; }
        }

        public int CursoId { get; set; }
        public virtual Curso Curso { get; set; }

        public int ItemPedidoId { get; set; }
        public virtual ItemPedido ItemPedido { get; set; }

        public Matricula() { }

        public Matricula(Aluno aluno, Curso curso, ItemPedido itemPedido)
        {
            this.Aluno = aluno;
            this.Curso = curso;
            this.ItemPedido = itemPedido;
            this.DataCadastro = DateTime.Now;
            this.enmStatus = enmStatusMatricula.PendenteCriarCresca;
        }

        public void AtualizarMatricula(int matriculaCBId, int alunoIdCB, string urlAva, string urlCertificado, string urlPesquisa)
        {
            this.UrlAvaCB = urlAva;
            this.UrlCertificadoCB = urlCertificado;
            this.enmStatus = enmStatusMatricula.EmAndamento;
            this.DataInicioCurso = DateTime.Now;
            if (this.Curso.PrazoExpiracao.HasValue)
                this.DataExpiracao = DateTime.Now.AddDays(this.Curso.PrazoExpiracao.Value);
            this.MatriculaIdCB = matriculaCBId;
            this.AlunoIdCB = alunoIdCB;
            this.UrlPesquisaCB = urlPesquisa;
            this.PesquisaRespondida = false;
        }

        public int TempoRestante()
        {
            if (this.DataExpiracao.HasValue)
            {
                if (this.DataExpiracao.Value > DateTime.Now)
                {
                    return this.DataExpiracao.Value.Subtract(DateTime.Now.Date).Days;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        
    }

    public enum enmStatusMatricula
    {
        Bloqueado = 0,
        EmAndamento = 1,
        Concluido = 2,
        Expirado = 3,
        Cancelado = 4,
        PendenteCriarCresca = 5,
    }
}
