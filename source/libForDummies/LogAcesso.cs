﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class LogAcesso
    {
        public int LogAcessoId { get; set; }

        public int AlunoId { get; set; }
        public virtual Aluno Aluno { get; set; }

        public DateTime DataLogin { get; set; }
        public DateTime? DataLogoff { get; set; }
        public string Ip { get; set; }

        public LogAcesso()
        {

        }

        public LogAcesso(int alunoId, DateTime dataLogin, string ip)
        {
            this.AlunoId = alunoId;
            this.DataLogin = dataLogin;
            this.Ip = ip;
        }
    }
}
