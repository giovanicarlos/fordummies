﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Dominio
{
    public class ItemPedidoOneShot : ItemPedido
    {
        public int CursoId { get; set; }
        public Curso Curso { get; set; }
    }
}
