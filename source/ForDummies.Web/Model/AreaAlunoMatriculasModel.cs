﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ForDummies.Dominio;

namespace WebELojaForDummies.Model
{
    public class AreaAlunoMatriculasModel
    {
        public List<Matricula> Matriculas { get; set; }

        public AreaAlunoMatriculasModel()
        {
            this.Matriculas = new List<Matricula>();
        }
    }
}