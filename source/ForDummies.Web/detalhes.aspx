<%@ Page Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="detalhes.aspx.cs" Inherits="ForDummies.Web.detalhes" %>
<%@ Register Src="UserControls/ucMatriculeSe.ascx" TagName="ucMatriculeSe" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <%--<asp:Literal ID="ltr_ogtitle" runat="server"></asp:Literal>
    <asp:Literal ID="ltr_ogimage" runat="server"></asp:Literal>
    <asp:Literal ID="ltr_ogurl" runat="server"></asp:Literal>
    <asp:Literal ID="ltr_ogdescription" runat="server"></asp:Literal>
    <meta property="og:type" content="article" />
    <meta property="og:site_name" content="uol" />
    <meta property="fb:admins" content="61" />--%>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function sucesso() { jQuery.blockUI({ message: jQuery('#divSucesso'), css: { color: '#ccc', background: 'none', width: '350px', border: 'none'} }); jQuery("#divSucesso").parent().appendTo(jQuery("form:first")) }
        function erro() { jQuery.blockUI({ message: jQuery('#divErro'), css: { color: '#ccc', background: 'none', width: '350px'} }) }
        function fechar() { jQuery.unblockUI(); }
    </script>
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
        <div class="breadcrumb espaco-topo">
            <a href="/">Home</a> <span class="barra">/</span> <asp:HyperLink ID="hlkAreaClassificacao" runat="server"></asp:HyperLink> <span class="barra">/</span> <asp:Label ID="lblNomeCurso" runat="server" Text="Label"></asp:Label>
        </div>
        <div class="caixaCurso">
            <div class="informacao-curso">

                <asp:Panel ID="pnlAcabou" runat="server" CssClass="pnlAcabou" Visible="false">
                    Voc� j� escolheu um curso esse m�s
                    <img src="/imagens/alert-curso.png" align="absmiddle" />
                </asp:Panel>

                <div id="fotos" runat="server">
                    <div class="imagens-curso">
                        <asp:Repeater ID="rptImagemConteudo" runat="server" 
                            onitemdatabound="rptImagemConteudo_ItemDataBound">
                            <HeaderTemplate>
                                <div class="slider-wrapper theme-default">
                                    <div id="slider" class="nivoSlider">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Image ID="imgCurso" CssClassclass="img-conteudo-curso" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                    </div>
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>                
                    </div>
                </div>

                <h1><asp:Literal ID="ltrNomeCurso" runat="server"></asp:Literal></h1>                <asp:Literal ID="ltrAudioCurso" runat="server"></asp:Literal>
                <div class="block-full"><p class="sub-titulo">CARGA HOR�RIA: <asp:Literal ID="ltrDuracao" runat="server"></asp:Literal></p></div>
                <div>
                    <asp:Literal ID="ltrDescricao" runat="server"></asp:Literal>
                </div>
                <p><asp:ImageButton ID="btnAssinante" Visible="false" CssClass="botao-assine" ImageUrl="/imagens/botao-plano-de-ja-assinei.png" runat="server" OnClick="btnAssinante_Click" /></p>
            </div>

            <div class="informacao-curso espaco-topo">
                <uc1:ucMatriculeSe ID="ucMatriculeSe" runat="server" />
                <div class="block-full"><p class="sub-titulo">PROGRAMA DO CURSO:</p></div>
                <div class="block-full">
                    <asp:DataList ID="dtlConteudos" RepeatColumns="2" Width="100%" RepeatDirection="Vertical"
                        runat="server" OnItemDataBound="dtlConteudos_ItemDataBound">
                        <ItemTemplate>
                            <asp:Literal ID="ltrTitulo" runat="server"></asp:Literal>
                        </ItemTemplate>
                    </asp:DataList>                
                </div>
                <div class="block-full"><p class="sub-titulo">REQUISITOS:</p></div>
                <asp:Literal ID="ltrRequisitos" runat="server"></asp:Literal>            
            </div>
        </div>

        <div class="campos2">
            <input id="dest" type="hidden" value="CONTENT" name="dest" />
        </div>

        <div class="cursos-relacionados espaco-topo"><div class="aba">CURSOS RELACIONADOS</div></div>
        <asp:Panel ID="BoxTopCursos" runat="server" CssClass="veja-tambem-top-cursos ui-corner-all">
            <div class="icone-veja-tambem">
            </div>
            <div class="container-cursos">
                <asp:DataList ID="dtlCursos" runat="server" RepeatDirection="Horizontal" RepeatColumns="4"
                    OnItemDataBound="dtlCursos_ItemDataBound" RepeatLayout="Flow">
                    <ItemStyle CssClass="container-curso-rodape" />
                    <ItemTemplate>
                        <a class="curso" href='../<%#Eval("UrlFormatoCursoSeo") %>'>
                        <asp:Image runat="server" ID="imgCurso" width="230" height="140" alt='<%#Eval("Nome") %>' />
                        </a>
                        <asp:Image ID="imgAudioCurso" Visible="false" ImageUrl="/imagens/audio-curso.png" runat="server" />
                        <div class="container-nomecurso-rodape"><a class="nome-curso-rodape" href='../<%#Eval("UrlFormatoCursoSeo") %>'><%#Eval("Nome") %></a></div>
                        <a class="curso" href='../<%#Eval("UrlSeo") %>'><asp:Image ID="immBotaoCurso" runat="server" /></a>
                        
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <div class="separador-outros-cursos"></div>
                    </SeparatorTemplate>
                </asp:DataList>
            </div>
        </asp:Panel>
    </div>
    </div>


    <div id="divSucesso">
        <div class="tBorda" style="width:478px;">
            <div class="E">
            </div>
            <div class="M" style="width:460px;">
            </div>
            <div class="D">
            </div>
        </div>
        <div class="cPadrao Centro" style="width:460px; padding-bottom:20px;">
            <div class="F">
                <a href="javascript:fechar();" class="fechar">x</a></div>
            <div>
                Voc� selecionou o curso<h2 style="margin:0; font-size:16px;">�<asp:Literal ID="ltrNomeCursomodal" runat="server"></asp:Literal>�</h2><br />
                <asp:Image CssClass="imagem-curso" ID="ImageCursoModal" runat="server" /><br /><br />
                Se o t�tulo estiver correto clique em �Confirmar�. <br />Para escolher outro curso clique
                em �Voltar�.</div>
            <div style="display: block; margin-top: 10px;">
                <asp:Button ID="btnMatriCular" Width="88" runat="server" Text="CONFIRMAR" OnClick="btnMatriCular_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="Button4" style="width: 88px;" type="button" value=" VOLTAR " onclick="fechar();" />
            </div>
        </div>
        <div class="rBorda" style="width:478px;">
            <div class="E">
            </div>
            <div class="M" style="width:460px;">
            </div>
            <div class="D">
            </div>
        </div>
    </div>
    <div id="divErro">
        <div class="tBorda">
            <div class="E">
            </div>
            <div class="M">
            </div>
            <div class="D">
            </div>
        </div>
        <div class="cPadrao Centro">
            <div class="F">
                <a href="javascript:fechar();" class="fechar">x</a></div>
            <div>
                Os cursos dispon�veis para este per�odo j� foram selecionados.</div>
            <div style="display: block; margin-top: 10px;">
                <input id="Button2" style="width: 25px;" type="button" value="OK" onclick="fechar();" />
            </div>
        </div>
        <div class="rBorda">
            <div class="E">
            </div>
            <div class="M">
            </div>
            <div class="D">
            </div>
        </div>
    </div>
</asp:Content>
