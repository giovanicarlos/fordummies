using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using libUOL.Interface;
using ForDummies.Dominio;
using ForDummies.Web.Provider;
using ForDummies.BL.Interface;
using Ninject;
using ForDummies.BL;

namespace ForDummies.Web.entrar
{
    public partial class Default : OpenIdDummiesPage
    {
        [Inject]
        public IUOLService uolService { get; set; }

        RetornoOpenId retorno;

        protected new void Page_Load(object sender, EventArgs e)
        {
            retorno = base.VerificaAutenticacaoOpenId();

            if (retorno != null)
            {
                if (retorno.Sucesso)
                {
                    Autenticar(retorno.Identificador);
                }                
            }
            base.EntrarComOpenId();
        }

        protected void Autenticar(string codProfile)
        {
            if (!string.IsNullOrEmpty(codProfile))
            {
                Aluno aluno = null;

                if (uolService.CadastrarUsuario(codProfile, out aluno))
                {
                    MembershipForDummiesProvider provider = new MembershipForDummiesProvider();
                    if (provider.ValidateUser(aluno.Email, aluno.Senha))
                        FormsAuthentication.SetAuthCookie(aluno.PessoaId.ToString(), !string.IsNullOrEmpty(Request.QueryString["remember"]));
                    else
                        Response.Redirect("/");
                    
                    if (!string.IsNullOrEmpty(Request.QueryString["curso"]) && !string.IsNullOrEmpty(Request.QueryString["venda"]))
                        Response.Redirect("/entrar/Matricular.aspx?curso=" + Request.QueryString["curso"] + "&venda=" + Request.QueryString["venda"]);
                }
                else
                {
                    Response.Redirect("/");
                }

                Response.Redirect("/");
            }
            Response.Redirect("/");
        }      
        
    }
}