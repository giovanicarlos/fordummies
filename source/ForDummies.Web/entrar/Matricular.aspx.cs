using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using ForDummies.Dominio;
using Ninject;
using ForDummies.BL.Interface;
using libUOL.Interface;

namespace ForDummies.Web.entrar
{
    public partial class Matricular : System.Web.UI.Page
    {

        [Inject]
        public ICursoServico cursoServico { get; set; }

        [Inject]
        public IUOLService uolService { get; set; }

        [Inject]
        public IPedidoServico pedidoServico { get; set; }

        [Inject]
        public IMatriculaServico matriculaServico { get; set; }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["curso"]) && !string.IsNullOrEmpty(Request.QueryString["sistema"]) && !string.IsNullOrEmpty(Request.QueryString["venda"]))
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("aluno"))
                {
                    Curso curso = cursoServico.GetById(Convert.ToInt32(Request.QueryString["curso"]));
                    if (curso != null)
                    {
                        TipoVendaUol tipoVenda = (TipoVendaUol)Convert.ToInt32(Request.QueryString["venda"]);
                        if (tipoVenda == TipoVendaUol.Assinatura)
                        {
                            int alunoId = Convert.ToInt32(Page.User.Identity.Name);

                            if (uolService.PossuiAssinatura() && matriculaServico.PermiteMatricular(alunoId, curso.FormatoId))
                            {
                                Pedido pedido = pedidoServico.CriarPedidoAssinatura(alunoId, curso.CursoId);
                                if (pedido != null)
                                {
                                    if (matriculaServico.Matricular(pedido.PedidoId))
                                    {
                                        Response.Redirect("/#painel");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Session["erro"] = true;
            Response.Redirect("/");
        }
    }

    public enum TipoVendaUol
    {
        OneShot = 1,
        Assinatura = 2
    }
}
