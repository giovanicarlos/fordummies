﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="assine.aspx.cs" Inherits="ForDummies.Web.assine" %>
<%@ Register Src="UserControls/ucMatriculeSe.ascx" TagName="ucMatriculeSe" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
            <div class="container-assine">
                <h1>COMO ASSINAR O <br />UOL FOR DUMMIES?</h1>

                <p>Aderindo ao nosso plano de assinatura, <span class="b">você pode escolher um curso por mês entre nossas opções</span> <span class="bi">For Dummies.</span></p>                <p>A partir daí, voce tem <span class="b">30 dias para aproveitar o conteúdo.</span> Depois desse prazo, você escolhe um novo curso e                o processo recomeça.</p>                 <p>Para estudar, você só precisa de um computador conectado                à Internet. Estude no seu ritmo, quando e onde quiser.</p>                <h3>São dezenas de cursos já disponíveis e muitos outros estão por vir.</h3>            </div>
            <div class="fr espaco-topo">
                <uc1:ucMatriculeSe ID="ucMatriculeSe" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
