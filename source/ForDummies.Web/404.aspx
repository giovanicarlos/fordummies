<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="ForDummies.Web._04" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>UOL For Dummies</title>
    <link href="/css/styles-errors.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="http://h.imguol.com/favicon.ico" />
    <meta name="robots" content="noindex,nofollow" />

<script type="text/javascript" src="http://jsuol.com/g/jquery/1.5.2/jquery.js"></script>

<style type="text/css">
	#content .txt{
		margin-top: 0px;
	}
</style>

</head>

<body id="versaoc">
	 <div id="geral">
     
        <div id="header">
			<div id="logo"></div>
        </div>
     
        <div id="content">	
			
			<div class="icon">
				<img src="http://ferr.i.uol.com.br/cursosonline/images/icon_erro.jpg" />
			</div>
			<div class="txt">
				<h1>P�gina n�o encontrada</h1>
				<h2>Esta p�gina n�o pode ser encontrada.</h2>
				<a href="/">VOLTAR PARA A HOME</a>
			</div>	
        	
        </div>
        
    <div id="footer">
	
		<div id="foot">
			<div><img src="http://seg.i.uol.com.br/images/rodape_logouol.jpg" alt="" width="37" height="12" /></div>
			<div><span>&nbsp;&nbsp;&copy; 1996-<%=DateTime.Now.Year %> UOL - O melhor conte&uacute;do. Todos os direitos reservados</span></div>
			<br class="clear" />
		</div>
	
	</div>
	
  </div>

  
<script type="text/javascript">

    var END_OF_INPUT = -1;
    var base64Chars = new Array(
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
);
    var reverseBase64Chars = new Array();
    for (var i = 0; i < base64Chars.length; i++) {
        reverseBase64Chars[base64Chars[i]] = i;
    }
    var base64Str;
    var base64Count;
    function setBase64Str(str) {
        base64Str = str;
        base64Count = 0;
    }
    function readBase64() {
        if (!base64Str) return END_OF_INPUT;
        if (base64Count >= base64Str.length) return END_OF_INPUT;
        var c = base64Str.charCodeAt(base64Count) & 0xff;
        base64Count++;
        return c;
    }
    function encodeBase64(str) {
        setBase64Str(str);
        var result = '';
        var inBuffer = new Array(3);
        var lineCount = 0;
        var done = false;
        while (!done && (inBuffer[0] = readBase64()) != END_OF_INPUT) {
            inBuffer[1] = readBase64();
            inBuffer[2] = readBase64();
            result += (base64Chars[inBuffer[0] >> 2]);
            if (inBuffer[1] != END_OF_INPUT) {
                result += (base64Chars[((inBuffer[0] << 4) & 0x30) | (inBuffer[1] >> 4)]);
                if (inBuffer[2] != END_OF_INPUT) {
                    result += (base64Chars[((inBuffer[1] << 2) & 0x3c) | (inBuffer[2] >> 6)]);
                    result += (base64Chars[inBuffer[2] & 0x3F]);
                } else {
                    result += (base64Chars[((inBuffer[1] << 2) & 0x3c)]);
                    result += ('=');
                    done = true;
                }
            } else {
                result += (base64Chars[((inBuffer[0] << 4) & 0x30)]);
                result += ('=');
                result += ('=');
                done = true;
            }
            lineCount += 4;
            if (lineCount >= 76) {
                result += ('\n');
                lineCount = 0;
            }
        }
        return result;
    }
    function readReverseBase64() {
        if (!base64Str) return END_OF_INPUT;
        while (true) {
            if (base64Count >= base64Str.length) return END_OF_INPUT;
            var nextCharacter = base64Str.charAt(base64Count);
            base64Count++;
            if (reverseBase64Chars[nextCharacter]) {
                return reverseBase64Chars[nextCharacter];
            }
            if (nextCharacter == 'A') return 0;
        }
        return END_OF_INPUT;
    }

    function ntos(n) {
        n = n.toString(16);
        if (n.length == 1) n = "0" + n;
        n = "%" + n;
        return unescape(n);
    }

    function decodeBase64(str) {
        setBase64Str(str);
        var result = "";
        var inBuffer = new Array(4);
        var done = false;
        while (!done && (inBuffer[0] = readReverseBase64()) != END_OF_INPUT
        && (inBuffer[1] = readReverseBase64()) != END_OF_INPUT) {
            inBuffer[2] = readReverseBase64();
            inBuffer[3] = readReverseBase64();
            result += ntos((((inBuffer[0] << 2) & 0xff) | inBuffer[1] >> 4));
            if (inBuffer[2] != END_OF_INPUT) {
                result += ntos((((inBuffer[1] << 4) & 0xff) | inBuffer[2] >> 2));
                if (inBuffer[3] != END_OF_INPUT) {
                    result += ntos((((inBuffer[2] << 6) & 0xff) | inBuffer[3]));
                } else {
                    done = true;
                }
            } else {
                done = true;
            }
        }
        return result;
    }

    var handleForm = function (e) {
        e.stopPropagation();
        e.preventDefault();
        var oForm = jQuery('#cadastro').get(0);
        var sUrl = oForm.action.toString();
        var aUrl = sUrl.split("?");
        var params = new Array();
        if (oForm.nome.value != "") {
            params.push('namPerson=' + escape(oForm.nome.value));
        }
        if (oForm.email.value != "") {
            params.push('namLogin=' + escape(oForm.email.value));
        }
        if (oForm.cep.value != "") {
            params.push('codPostalArea=' + escape(oForm.cep.value));
        }
        if (params.length > 0) {
            params = encodeBase64(params.join(";"));
            aUrl[1] = 'lpo=' + params + "&" + aUrl[1];
        }
        document.location.href = aUrl.join("?");
        return false;
    }

    jQuery('#cadastro').submit(function (e) { handleForm(e) });

</script>

<!-- SiteCatalyst code version: H.19.3. Copyright 1997-2009 Omniture, Inc. More info available at http://www.omniture.com -->
<script language="JavaScript" type="text/javascript" src="https://simg.uol.com.br/nocache/omtr/cursosonline.js"></script>
<script language="JavaScript" type="text/javascript"><!--
    /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
    var s_code = uol_sc.t(); if (s_code) document.write(s_code)//--></script>
<!-- End SiteCatalyst code version: H.19.3. -->

</body>
</html>