<%@ Page Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="duvidas.aspx.cs" Inherits="ForDummies.Web.duvidas" Title="D�vidas e Perguntas Frequentes - UOL Cursos Online" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
<meta name="Description" content="Veja as perguntas frequentes (FAQ) e tire suas d�vidas - UOL Cursos Online" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
	    jQuery(function() {
		    jQuery(".accordion").accordion({
			    autoHeight: false,
			    navigation: true,
			    collapsible: true,
			    active: false
		    });
	    });
	</script>
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
        <div class="interior-de-accordions">
            <h1 class="titulo-duvidas">D�VIDAS</h1>
            <h2>Os cursos For Dummies</h2>
            <div class="accordion">
                <h3><a href="#">- Quem pode realizar os cursos For Dummies?</a></h3>
                <div><p>Qualquer pessoa que deseja expandir seus conhecimentos pode realizar os cursos.</p></div>
                <h3 class="separador"><a href="#">- Os cursos s�o reconhecidos pelo MEC?</a></h3>
                <div><p>Os cursos For Dummies s�o classificados como cursos livres e, portanto, n�o precisam ser reconhecidos pelo MEC. Apenas cursos formais de gradua��o e p�s-gradua��o precisam de reconhecimento.</p></div>
                <h3 class="separador"><a href="#">- Qual � o prazo para a realiza��o dos cursos?</a></h3>
                <div><p>Ap�s realizar a assinatura do UOL For Dummies, o aluno j� pode ativar seu curso e come�ar a estudar. A partir da data de ativa��o do curso, o aluno tem 30 dias para finaliz�-lo.</p></div>
                <h3 class="separador"><a href="#">- Os cursos possuem certificado?</a></h3>
                <div><p>Sim. Ao t�rmino do curso, o aluno realiza uma avalia��o final. Atingindo o m�nimo de 70%, o aluno ter� dispon�vel um certificado digital.</p></div>
            </div>
            <h2>A assinatura</h2>
            <div class="accordion">
                <h3><a href="#">- Ao longo da assinatura, os cursos s�o acumulativos?</a></h3>
                <div><p>Sim. O aluno tem direito a realizar um curso por m�s. Caso ele fique um ou mais meses sem ativar um curso, os conte�dos se acumulam e podem ser ativados no pr�ximo acesso � �rea do Aluno.</p></div>
                <h3 class="separador"><a href="#">- Como acessar os cursos ap�s a assinatura?</a></h3>
                <div><p>Sempre que quiser visualizar seus cursos, o aluno deve acessar o site do UOL for Dummies e, na p�gina inicial, digitar o e-mail e senha cadastrados no momento da assinatura.</p></div>
            </div>
            <h2>�rea do Aluno</h2>
            <div class="accordion">
                <h3><a href="#">- O que � a �rea do Aluno?</a></h3>
                <div><p>Esta �rea � o espa�o pessoal do aluno. Nela, ele ir� encontrar seus dados, seus recados, a lista dos cursos que est� realizando, al�m de �cones de acesso aos cursos e download do conte�do do curso em PDF, quando dispon�vel.</p></div>
                <h3 class="separador"><a href="#">- Como acessar a �rea do Aluno?</a></h3>
                <div><p>Para acessar a �rea do Aluno, o aluno deve digitar seu e-mail e senha cadastrados no momento da assinatura no canto superior direito da p�gina inicial do UOL For Dummies.</p></div>
                <h3 class="separador"><a href="#"> - N�o consigo acessar a �rea do Aluno, o que devo fazer?</a></h3>
                <div>
                    <p>- Verifique se seus dados est�o sendo digitados corretamente.</p>
                    <p>- Verifique a forma como foi digitada a senha, se h� letras mai�sculas.</p>
                    <p>- Verifique se a tecla CAPS LOCK est� ativada.</p>
                </div>
            </div>
            <h2>Ambiente Virtual de Aprendizagem</h2>
            <div class="accordion">
                <h3><a href="#">- Como avan�ar e retroceder as telas do curso?</a></h3>
                <div><p>Na parte inferior das telas do curso, aluno ir� encontrar setas de navega��o, atrav�s das quais poder� avan�ar ou retroceder as p�ginas. Para esta mesma fun��o, � poss�vel utilizar tamb�m as setas localizadas nas laterais da tela de conte�do do curso.</p></div>
                <h3 class="separador"><a href="#">- Como acessar ferramentas de aprendizagem?</a></h3>
                <div><p>Os cursos possuem algumas ferramentas que auxiliam o aluno durante o estudo. Localizadas no canto direito da tela, s�o elas: atividades, biblioteca e anota��es.</p></div>
                <h3 class="separador"><a href="#">- Como fazer ANOTA��ES no Ambiente Virtual?</a></h3>
                <div><p>Logo abaixo das Ferramentas de Aprendizagem, h� um campo em branco onde o aluno pode realizar suas anota��es. � importante n�o se esquecer de salvar a anota��o depois de escrever. Para visualizar as anota��es feitas em outras p�ginas ou unidades, basta o aluno clicar em �ver todas�.</p></div>
            </div>
            <h2>Avalia��o Final e Certificado</h2>
            <div class="accordion">
                <h3><a href="#">Como funciona a avalia��o final?</a></h3>
                <div>
                    <p>Ao final do curso, o aluno deve realizar uma avalia��o. Atingindo 70% aproveitamento, o aluno consegue seu certificado de conclus�o, que fica dispon�vel na sua ��rea do Aluno�.</p>
                    <p>No entanto, para que o aluno consiga acessar a avalia��o final, � necess�rio que ele tenha conclu�do 100% do curso. Ou seja, ele dever� ter visualizado todas as telas do curso.</p>
                    <p>Os exerc�cios realizados nas unidades n�o s�o utilizados para a avalia��o do desempenho do aluno e, portanto, n�o influenciam na pontua��o da avalia��o final. Eles apenas servem como uma ferramenta de revis�o dos conte�dos.</p>
                </div>
                <h3 class="separador"><a href="#">O curso travou e n�o � poss�vel realizar a Avalia��o Final.</a></h3>
                <div>
                    <p>Isto ocorreu porque provavelmente o aluno "pulou" alguma tela ou ent�o o curso foi atualizado pelo sistema enquanto era realizado.</p>
                    <p>Algum slide do curso n�o foi visualizado e, por este motivo, a avalia��o final n�o est� dispon�vel. O aluno deve acessar sua �rea do Aluno com e-mail e senha cadastrados. Em seguida, deve clicar em �Detalhes do andamento� para identificar o slide pendente. Desta forma, o curso atingir� 100%, permitindo a realiza��o da Avalia��o Final.</p>
                </div>
                <h3 class="separador"><a href="#">N�o consegui a nota m�nima para emitir meu certificado.</a></h3>
                <div>
                    <p>Caso o aluno n�o consiga a nota m�nima para a emiss�o do certificado, o sistema disponibiliza, automaticamente, 3 novas chances para a realiza��o da prova.</p>
                    <p>Se, mesmo assim, o aluno n�o conseguir o aproveitamento m�nimo exigido, ele deve entrar em contato com a Central de Atendimento.</p>
                    <p>Capitais e regi�o metropolitana � 4003-6792<br />Demais localidades - 0800 728 2052</p>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
        </div>
    </div>
</asp:Content>
