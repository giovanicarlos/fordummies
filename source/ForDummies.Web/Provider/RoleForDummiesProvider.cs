﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using ForDummies.BL.Interface;
using Ninject;
using ForDummies.Web.App_Start;

namespace ForDummies.Web.Provider
{
    public class RoleForDummiesProvider : RoleProvider
    {

        public ILogAcessoServico logAcessoServico { get; set; }

        public RoleForDummiesProvider ()
	    {
            logAcessoServico = NinjectWebCommon.bootstrapper.Kernel.Get<ILogAcessoServico>();
	    }        

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            if (logAcessoServico.ValidaAcessoAluno())
            {
                return new string[] { "aluno" };
            }
            else
            {
                FormsAuthentication.SignOut();
                return null;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}