<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ava.aspx.cs" Inherits="ForDummies.Web.AVA.Ava" %>
<%@ Register src="../UserControls/uaAreaAluno.ascx" tagname="uaAreaAluno" tagprefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title></title>
   <!-- FONTS -->
    <link rel='stylesheet' type='text/css' href='/css/fontes/IstokWeb.css'/>
    <link rel="SHORTCUT ICON" href="http://h.imguol.com/favicon.ico" />
    <!-- /UOL -->
    <link href="/css/Styles-min.css" rel="stylesheet" type="text/css" />
    <link href="/css/ava.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="faixa-topo">
            <div id="TopoCursosUol" class="corpo-maior">
                 <a href="/"><h1 class="logo">Uol For Dummies</h1></a>
                <uc3:uaAreaAluno ID="uaAreaAluno1" runat="server" />
                <div style="clear:both;"></div>
            </div>
            <div class="BarraFixa"></div>
        </div>
        <iframe id="Iframe" runat="server" src="" scrolling="auto"></iframe>
        <div class="linha-rodape"></div>
        <div id="rodape">
            <div class="corpo-maior">            
                <div class="for-dummies"><img src="../imagens/for-dummies.png" alt=""/></div>    
            </div>
        </div>
        <div id="rodape-direitos">
            <div class="corpo-maior">
                <div class="texto-direitos">� 1996 - <%=DateTime.Now.Year.ToString() %> UOL - O melhor conte�do.  Todos os direitos reservados.</div>
            </div>
        </div>
    </form>
</body>
</html>
