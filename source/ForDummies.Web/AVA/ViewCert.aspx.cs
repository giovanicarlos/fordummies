using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ForDummies.Dominio;

namespace ForDummies.Web.AVA
{
    public partial class ViewCert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Response.Clear();
                string id = Request.QueryString["id"];
                if (string.IsNullOrEmpty(id))
                    Response.Write("Erro ao exibir certificado");
                else
                {
                    /* corrigir
                    TO.Matricula toMatricula = new TO.Matricula(Convert.ToInt32(id));
                    ExibeCertificado(toMatricula);
                     */
                }
            }
        }

        private void ExibeCertificado(Matricula toMatricula)
        {
            /* corrigir
            new BL.CertificadoMatricula().ImprimirCertificado(toMatricula);
             */
        }
    }
}
