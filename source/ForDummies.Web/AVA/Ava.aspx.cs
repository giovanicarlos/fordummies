using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ForDummies.OpenID.Security;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using Ninject;

namespace ForDummies.Web.AVA
{
    public partial class Ava : System.Web.UI.Page
    {
        [Inject]
        public IMatriculaServico matriculaServico { get; set; }

        [Inject]
        public ICursoServico cursoServico { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["m"]))
                {
                    using (Simetrica cript = new Simetrica(TipoSimetrica.Rijndael))
                    {
                        cript.Chave = ConfigurationManager.AppSettings["SecretKey"];
                        string[] valores = cript.DescriptografaQueryString(Request.QueryString["m"]).Split('|');
                        string matriculaId = valores[0];
                        string ferramenta = valores[1];
                        string url = string.Empty;
                        Matricula Matricula = matriculaServico.GetById(Convert.ToInt32(matriculaId));
                        if (Matricula != null)
                        {
                            if (ferramenta == "acessar")
                                url = Matricula.UrlAvaCB;
                            else
                                url = Matricula.UrlPesquisaCB;

                        }
                        Iframe.Attributes["src"] = url;
                    }
                    GetTitleAva();
                }
            }
        }

        private void GetTitleAva()
        {
            string id = Request.QueryString["id"];
            string qs = Request.Url.Query.Replace("id=" + id, string.Empty);
            if (!string.IsNullOrEmpty(id))
            {
                string[] parametros = null;
                using (Simetrica cript = new Simetrica(TipoSimetrica.Rijndael))
                {
                    cript.Chave = ConfigurationManager.AppSettings["SecretKey"];
                    id = cript.DescriptografaQueryString(id);
                    parametros = id.Split(new string[] { ">--CB--<" }, StringSplitOptions.None);

                    string email = string.Empty;
                    string senha = string.Empty;
                    string ferramenta = string.Empty;
                    string matricula = string.Empty;
                    string matriculaCriptografada = string.Empty;

                    if (parametros.Length == 4)
                    {
                        /* posi��es do vetor
                         * 1 = email
                         * 2 = senha
                         * 3 = matricula
                         * 4 = ferramenta  
                         */
                        matricula = cript.DescriptografaQueryString(parametros[2]);
                        ferramenta = cript.DescriptografaQueryString(parametros[3]);
                        matriculaCriptografada = parametros[2];
                    }
                    if (parametros.Length == 2)
                    {
                        /* posi��es do vetor
                         * 1 = matricula
                         * 2 = ferramenta
                         * */
                        matricula = cript.DescriptografaQueryString(parametros[0]);
                        ferramenta = cript.DescriptografaQueryString(parametros[1]);
                        matriculaCriptografada = parametros[0];
                    }

                    Matricula Matricula = matriculaServico.GetById(Convert.ToInt32(matricula));
                    if (Matricula != null)
                    {
                        Curso Curso = cursoServico.Consultar(Matricula.MatriculaId);
                        if (Curso != null)
                            Page.Title = Curso.Nome;
                        else
                            Page.Title = "AVA";
                    }
                }
            }
        }
    }
}