<%@ Page Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="como-utilizar.aspx.cs" Inherits="ForDummies.Web.como_utilizar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
            <div class="container-como-utilizar">
                <h1 class="titulo-comoutilizar">COMO UTILIZAR O UOL FOR DUMMIES?</h1>
                <div class="passo passo01">
                    <br /><br />
                    <p><strong>1- Escolha seu primeiro curso</strong></p>
                    <p>Veja se o conte�do das aulas e se a carga hor�ria atendem suas necessidades.</p>
                </div>
                <div class="passo passo02">
                    <br /><br />
                    <p><strong>2- Fa�a sua assinatura!</strong></p>
                    <p>S�o R$49,90 mensais - ou R$44,90 se voc� j� for assinante UOL</p>
                </div>
                <div class="passo passo03">
                    <br /><br />
                    <p><strong>3- Realize seu curso</strong></p>
                    <p>A cada 30 dias voc� pode escolher um novo curso, estudar no seu ritmo, quando e onde quiser.</p>
                </div>
                <div class="passo passo04">
                    <br /><br />
                    <p><strong>4- Adquira o seu certificado e escolha um novo curso!</strong></p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
