using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using Ninject;
using ForDummies.BL.Interface;
using ForDummies.Dominio;
//using libBrasilShop.Util;

namespace ForDummies.Web.UserControls
{
    public partial class ucMenu : System.Web.UI.UserControl
    {
        private bool categoriasVertical;
        
        [Inject]
        public IClassificacaoServico classificacaoServico { get; set; }

        public bool CategoriasVertical
        {
            get 
            {
                return categoriasVertical; 
            }
            set { categoriasVertical = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PreencheMenu();   
            }
        }

        private void PreencheMenu()
        {
            if (!CategoriasVertical)
            {
                dtlItensMenu.RepeatColumns = 3;
                pnlConteudo.CssClass = "classChange conteudoInterno";
                pnlTopoMenu.CssClass = "topo topo-click";
            }
            dtlItensMenu.DataSource = classificacaoServico.ConsultarClassificacoes();
            dtlItensMenu.DataBind();
        }

        protected void dtlItensMenu_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Classificacao toClassificacao = (Classificacao)e.Item.DataItem;
            toClassificacao = classificacaoServico.GetById(toClassificacao.ClassificacaoId);
            HyperLink lnkMenu = (HyperLink)e.Item.FindControl("lnkMenu");
            lnkMenu.NavigateUrl = "/categoria/" + toClassificacao.UrlSeo;
            lnkMenu.Text = toClassificacao.Nome;
            if (Request.QueryString["id"] != null  && Request.QueryString["id"] == toClassificacao.UrlSeo)
            {
                lnkMenu.CssClass = "active";
            }
            
        }


    }
}
