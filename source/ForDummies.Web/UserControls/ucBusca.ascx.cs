using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

namespace ForDummies.Web.UserControls
{
    public partial class ucBusca : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Titulo"]))
                    txtBusca.Text = Request.QueryString["Titulo"];
            }
        }

        protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("/resultado-de-busca.aspx?Titulo=" + txtBusca.Text);
        }
    }
}