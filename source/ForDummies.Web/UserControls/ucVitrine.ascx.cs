﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Ninject;
using ForDummies.BL;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Web.Helpers;

namespace ForDummies.Web.UserControls
{
    public partial class ucVitrine : System.Web.UI.UserControl
    {
        [Inject]
        public ICursoServico cursoServico { get; set; }

        [Inject]
        public IImagemHelper imagemHelper { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.User.Identity.IsAuthenticated)
                banner.Visible = false;

            CarregaCursos();
        }

        private void CarregaCursos()
        {
            rptCurso.DataSource = cursoServico.ConsultarCursos(true, 1, 9);
            rptCurso.DataBind();          
        }

        int nBorda = 0;
        protected void rptCurso_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Separator)
            {
                HtmlControl borda = (HtmlControl)e.Item.FindControl("borda");

                nBorda += 1;
                if (nBorda == 3)
                {
                    borda.Visible = true;
                    nBorda = 0;
                }
                else
                {
                    borda.Visible = false;
                }
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Curso toCurso = e.Item.DataItem as Curso;
                Image imgCurso = (Image)e.Item.FindControl("imgCurso");
                imgCurso.ImageUrl = imagemHelper.GetUrlImagemCurso(toCurso.CursoId);
            }
        }
    }
}