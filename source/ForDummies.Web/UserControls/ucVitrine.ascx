﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucVitrine.ascx.cs" Inherits="ForDummies.Web.UserControls.ucVitrine" %>
<style type="text/css">
	#inc-home-1{width: 770px;margin:0;float:left;}
	.banner a img{border:none!important;}
	#inc-home-1 .banner{overflow:hidden;width:770px;position:relative;height: 253px!important;background:url(../imagens/dummie.png) no-repeat;}
	#inc-home-1 .banner img{display:block;}            
	#inc-home-1 .banner-content{float:right;width:550px;height:247px;margin:16px 0 0 0;}
	#inc-home-1 .banner-content .texto-vitrine{float:left;width:230px;font: 30px/35px 'IstokWebBold', Arial, sans-serif;margin-top:20px;}
	#inc-home-1 .banner-content .cifrao{float:left;font-size:20px;}
	#inc-home-1 .banner-content .valor{float:left;font: 80px/80px 'IstokWebBold', Arial, sans-serif;}
	#inc-home-1 .banner-content .centavos{float:left;font: 20px/30px 'IstokWebBold', Arial, sans-serif;}
	#inc-home-1 .banner-content .assinantes{float:left;width:210px;;text-align:center; font: 11px/14px 'IstokWebBold', Arial, sans-serif;color:#414141;}
	#inc-home-1 .banner-content .video-home{float:right;width:315px;height:159px;background:#232323 url(../imagens/play-video.png) no-repeat center;margin-bottom:14px;}
	#inc-home-1 .banner-content .texto-video{float:right;width:320px;text-align:right;}
	#inc-home-1 .banner-content .btn-quero-assinar{float:right;}
	#inc-home-1 .banner .next,#inc-home-1 .banner .prev{border: none;display: none;height: 50px;width: 30px;position: absolute;top: 90px;background: none;cursor:pointer;}
	#inc-home-1 .banner .next{right:0;background: url(/css/vitrines/tvflash-seta-next.png) no-repeat 4px 0px;}
	#inc-home-1 .banner .prev{left:0;background: url(/css/vitrines/tvflash-seta-prev.png) no-repeat -9px 0px;) no-repeat -200px -200px}
	
	#inc-home-2{padding:1px;margin-top: 20px;}
	#inc-home-2 .como-funciona{float:right;width:980px;border-bottom:solid 1px #414141;margin-bottom:14px;}
	#inc-home-2 .como-funciona .aba{float:left;color:#fff200;padding:5px 15px;background:#414141;font: 11px/16px 'IstokWebRegular', Arial, sans-serif;}
	#inc-home-2 .passos{float:left;width:980px;height:200px;margin:40px 0 10px 0;background:url("/imagens/como-funciona.jpg") no-repeat top center;}
	#inc-home-2 .txt{font-size:12px;font-weight:normal!important;}
	#inc-home-2 div.a, #inc-home-2 div.b, #inc-home-2 div.c{float:left;}
	#inc-home-2 div.a .txt{width: 138px;margin:160px 13px 0px 100px;}	
	#inc-home-2 div.b .txt{width: 120px;margin:160px 35px 0px 190px;text-align:center}
	#inc-home-2 div.c .txt{width: 150px;margin:160px 10px 0px 150px;text-align:center}	
	#inc-home-2 div.c .txt span{font-size: 16px;font-weight: bold;}
	#inc-home-2 div.c .assine{width: 102px;background:#ff9d00;margin-left: 60px;margin-top: 13px;height: 24px;padding:1px;}	
	#inc-home-2 div.c .assine a{color: white;font-size: 14px;text-transform: uppercase;text-decoration: none;margin-top: 3px;display: block;margin-left: 13px;font-weight: bold;}
	.dottedTop{border-top: 1px dotted #CCC;heigth:2px;line-heigth:2px;min-heigth:2px;margin-right: 70px;}	
</style>
<div id="inc-home-1">
  <div class="banner" runat="server" id="banner">
    <div class="banner-content">
        <div class="texto-vitrine">
            <div class="texto">Nunca foi tão fácil aprender.</div>
            <div class="cifrao">R$</div>
            <div class="valor">49</div>
            <div class="centavos">,90/mês</div>
            <div class="assinantes">R$ 44,90/MÊS ASSINANTES UOL</div>
        </div>
        <div class="texto-video">
            <div class="video-home"></div>
            <a href="/entrar" class="btn-quero-assinar"><img src="../imagens/btn-quero-assinar.png" alt="Quero assinar"/></a>
        </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="lista-cursos">
    <div class="mais-vendidos"><div class="aba">MAIS VENDIDOS</div></div>
      <asp:Repeater ID="rptCurso" runat="server" OnItemDataBound="rptCurso_ItemDataBound">
        <HeaderTemplate><ul class="vitrine-home"></HeaderTemplate>
        <ItemTemplate>
            <li>
                <a href='/detalhe/<%#Eval("UrlSeo") %>'>
                    <asp:Image ID="imgCurso" BorderWidth="0" runat="server" />
                </a>
                <a href='/detalhe/<%#Eval("UrlSeo") %>' class="link-curso-texto"><%#Eval("Nome") %></a>
            </li>
        </ItemTemplate>
            <SeparatorTemplate>
                <li class="separador-linha" runat="server" id="borda"></li>
            </SeparatorTemplate>
        <FooterTemplate></ul></FooterTemplate>
      </asp:Repeater>
  </div>                 
</div>
<div style="clear:both"></div>
<script type="text/javascript">
    jQuery('div').each(function (i) {
        var x = jQuery(this).attr('class');
        if (x == "area-aluno-logado allCornerShadow") {
            jQuery('#inc-home-1 .banner').hide();
        }
    });
</script>
