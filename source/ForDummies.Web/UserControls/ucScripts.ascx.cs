﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using Ninject;

namespace ForDummies.Web.UserControls
{
    public partial class ucScripts : System.Web.UI.UserControl
    {
        [Inject]
        public IClassificacaoServico ClassificacaoServico { get; set; }
        [Inject]
        public ICursoServico CursoServico { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool permiteRoiMeter = false; //nao definiu ainda
            bool permiteOmniture = true;
            bool permiteRemarketing = true;
            inserirScript(permiteRoiMeter, permiteOmniture, permiteRemarketing);
        }

        private void inserirScript(bool RoiMeter, bool Omniture, bool Remarketing)
        {

            string Script = string.Empty;

            if (Page is Default)
            {
                if (Request.QueryString["id"] != null)
                {
                    Classificacao toClassificacao = null;

                    toClassificacao = ClassificacaoServico.Consultar(Request.QueryString["id"]);
                    if (RoiMeter)
                        Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp','55');  UOLRM.addGrouping('Categoria de Origem', 'Home/" + toClassificacao.Nome + "');             UOLRM.check(61);   </script>";
                    if (Omniture)
                        Script += scriptOmniture("categorias", toClassificacao.Nome, "categorias", toClassificacao.Nome);
                    if (Remarketing)
                        Script += scriptRemarketing("Categorias", toClassificacao.Nome);
                    Script += scriptAlturaCategoria(null);
                }
                else
                {
                    if (RoiMeter)
                        Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp', '54');  UOLRM.addGrouping('Categoria de Origem', 'Home');             UOLRM.check(61);   </script>";
                    if (Omniture)
                        Script += scriptOmniture("home", null, "home", null);
                    if (Remarketing)
                        Script += scriptRemarketing("Home", null);
                    Script += scriptAlturaCategoria("193");
                }
            }

            if (Page is detalhes)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    Curso toCurso = CursoServico.Consultar(Request.QueryString["id"]);
                    Classificacao toClassificacao = CursoServico.ConsultarClassificacao(toCurso.CursoId);
                    if (RoiMeter)
                        Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp','56');  UOLRM.addGrouping('Categoria de Origem', 'Home/" + toClassificacao.Nome + "/" + toCurso.Nome + "');             UOLRM.check(61);   </script>";
                    if (Omniture)
                        Script += scriptOmniture("categorias", toClassificacao.Nome, "cursos", toCurso.Nome);
                    if (Remarketing)
                        Script += scriptRemarketing("Cursos", toClassificacao.Nome + "/" + toCurso.Nome);
                }
            }

            if (Page is como_utilizar)
            {
                if (RoiMeter)
                    Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp','57');  UOLRM.addGrouping('Categoria de Origem', 'Home/Como utilizar');             UOLRM.check(61);   </script>";
                if (Omniture)
                    Script += Script = scriptOmniture("como utilizar", null, "card", "como utilizar");
                if (Remarketing)
                    Script += scriptRemarketing("Como utilizar", null);
            }

            if (Page is duvidas)
            {
                if (RoiMeter)
                    Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp','58');  UOLRM.addGrouping('Categoria de Origem', 'Home/Duvidas');             UOLRM.check(61);   </script>";
                if (Omniture)
                    Script += Script = scriptOmniture("duvidas", null, "card", "duvidas");
                if (Remarketing)
                    Script += scriptRemarketing("Duvidas", null);
            }

            if (Page is fale_conosco)
            {
                if (RoiMeter)
                    Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp','60');  UOLRM.addGrouping('Categoria de Origem', 'Home/Fale Conosco');             UOLRM.check(61);   </script>";
                if (Omniture)
                    Script += Script = scriptOmniture("fale conosco", null, "card", "fale conosco");
                if (Remarketing)
                    Script += scriptRemarketing("Fale conosco", null);
            }

            if (Page is resultado_de_busca)
            {
                if (RoiMeter)
                    Script += " <script type='text/javascript'>  UOLRM.addGrouping('src_pagetp','61');  UOLRM.addGrouping('Categoria de Origem', 'Home/Pagina de Busca');             UOLRM.check(61);   </script>";
                if (Omniture)
                    Script += Script = scriptOmniture("pagina de busca", null, "card", "pagina de busca");
                if (Remarketing)
                    Script += scriptRemarketing("Pagina de busca", null);
            }

            if (Page is uol_for_dummies)
            {
                if (Omniture)
                    Script += Script = scriptOmniture("o que e", null, "card", "o que e");
                if (Remarketing)
                    Script += scriptRemarketing("O que e", null);
            }

            ltrScripts.Text = Script;
        }

        private string scriptOmniture(string channel, string subchannel, string typepage, string titulo)
        {
            if (!string.IsNullOrEmpty(subchannel))
                subchannel = "'" + subchannel + "'";

            StringBuilder retorno = new StringBuilder();
            retorno.AppendLine("<!-- SiteCatalyst code version: H.22.1. Copyright 1997-2012 Omniture, Inc. More info available at http://www.omniture.com -->");
            retorno.AppendLine("<script type=\"text/javascript\">");
            retorno.AppendLine("var Config = window.config || {};");
            retorno.AppendLine("Config.Metricas = {");
            retorno.AppendLine("'channel': '" + channel + "',");
            retorno.AppendLine("'subchannel':[" + subchannel + "],");
            retorno.AppendLine("'typepage':'" + typepage + "',");
            retorno.AppendLine("'titulo': '" + titulo + "',");
            retorno.AppendLine("'testAb':''");
            retorno.AppendLine("};");
            retorno.AppendLine("</script>");
            retorno.AppendLine("<script type=\"text/javascript\" src=\"https://simg.uol.com.br/nocache/omtr/fordummies.js\"></script>");
            retorno.AppendLine("<script language=\"JavaScript\" type=\"text/javascript\"><!--");
            retorno.AppendLine("/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/");
            retorno.AppendLine("var s_code=uol_sc.t();if(s_code)document.write(s_code)//--></script>");
            retorno.AppendLine("<!-- End SiteCatalyst code version: H.22.1. -->");
            retorno.AppendLine("");
            return retorno.ToString();
        }

        private string scriptRemarketing(string pagetype, string pcat)
        {
            StringBuilder retorno = new StringBuilder();
            retorno.AppendLine("<script type=\"text/javascript\">");
            retorno.AppendLine("var google_tag_params = {");
            retorno.AppendLine("pname: 'UOL for Dummies', ");
            retorno.AppendLine("pagetype: '" + pagetype + "', ");
            if (!string.IsNullOrEmpty(pcat))
                retorno.AppendLine("pcat: '" + pcat + "'");
            retorno.AppendLine("};");
            retorno.AppendLine("</script>");
            retorno.AppendLine("<!-- Google Code for Smart_Pixel_Categoria_EAD-Emprego -->");
            retorno.AppendLine("<script type=\"text/javascript\">");
            retorno.AppendLine("/* <![CDATA[ */");
            retorno.AppendLine("var google_conversion_id = 1036145036;");
            retorno.AppendLine("var google_conversion_label = \"lNarCJLC4gMQjKOJ7gM\";");
            retorno.AppendLine("var google_custom_params = window.google_tag_params;");
            retorno.AppendLine("var google_remarketing_only = true;");
            retorno.AppendLine("/* ]]> */");
            retorno.AppendLine("</script>");
            retorno.AppendLine("<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">");
            retorno.AppendLine("</script>");
            retorno.AppendLine("<noscript>");
            retorno.AppendLine("<div style=\"display:inline;\">");
            retorno.AppendLine("<img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//googleads.g.doubleclick.net/pagead/viewthroughconversion/1036145036/?value=0&amp;label=lNarCJLC4gMQjKOJ7gM&amp;guid=ON&amp;script=0\"/>");
            retorno.AppendLine("</div>");
            retorno.AppendLine("</noscript>");
            retorno.AppendLine("");
            return retorno.ToString();
        }

        private string scriptAlturaCategoria(string acrescimo)
        {
            StringBuilder retorno = new StringBuilder();
            retorno.AppendLine("<script type=\"text/javascript\">");
            retorno.AppendLine("jQuery(window).load(function () {");
            retorno.AppendLine("var altura = jQuery(\".lista-cursos\").height();");
            if (Context.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(acrescimo))
                retorno.AppendLine("altura -= 285;");
            else
                retorno.AppendLine("altura -= 15;");
            if (!string.IsNullOrEmpty(acrescimo))
                retorno.AppendLine("altura += " + acrescimo + ";");
            retorno.AppendLine("jQuery(\".classChange\").attr(\"style\", \"min-height:\" + altura + \"px\");");
            retorno.AppendLine("})");
            retorno.AppendLine("</script>");
            retorno.AppendLine("");
            return retorno.ToString();
        }
    }
}