//------------------------------------------------------------------------------
// <gerado automaticamente>
//     O c�digo foi gerado por uma ferramenta.
//
//     As altera��es ao arquivo poder�o causar comportamento incorreto e ser�o perdidas se
//     o c�digo � regenerado. 
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace ForDummies.Web.UserControls {
    
    
    public partial class ucResultadosBusca {
        
        /// <summary>
        /// Controle ltrRegostros.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrRegostros;
        
        /// <summary>
        /// Controle pnlVisualizar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlVisualizar;
        
        /// <summary>
        /// Controle ddlTipoCurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlTipoCurso;
        
        /// <summary>
        /// Controle pnlNenhumResultado.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlNenhumResultado;
        
        /// <summary>
        /// Controle rptCursos.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rptCursos;
        
        /// <summary>
        /// Controle UcPaginacao2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declara��o do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::ForDummies.Web.UserControls.ucPaginacao UcPaginacao2;
    }
}
