﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uaAreaAluno.ascx.cs" Inherits="ForDummies.Web.UserControls.uaAreaAluno" %>
<asp:Panel ID="pnlAlunoLogado" runat="server" Visible="false" CssClass="area-aluno-logado">

    <div class="info">
        <div class="txtAluno">Área do aluno</div>
        <asp:LinkButton ID="lnkSair" runat="server" CssClass="link-sair" OnClick="lnkSair_Click">Sair</asp:LinkButton>
    </div>

    <div class="conteudo allCornerShadow">                
    <div class="espaco-direito-selecionar">
        <p>Olá <asp:Literal ID="ltrNomeAluno" runat="server" Text=""></asp:Literal>!</p>
        <p>Seja bem-vindo à sua área de cursos UOL</p>
    </div>
    </div>
</asp:Panel>



