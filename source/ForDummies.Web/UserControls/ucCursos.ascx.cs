using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Collections.Generic;
using ForDummies.Web.Helpers;
using ForDummies.BL.Interface;
using Ninject;
using ForDummies.Dominio;
using System.Linq;

namespace ForDummies.Web.UserControls
{
    public partial class ucCursos : System.Web.UI.UserControl
    {
        [Inject]
        public ICursoServico cursoServico { get; set; }

        [Inject]
        public IClassificacaoServico classificacaoServico { get; set; }

        [Inject]
        public IImagemHelper imagemHelper { get; set; }

        private int paginaAtual
        {
            get
            {
                int pagina = 1;
                if (!string.IsNullOrEmpty(Request.QueryString["p"]))
                    if (!int.TryParse(Request.QueryString["p"], out pagina))
                        pagina = 1;
                return pagina;
            }
        }
        private int registrosPorPagina = 15;
        private int total;
        private int? TipoCurso
        {
            get
            {

                if (!string.IsNullOrEmpty(Request.QueryString["t"]))
                {
                    int Tipo = 0;
                    int.TryParse(Request.QueryString["t"], out Tipo);
                    return Tipo;
                }
                return null;
            }
        }

        internal UOL _master
        {
            get { return (UOL)this.Page.Master; }
            set
            {
                UOL master = (UOL)this.Page.Master;
                master = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CarregaCursos(TipoCurso);
            }
        }

        private void VerificaPaginacao(string url)
        {
            int MaxP = (int)(this.total / this.registrosPorPagina);
            int resto;
            Math.DivRem(this.total, this.registrosPorPagina, out resto);
            if (resto > 0)
                MaxP++;

            HtmlLink linkMetaNext = new HtmlLink();
            linkMetaNext.Attributes["rel"] = "next";
            string URL = Request.Url.Host;
           
            linkMetaNext.Attributes["href"] = URL + "/" + url + "?p=" + (this.paginaAtual + 1);
            

            HtmlLink linkMetaPrev = new HtmlLink();
            linkMetaPrev.Attributes["rel"] = "prev";
           
            linkMetaPrev.Attributes["href"] = URL + "/" + url + "?p=" + (this.paginaAtual - 1);
           

            if (!string.IsNullOrEmpty(Request.QueryString["p"]))
            {
                if (Request.QueryString["p"].Equals("1"))
                {
                    HtmlLink linkMeta = new HtmlLink();
                    linkMeta.Attributes["rel"] = "canonical";
                 
                    linkMeta.Attributes["href"] = URL + "/" + url;
                   
                    Page.Header.Controls.Add(linkMeta);
                }
            }

            if (paginaAtual.Equals(1))
                Page.Header.Controls.Add(linkMetaNext);
            else if (paginaAtual >= MaxP)
                Page.Header.Controls.Add(linkMetaPrev);
            else
            {
                Page.Header.Controls.Add(linkMetaNext);
                Page.Header.Controls.Add(linkMetaPrev);
            }
        }


        private void CarregaCursos(int? tipo)
        {
            string url = "/Default.aspx?p=";
            Classificacao classificacao = null;

            if (Request.QueryString["id"] != null)
            {
                classificacao = classificacaoServico.Consultar(Request.QueryString["id"]);
                url = "/" + classificacao.UrlSeo + "?p=";
                string pagina = string.Empty;
                if (paginaAtual != 1)
                    pagina = "P�gina " + paginaAtual + " - ";
                this.Page.Title = "Cursos de " + classificacao.Nome + " Online - " + pagina + "UOL For Dummies";
                HtmlMeta HtmlMetaKeyDescription = new HtmlMeta();
                HtmlMetaKeyDescription.Content = pagina + "Cursos de " + classificacao.Nome + " Online - " + pagina + "UOL For Dummies";
                Page.Header.Controls.Add(HtmlMetaKeyDescription);

                ltrNomeClassificacao.Text = classificacao.Nome.ToUpper() + " PARA LEIGOS";
                pnlCursoTopo.Visible = true;
                pnlCursoRodape.Visible = true;
            }
            else
            {
                registrosPorPagina = 5;
            }

            int formato = 1;
            bool destaque = false;
            if (classificacao == null)
            {
                dtlCursos.DataSource = cursoServico.ConsultarCursos(formato, destaque, paginaAtual, registrosPorPagina);
                dtlCursos.DataBind();
                total = dtlCursos.Items.Count;
            }
            else
            {
                dtlCursos.DataSource = cursoServico.ConsultarCursos(classificacao.ClassificacaoId, formato, destaque, paginaAtual, registrosPorPagina);
                dtlCursos.DataBind();
                total = dtlCursos.Items.Count;
            }
           

            if (classificacao != null)
                VerificaPaginacao(classificacao.UrlSeo);

            if (dtlCursos.Items.Count == 0)
            {
                HtmlMeta linkRobots = new HtmlMeta();
                linkRobots.Name = "robots";
                linkRobots.Content = "noindex,follow";
                Page.Header.Controls.Add(linkRobots);
            }

            PreenchePaginacao(url);
        }


        int nBorda = 0;
        protected void dtlCursos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Separator)
            {
                HtmlControl borda = (HtmlControl)e.Item.FindControl("borda");

                nBorda += 1;
                if (nBorda == dtlCursos.RepeatColumns)
                {
                    borda.Visible = true;
                    nBorda = 0;
                }
                else
                {
                    borda.Visible = false;
                }
            }

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
           
                Curso toCurso = e.Item.DataItem as Curso;
                Image immBotaoCurso = (Image)e.Item.FindControl("immBotaoCurso");
                Image imgCurso = (Image)e.Item.FindControl("imgCurso");
                imgCurso.ImageUrl = imagemHelper.GetUrlImagemCurso(toCurso.CursoId);
            }
        }

        private void PreenchePaginacao(string url)
        {
            UcPaginacao2.Paginar(paginaAtual, registrosPorPagina, total, url);
        }
    }
}