using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace ForDummies.Web.UserControls
{
    public partial class ucPaginacao : System.Web.UI.UserControl
    {
        private int paginas;
        private int paginaAtual;
        private int registrosPagina;
        private int totalRegistros;
        private string url;

        public int PaginaAtual
        {
            get { return paginaAtual; }
            set { paginaAtual = value; }
        }

        public int RegistrosPagina
        {
            get { return registrosPagina; }
            set { registrosPagina = value; }
        }

        public int TotalRegistros
        {
            get { return totalRegistros; }
            set { totalRegistros = value; }
        }

        public int Paginas
        {
            get { return paginas; }
            set { paginas = value; }
        }

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        protected class Paginacao
        {
            private int pagina;
            private string texto;
            private bool ativo;
            private string classe;

            public int Pagina
            {
                get { return pagina; }
            }

            public string Texto
            {
                get { return texto; }
            }

            public bool Ativo
            {
                get { return ativo; }
            }
            public string Classe
            {
                get { return classe; }
            }

            public Paginacao(int pagina, string texto, bool ativo, string classe)
            {
                this.pagina = pagina;
                this.texto = texto;
                this.ativo = ativo;
                this.classe = classe;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void Paginar(int pagina, int tamanho, int total, string urlPagina)
        {
            this.paginaAtual = pagina;
            this.registrosPagina = tamanho;
            this.totalRegistros = total;
            this.Url = urlPagina;

            /* calculando o numero de p�ginas */
            this.paginas = (int)(this.totalRegistros / this.registrosPagina);
            int resto;
            Math.DivRem(this.totalRegistros, this.registrosPagina, out resto);
            if (resto > 0)
                this.paginas++;

            /* definindo visibilidade dos div's  */
            pnlPaginacao.Visible = this.paginas > 1;

            /* definindo link paginaAnterior */
            lnkPaginaAnterior.NavigateUrl = this.Url + (this.paginaAtual - 1).ToString();
            lnkPaginaAnterior.Enabled = this.paginaAtual > 1;
            if (!lnkPaginaAnterior.Enabled)
                lnkPaginaAnterior.CssClass = "linkdisabled";
            else
                lnkPaginaAnterior.CssClass = "link";

            /* definindo link proximaPagina */
            lnkProximaPagina.NavigateUrl = this.Url + (this.paginaAtual + 1).ToString();
            lnkProximaPagina.Enabled = this.paginaAtual < this.paginas;
            if (!lnkProximaPagina.Enabled)
                lnkProximaPagina.CssClass = "linkdisabled";
            else
                lnkProximaPagina.CssClass = "link";

            /* repeater das paginas */
            List<Paginacao> lista = new List<Paginacao>();
            for (int p = 1; p <= this.paginas; p++)
            {
                bool ativo = p != this.paginaAtual;
                string classe = string.Empty;
                if (ativo == false)
                    classe = "link-pagina-ativo";
                else
                    classe = "link-pagina";
                Paginacao pag = new Paginacao(p, p.ToString(), ativo, classe);
                lista.Add(pag);
            }
            rptPaginas.DataSource = lista;
            rptPaginas.DataBind();
            if (lista.Count > 0)
                pnlPaginacao.Visible = true;
        }

        protected void rptPaginas_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Paginacao paginacao = e.Item.DataItem as Paginacao;
                HyperLink lnkPag = (HyperLink)e.Item.FindControl("lnkPag");
                lnkPag.NavigateUrl = this.Url + paginacao.Pagina;
            }
        }
    }
}
