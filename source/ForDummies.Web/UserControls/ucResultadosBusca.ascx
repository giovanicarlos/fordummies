<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucResultadosBusca.ascx.cs" Inherits="ForDummies.Web.UserControls.ucResultadosBusca" %>
<%@ Register Src="ucPaginacao.ascx" TagName="ucPaginacao" TagPrefix="uc3" %>
<div class="topo-descrip fl">
    <p><a href="/">Home</a> <span class="barra">/</span> <h1 class="busca">Resultado de busca</h1></p>
</div>
<h3>
    <asp:Literal ID="ltrRegostros" runat="server"></asp:Literal>
    <asp:Panel ID="pnlVisualizar" runat="server" CssClass="filtros" Visible="false">
        Visualizar: 
        <asp:DropDownList ID="ddlTipoCurso" CssClass="dropbox" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoCurso_SelectedIndexChanged">
            <asp:ListItem Value="">Todos os cursos</asp:ListItem>
            <asp:ListItem Value="0">Curso Online</asp:ListItem>
            <asp:ListItem Value="1">Audiocurso</asp:ListItem>
        </asp:DropDownList>
    </asp:Panel>
</h3>
<asp:Panel ID="pnlNenhumResultado" Visible="false" CssClass="busca-dicas" runat="server">
Dicas: <br />
* Certifique-se de que todas as palavras estejam escritas corretamente <br />
* Evite abreviaturas <br />
* Evite especificar muito a sua busca <br />
</asp:Panel>
<asp:Repeater ID="rptCursos" runat="server" OnItemDataBound="rptCursos_ItemDataBound">
    <ItemTemplate>
        <div class="resultado-curso">
            <a href='/detalhe/<%#Eval("UrlSeo") %>'>
                <asp:Image ToolTip='<%#Eval("Nome") %>' ID="imgCurso" BorderWidth="0" runat="server" CssClass="imagem" />
            </a>
            <div class="fl">
                <h2>
                    <a href='/detalhe/<%#Eval("UrlSeo") %>'><%#Eval("Nome") %></a> <asp:Literal ID="ltrIdioma" runat="server"></asp:Literal>
                </h2>
                <p><%#Eval("Descricao") %></p>
            </div>
            <asp:HyperLink ID="linkBotaoCurso" runat="server"></asp:HyperLink>
        </div>
    </ItemTemplate>
</asp:Repeater>
<uc3:ucPaginacao ID="UcPaginacao2" runat="server" />
