<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPaginacao.ascx.cs" Inherits="ForDummies.Web.UserControls.ucPaginacao" %>
<asp:Panel ID="pnlPaginacao" CssClass="paginacao" runat="server" Visible="false">
    <asp:HyperLink ID="lnkPaginaAnterior" CssClass="link" runat="server"><< Anterior</asp:HyperLink>
    <asp:Repeater ID="rptPaginas" runat="server" OnItemDataBound="rptPaginas_ItemDataBound">
	    <ItemTemplate>
		    <asp:HyperLink ID="lnkPag" CssClass='<%# Eval("Classe") %>' runat="server"><%# Eval("Texto") %></asp:HyperLink>&nbsp;
	    </ItemTemplate>
    </asp:Repeater>
    <asp:HyperLink ID="lnkProximaPagina" CssClass="link" runat="server">Pr�ximo >></asp:HyperLink>
</asp:Panel>