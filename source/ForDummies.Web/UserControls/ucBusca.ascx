<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBusca.ascx.cs" Inherits="ForDummies.Web.UserControls.ucBusca" %>
<asp:Panel ID="pnlBusca" CssClass="busca-barra" runat="server" DefaultButton="btnBuscar">
    <asp:TextBox ID="txtBusca" CssClass="textbox autocomplete" placeholder="Buscar..." runat="server"></asp:TextBox>
    <asp:ImageButton ID="btnBuscar" CssClass="button" ImageUrl="/imagens/botao-buscar.png" runat="server" OnClick="btnBuscar_Click" />
</asp:Panel>
