﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMatriculeSe.ascx.cs" Inherits="ForDummies.Web.UserControls.ucMatriculeSe" %>
                <div class="form" id="form">

                    <asp:HiddenField  ID="sistema"  runat="server" />
                    <asp:HiddenField  ID="curso" runat="server"  />
                    <div class="topo">MATRICULE-SE JÁ!</div>
                    <div>
                        <div class="linha">
                            <label for="nome">Nome:</label><input name="nome" type="text" class="labels" id="nome" />
                        </div>
                        <div class="linha">
                            <label for="email">Email:</label><input name="email" type="text" class="labels" id="email" />
                        </div>
                        <div class="linha">
                            <label for="cep">CEP:</label><input name="cep" type="text" class="labels" id="cep" />
                        </div>
                        <asp:Panel ID="pnlAssineJa" runat="server" CssClass="container-assinatura">
                            <div class="plano">Plano Assinatura: <br /><span class="curso-mes">(1 curso por mês)</span></div>
                            <p class="valor-plano">R$49,90<span>/mês</span></p>
                            <p class="txt_assinante">R$44,90 para assinantes UOL</p>
                        </asp:Panel>
                        <div class="linha">
                            <input type="button" name="imageField2" id="imageField2" class="btn_hib" value="." onclick="window.location = returnDest();" />
                        </div>
                    </div>
                    <div class="linha-social">
                        <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-action="like"></div>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-text="UOL For Dummies" data-lang="pt">Tweetar</a>
                    </div>
                </div>