﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucRodape.ascx.cs" Inherits="ForDummies.Web.UserControls.ucRodape" %>
<style type="text/css">
	#rodape-social{
        float:left;
        width:100%;
        height:60px;
        background:#b9b9b9;
	}	
	#rodape-social .receba-novidades-texto{float:left;margin:12px 0 0 0;font: 14px/14px 'IstokWebRegular', Arial, sans-serif;}
	#rodape-social .container-email-novidades{float:left;margin:12px 0 0 5px;background:url(../imagens/bg-email-novidades.png) no-repeat;width:311px;height:15px;padding:10px;}
	#rodape-social .container-email-novidades input[type=text]{width:311px;height:15px;border:none;font-size:11px;}
	#rodape-social .btn-enviar-email{float:left;margin:12px 0 0 5px;cursor:pointer;}
	#rodape-social a.icn-youttube{float:left;width:23px;height:23px;background:url(../imagens/icn_youtube.png) no-repeat;margin:0 10px 0 0;}
	#rodape-social a.icn-facebook{float:left;width:23px;height:23px;background:url(../imagens/icn_facebook.png) no-repeat;margin:0 10px 0 0;}
	#rodape-social .redes{float:right;text-align:left;margin:17px 0 0 0;}
	#rodape{
        float:left;
        width:100%;
        background:#414141;
        padding:30px 0 10px 0;
        text-align:center;
	}	
	#rodape-direitos{
        float:left;
        width:100%;
        height:41px;
        background:#303030;
        text-align:center;
        padding:0;
        line-height:41px;
	}
	#rodape .cursos-categoria{float:left;margin:0 150px 0 0;}
	#rodape .cursos-categoria a.item-categoria{float:left;text-decoration:none;font: 12px/14px 'IstokWebRegular', Arial, sans-serif;color:#fff;margin:4px 46px 4px 0;}
	#rodape .titulo-rodape{font: 13px/14px 'IstokWebRegular', Arial, sans-serif;color:#fff200;margin:0 0 30px 0;text-align:left;}
	#rodape .sobre{float:left;margin:0;}
	#rodape .sobre a.item-categoria{display:block;text-decoration:none;font: 12px/14px 'IstokWebRegular', Arial, sans-serif;color:#fff;margin:7px 46px 7px 0;text-align:left;}
	#rodape .for-dummies{float:right;}
	#rodape-direitos .texto-direitos{float:right;font: 12px/41px 'IstokWebRegular', Arial, sans-serif;color:#fff;}
</style>
<div id="rodape-social">
    <div class="corpo-maior">        <div class="fl">            <div class="receba-novidades-texto">Receba novidades<br />de cursos em seu email!</div>            <div class="container-email-novidades">
                <asp:TextBox ID="txtEmail" runat="server" placeholder="Digite seu email..."></asp:TextBox>
            </div>
            <img src="../imagens/btn-enviar-email.png" border="0" alt="" id="email-news" class="btn-enviar-email" />
        </div>
        <div class="redes">
            <a href="https://www.facebook.com/uolcursosonline" class="icn-facebook"></a>
            <a href="http://www.youtube.com/cursosonlineuol" class="icn-youttube"></a>
            <a href="https://twitter.com/share" class="twitter-share-button" data-text="UOL For Dummies" data-lang="pt">Tweetar</a>
            <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-action="recommend"></div>
        </div>
    </div>
</div>
<div id="rodape">
    <div class="corpo-maior">
        <div class="cursos-categoria">
            <div class="titulo-rodape">CURSOS POR CATEGORIA</div>
            <asp:DataList ID="dtlCategorias" runat="server">
                <ItemTemplate>
                    <a href='<%#Eval("UrlSeo") %>' class="item-categoria"><%#Eval("Nome") %></a>
                </ItemTemplate>            
            </asp:DataList>
        </div>

        <div class="sobre">
            <div class="titulo-rodape">SOBRE</div>
            <ul>
                <li><a href='uol-for-dummies.aspx' class="item-categoria">O que é UOL For Dummies?</a></li>
                <li><a href='como-utilizar.aspx' class="item-categoria">Como utilizar?</a></li>
                <li><a href='duvidas.aspx' class="item-categoria">Dúvidas</a></li>
                <li><a href='fale-conosco.aspx' class="item-categoria">Fale conosco</a></li>
                <li><a href='assine.aspx' class="item-categoria">Assine</a></li>
            </ul>
        </div>
        <div class="for-dummies"><img src="../imagens/for-dummies.png" alt=""/></div>    
    </div>
</div>
<div id="rodape-direitos">
    <div class="corpo-maior">
        <div class="texto-direitos">© 1996 - <%=DateTime.Now.Year.ToString() %> UOL - O melhor conteúdo.  Todos os direitos reservados.</div>
    </div>
</div>