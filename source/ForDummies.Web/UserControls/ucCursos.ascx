<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCursos.ascx.cs" Inherits="ForDummies.Web.UserControls.ucCursos" %>
<%@ Register Src="ucPaginacao.ascx" TagName="ucPaginacao" TagPrefix="uc1" %>
<asp:Panel ID="pnlCursosdosiste" runat="server" CssClass="container-listagem-cursos">
    <asp:Panel ID="pnlCursoTopo" CssClass="topo-descrip" runat="server" Visible="false">
        <h1 class="tituloMaior"><asp:Literal ID="ltrNomeClassificacao" runat="server"></asp:Literal></h1>
    </asp:Panel>
    <div class="lista-cursos">
        <ul>
        <asp:DataList ID="dtlCursos" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" OnItemDataBound="dtlCursos_ItemDataBound" RepeatLayout="Flow">
            <ItemTemplate>
                <li>
                    <a class="curso" href='/detalhe/<%#Eval("UrlSeo") %>'>
                        <asp:Image ID="imgCurso" CssClass="imagemCurso" BorderWidth="0" runat="server" />
                        <%#Eval("Nome") %>
                    </a>
                </li>
            </ItemTemplate>
            <SeparatorTemplate>
                <li class="separador-linha" runat="server" id="borda"></li>
            </SeparatorTemplate>
        </asp:DataList>
        </ul>
    </div>
    <asp:Panel ID="pnlCursoRodape" runat="server" Visible="false">
        <uc1:ucPaginacao ID="UcPaginacao2" runat="server" Visible="true" />
    </asp:Panel>
</asp:Panel>