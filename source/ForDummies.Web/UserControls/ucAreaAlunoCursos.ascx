﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAreaAlunoCursos.ascx.cs" Inherits="ForDummies.Web.UserControls.ucAreaAlunoCursos" %>
<div class="area-aluno-cursos">
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
            <div class="cursos-topo">
                <div class="fl">
                    <div class="cursos-dummies">MEUS CURSOS</div>
                </div>
                <div class="fr">
                    <div class="btn-group">
                        <button class="btn btn-novo"><asp:Literal ID="ltrAcao" runat="server" Text="Todos"></asp:Literal></button>
                        <button class="btn btn-amarelo dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                    <ul class="dropdown-menu">
                        <li>
                            <asp:LinkButton ID="lnkTodos" runat="server" onclick="lnkTodos_Click">Todos</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkConcluidos" runat="server" onclick="lnkConcluidos_Click">Concluídos</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkAndamento" runat="server" onclick="lnkAndamento_Click">Em andamento</asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkExpirados" runat="server" onclick="lnkExpirados_Click">Expirados</asp:LinkButton>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>

            <asp:Panel ID="pnlSemCursos" runat="server" CssClass="sem-cursos" Visible="false">
                
                <div class="exclamacao"><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></div>
            </asp:Panel>

        <asp:Panel ID="pnlAssinatura" runat="server" CssClass="box-box-modelo">
            <h2 id="hSubTitleAss" runat="server">Plano assinatura:</h2>
            <asp:Repeater ID="rptMatriculasAssinatura" runat="server"  OnItemDataBound="rptMatriculas_ItemDataBound">
                <HeaderTemplate>
                    <div id="venda-assinatura" class="cursos-slider">
		                    <div class="slideshow slides_container">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="slide">
                        <asp:Repeater ID="rptMatriculaFour" runat="server" OnItemDataBound="rptMatriculaFour_ItemDataBound">
                            <ItemTemplate>
                                <div class="matricula">
                                  <asp:Image CssClass="imagem-do-curso" ID="imgCurso" runat="server" width="140" height="81" />
                                    <div class="descricao-do-curso">
                                        <h4>
                                            <asp:Image ID="imgAudioCurso" Visible="false" ImageUrl="/imagens/audio-curso.png" runat="server" />
                                            <%#Eval("Curso.Nome") %>
                                        </h4>
                                        <div class="porcentagem-curso">
                                            <asp:Panel ID="pnlPorcentagem" runat="server" CssClass="porcento">
                                                <div class="progress">
                                                    <div style="width:<%# Eval("PorcentagemConclusao", "{0:N0}") + "%;" %>"></div>
                                                </div>
                                                <p><%# Eval("PorcentagemConclusao", "{0:N0}") + "%"%></p>
                                                <div style="clear:both;"></div>
                                            </asp:Panel>
                                            <asp:Literal ID="ltrNaoIniciado" runat="server"></asp:Literal>
                                            <asp:Literal ID="ltrConcluido" runat="server" Visible="false"><div class="container-full"><span class="curso-concluido">Concluído</span></div></asp:Literal>
                                        </div>
                                        <div class="acesso-curso">
                                            <asp:Literal ID="ltrTempoRestante" runat="server"></asp:Literal>
                                            <div class="container-full"><asp:LinkButton ID="lnkCertificado" Visible="false" OnClick="lnkCertificado_Click" runat="server" CssClass="emitir-certificado">EMITIR CERTIFICADO</asp:LinkButton></div>
                                        </div>
                                        <asp:HyperLink ID="lnkMatricula" Target="_blank" runat="server"></asp:HyperLink>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
		            </div>
                        <div class="group-pagination">
                            <div class="nexte">Próximo >></div>
                            <div class="preve"><< Anterior</div>
                        </div>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>

        <div id="boxTrocaNome">
            <div class="tBorda">
                <div class="E"></div><div class="M"></div><div class="D"></div>
            </div>
            <div class="cPadrao Centro">
                <div class="F"><a href="javascript:closeFuncao();" class="fechar">x</a></div>
                <asp:Panel ID="pnlNomeCertificado" runat="server">
                    <div>Digite abaixo o nome que constará no seu Certificado</div>
                    <div style="display: block; margin-top: 10px;">
                        <asp:TextBox ID="txtNomeCertificado" runat="server"></asp:TextBox>
                        <asp:Button ID="btnImprimirCertificado" Width="25" runat="server" Text="OK" OnClick="btnImprimirCertificado_Click"/>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlPesquisa" runat="server" Visible="false">
                    <div>Para emitir seu certificado é preciso responder à pesquisa de satisfação. É bem rápido. Vamos lá?</div>
                    <div style="display: block; margin-top: 10px;">
                        <asp:Button ID="btnResponderAgora" Width="100" runat="server" Text="Responder Agora" OnClick="btnResponderAgora_Click"/> 
                        <asp:Button ID="btnResponderDepois" Width="100" runat="server" Text="Responder Depois" OnClientClick="javascript:closeFuncao(); return false;"/>
                    </div>
                </asp:Panel>
            </div>
            <div class="rBorda">
                <div class="E"></div><div class="M"></div><div class="D"></div>
            </div>
        </div>

        </div>
    </div>
</div>