﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using ForDummies.BL.Interface;

namespace ForDummies.Web.UserControls
{
    public partial class ucRodape : System.Web.UI.UserControl
    {
        [Inject]
        public IClassificacaoServico classificacaoServico { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                dtlCategorias.RepeatColumns = 2;
                dtlCategorias.DataSource = classificacaoServico.ConsultarClassificacoes();
                dtlCategorias.DataBind();
            }
        }
    }
}
