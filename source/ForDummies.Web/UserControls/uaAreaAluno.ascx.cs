﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Linq;
using ForDummies.BL.Interface;
using Ninject;


namespace ForDummies.Web.UserControls
{
    public partial class uaAreaAluno : System.Web.UI.UserControl
    {
        [Inject]
        public ILogAcessoServico logAcessoServico { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            ControlaPainelLogin(Context.User.Identity.IsAuthenticated);
        }

        private void ControlaPainelLogin(bool logado)
        {
            if (logado)
            {
                pnlAlunoLogado.Visible = true;
            }
            else
            {
                pnlAlunoLogado.Visible = false;
            }

        }

        protected void lnkSair_Click(object sender, EventArgs e)
        {
            if (Page is AVA.Ava)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "fechar", "window.close();", true);
            else
            {

                logAcessoServico.FinalizarSessao(Convert.ToInt32(HttpContext.Current.User.Identity.Name), HttpContext.Current.Request.UserHostAddress);
				
                HttpCookie UOL_ID = new HttpCookie("UOL_ID");
                UOL_ID.Domain = ".uol.com.br";
                UOL_ID.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(UOL_ID);
				
                Response.Redirect("https://acesso.uol.com.br/logout.html?dest=REDIR|http://dummies.uol.com.br");
            }
        }
    }
}