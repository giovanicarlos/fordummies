﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.UI.HtmlControls;
using WebELojaForDummies.Model;
using ForDummies.Dominio;
using Ninject;
using ForDummies.BL;
using ForDummies.OpenID.Security;

namespace ForDummies.Web.UserControls
{
    public partial class ucAreaAlunoCursos : System.Web.UI.UserControl
    {
        [Inject]
        public MatriculaServico matriculaServico { get; set; }

        [Inject]
        public CursoServico cursoServico { get; set; }

        [Inject]
        public AlunoServico alunoServico { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ControlaPainelLogin(Context.User.Identity.IsAuthenticated);
        }

        private void ControlaPainelLogin(bool logado)
        {
            if (logado)
            {
                matriculaServico.AtualizarMatriculas(Convert.ToInt32(Context.User.Identity.Name));
                if (!Page.IsPostBack)
                {
                    CarregaMatriculas(null);
                }
            }
        }


        private void CarregaMatriculas(enmStatusMatricula? statusMatricula)
        {

            if (statusMatricula == null)
            {
                List<Matricula> matriculas = matriculaServico.Consultar(Convert.ToInt32(Page.User.Identity.Name), null);
                if (matriculas == null || matriculas.Count == 0)
                {
                    ltrMsg.Text = "Você ainda não escolheu um curso.";
                    pnlSemCursos.Visible = true;
                    pnlAssinatura.Visible = false;
                }
                else
                {
                    pnlSemCursos.Visible = false;
                    pnlAssinatura.Visible = true;

                    if (matriculas.Count() > 0)
                    {
                        foreach (var m in matriculas)
                        {
                            bool respondida = false;
                            if (m.PorcentagemConclusao == 100M && m.Status == (byte)enmStatusMatricula.Concluido)
                            {
                                respondida = true;
                                m.CertificadoDisponivel = true;
                            }
                        }

                        PreencheGrid(matriculas, rptMatriculasAssinatura, pnlAssinatura, hSubTitleAss, false);
                        pnlAssinatura.BorderWidth = 0;
                    }
                }
            }
            else
            {
                List<Matricula> matriculas = matriculaServico.Consultar(Convert.ToInt32(Page.User.Identity.Name), statusMatricula);
                if (matriculas == null)
                {
                    ltrMsg.Text = "Nenhum curso encontrado.";
                    pnlSemCursos.Visible = true;
                    pnlAssinatura.Visible = false;
                }
                else
                {
                    pnlSemCursos.Visible = false;
                    pnlAssinatura.Visible = true;

                    if (matriculas.Count() > 0)
                    {
                        foreach (var m in matriculas)
                        {
                            bool respondida = false;
                            if (m.PorcentagemConclusao == 100M && m.Status == (byte)enmStatusMatricula.Concluido)
                            {
                                respondida = true;
                                m.CertificadoDisponivel = true;
                            }
                        }

                        PreencheGrid(matriculas, rptMatriculasAssinatura, pnlAssinatura, hSubTitleAss, false);
                        pnlAssinatura.BorderWidth = 0;
                    }
                }
            }
        }

        private void PreencheGrid(List<Matricula> matriculas, Repeater rptMatriculas, Panel pnlBox, HtmlGenericControl hSubTitle, bool temOsDois)
        {
            int p = 6;
            if (temOsDois)
            {
                p = 2;
                pnlBox.CssClass = pnlBox.CssClass + " largura-dois-box";
                hSubTitle.Visible = true;
            }
            else
            {
                p = 6;
                hSubTitle.Visible = false;
                pnlBox.CssClass = pnlBox.CssClass + " largura-um-box";
            }

            if (matriculas.Count > 0)
            {
                List<AreaAlunoMatriculasModel> models = new List<AreaAlunoMatriculasModel>();

                int j = -1;
                for (int i = 0; i < matriculas.Count; i++)
                {
                    if (i % p == 0)
                    {
                        j++;
                        models.Add(new AreaAlunoMatriculasModel());
                    }
                    models[j].Matriculas.Add(matriculas[i]);
                }


                rptMatriculas.Visible = true;
                rptMatriculas.DataSource = models;
                rptMatriculas.DataBind();
            }
            else
            {
                pnlBox.Visible = false;
                rptMatriculas.Visible = false;
            }
        }

        protected void rptMatriculas_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AreaAlunoMatriculasModel model = e.Item.DataItem as AreaAlunoMatriculasModel;
                Repeater rptMatriculaFour = (Repeater)e.Item.FindControl("rptMatriculaFour");
                rptMatriculaFour.DataSource = model.Matriculas;
                rptMatriculaFour.DataBind();
            }
        }

        protected void rptMatriculaFour_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Matricula Matricula = e.Item.DataItem as Matricula;
                Literal ltrTempoRestante = (Literal)e.Item.FindControl("ltrTempoRestante");
                Literal ltrConcluido = (Literal)e.Item.FindControl("ltrConcluido");
                LinkButton lnkCertificado = (LinkButton)e.Item.FindControl("lnkCertificado");
                HyperLink lnkMatricula = (HyperLink)e.Item.FindControl("lnkMatricula");
                Image imgAudioCurso = (Image)e.Item.FindControl("imgAudioCurso");
                Panel pnlPorcentagem = (Panel)e.Item.FindControl("pnlPorcentagem");
                Literal ltrNaoIniciado = (Literal)e.Item.FindControl("ltrNaoIniciado");
                Image imgCurso = (Image)e.Item.FindControl("imgCurso");
                imgCurso.ImageUrl = new Helpers.ImagemHelper(cursoServico).GetUrlImagemCurso(Matricula.CursoId);

                if (Matricula.TempoRestante() <= 0 && Matricula.Status != (byte)enmStatusMatricula.Concluido)
                {
                    ltrTempoRestante.Text = "<div class=\"container-full-vermelho\">Seu acesso expirou</div>";
                }
                else
                {
                    ltrTempoRestante.Text = "<div class=\"container-full\">Seu acesso expira<br />em " + Matricula.TempoRestante().ToString() + " dias</div>";
                }


                if (Matricula.CertificadoDisponivel)
                {
                    lnkCertificado.Visible = true;
                    lnkCertificado.CommandArgument = Matricula.MatriculaId.ToString();
                    ltrTempoRestante.Visible = false;
                }

                if (Matricula.Status == (byte)enmStatusMatricula.Concluido)
                {
                    pnlPorcentagem.Visible = false;
                    ltrConcluido.Visible = true;
                    if (Matricula.TempoRestante() <= 0)
                        lnkMatricula.Visible = false;
                    else
                    {
                        lnkMatricula.Visible = true;
                        lnkMatricula.NavigateUrl = GeraUrlAVA(Matricula.MatriculaId, "acessar");
                        lnkMatricula.CssClass = "botao-acessar";
                    }
                }
                else if (Matricula.TempoRestante() > 0)
                {
                    lnkMatricula.NavigateUrl = GeraUrlAVA(Matricula.MatriculaId, "acessar");
                    lnkMatricula.CssClass = "botao-acessar";
                }
                else
                {
                    lnkMatricula.NavigateUrl = "~/detalhe/" + Matricula.Curso.UrlSeo;
                    lnkMatricula.CssClass = "botao-acessar";
                    lnkMatricula.Target = "_self";
                    pnlPorcentagem.CssClass = "porcento-vermelho";
                }
            }

        }

        private string GeraUrlAVA(int matriculaId, string ferramenta)
        {
            using (Simetrica cript = new Simetrica(TipoSimetrica.Rijndael))
            {
                cript.Chave = ConfigurationManager.AppSettings["SecretKey"];
                string m = cript.CriptografaQueryString(matriculaId.ToString() + "|" + ferramenta);
                return ConfigurationManager.AppSettings["CaminhoAVAIFrame"] + "AVA/Ava.aspx?m=" + m;
            }
        }

        protected void lnkCertificado_Click(object sender, EventArgs e)
        {
            Aluno Aluno = alunoServico.GetById(Convert.ToInt32(Page.User.Identity.Name));
            if (Aluno != null)
            {
                Matricula Matricula = matriculaServico.GetById(Convert.ToInt32(((LinkButton)sender).CommandArgument));

                if (!string.IsNullOrEmpty(Aluno.Nome.Trim()))
                {
                    if (ConfigurationManager.AppSettings["PossuiPesquisa"] == "true")
                    {
                        if (!Matricula.PesquisaRespondida)
                        {
                            pnlNomeCertificado.Visible = false;
                            pnlPesquisa.Visible = true;
                            btnResponderAgora.CommandArgument = Matricula.MatriculaId.ToString();

                            Page.ClientScript.RegisterStartupScript(GetType(), "janelinha", "<script type='text/javascript'>CertificadoNome();</script>");
                        }
                        else
                        {
                            txtNomeCertificado.Enabled = false;
                            Response.Redirect(GeraUrlAVA(Matricula.MatriculaId, "certificado"));
                        }
                    }
                    else
                    {
                        txtNomeCertificado.Enabled = false;
                        Response.Redirect(GeraUrlAVA(Matricula.MatriculaId, "certificado"));
                    }

                }
                else
                {
                    if (ConfigurationManager.AppSettings["PossuiPesquisa"] == "false")
                    {
                        btnImprimirCertificado.OnClientClick = "javascript:closeFuncao();";
                    }
                    else
                    {
                        if (Matricula.PesquisaRespondida)
                            btnImprimirCertificado.OnClientClick = "javascript:closeFuncao();";
                    }
                    btnImprimirCertificado.CommandArgument = ((LinkButton)sender).CommandArgument;
                    Page.ClientScript.RegisterStartupScript(GetType(), "janelinha", "<script type='text/javascript'>CertificadoNome();</script>");
                }
            }
        }

        protected void btnResponderAgora_Click(object sender, EventArgs e)
        {
            Matricula Matricula = matriculaServico.GetById(Convert.ToInt32(((Button)sender).CommandArgument));
            Response.Redirect(GeraUrlAVA(Matricula.MatriculaId, "pesquisa"));
        }


        protected void btnImprimirCertificado_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNomeCertificado.Text))
            {
                Aluno Aluno = alunoServico.GetById(Convert.ToInt32(Page.User.Identity.Name));
                if (Aluno != null)
                {
                    Aluno.Nome = txtNomeCertificado.Text;
                    txtNomeCertificado.Enabled = false;
                    alunoServico.Update(Aluno);

                    Matricula Matricula = matriculaServico.GetById(Convert.ToInt32(((Button)sender).CommandArgument));
                    if (ConfigurationManager.AppSettings["PossuiPesquisa"] == "true")
                    {
                        if (!Matricula.PesquisaRespondida)
                        {
                            pnlPesquisa.Visible = true;
                            pnlNomeCertificado.Visible = false;
                            btnResponderAgora.CommandArgument = Matricula.MatriculaId.ToString();
                            Page.ClientScript.RegisterStartupScript(GetType(), "janelinha", "<script type='text/javascript'>CertificadoNome();</script>");
                        }
                        else
                        {
                            Response.Redirect(GeraUrlAVA(Matricula.MatriculaId, "certificado"));
                        }
                    }
                    else
                    {
                        Response.Redirect(GeraUrlAVA(Matricula.MatriculaId, "certificado"));
                    }
                }
            }
        }

        protected void lnkTodos_Click(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                ltrAcao.Text = "Todos";
                CarregaMatriculas(null);
            }
            else
                Response.Redirect("/entrar");
        }

        protected void lnkConcluidos_Click(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                ltrAcao.Text = "Concluídos";
                CarregaMatriculas(enmStatusMatricula.Concluido);
            }
            else
                Response.Redirect("/entrar");
        }

        protected void lnkAndamento_Click(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                ltrAcao.Text = "Em andamento";
                CarregaMatriculas(enmStatusMatricula.EmAndamento);
            }
            else
                Response.Redirect("/entrar");
        }

        protected void lnkExpirados_Click(object sender, EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated)
            {
                ltrAcao.Text = "Expirados";
                CarregaMatriculas(enmStatusMatricula.Expirado);
            }
            else
                Response.Redirect("/entrar");
        }
    }
}