<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMenu.ascx.cs" Inherits="ForDummies.Web.UserControls.ucMenu" %>
<script type='text/javascript'>
    jQuery(function(){if(jQuery(".classChange").hasClass("conteudoInterno")){jQuery(".classChange").removeClass("conteudoInterno").addClass("conteudo").hide();}jQuery(".topo-click").click(function(){jQuery(".conteudo").toggle("blind");return false;});});
</script>
<div class="escolha-sua-area">
    <asp:Panel ID="pnlTopoMenu" runat="server" CssClass="topo"><h2>CATEGORIAS</h2></asp:Panel>
    <asp:Panel ID="pnlConteudo" runat="server" CssClass="classChange conteudo">
        <div class="corpo">
            <asp:DataList ID="dtlItensMenu" runat="server" RepeatLayout="Flow" 
                ItemStyle-CssClass="itemMenu" 
                OnItemDataBound="dtlItensMenu_ItemDataBound">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkMenu" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:DataList>
        </div>
    </asp:Panel>
</div>