﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using ForDummies.BL.Interface;
using System.Configuration;

namespace ForDummies.Web.UserControls
{
    public partial class ucMatriculeSe : System.Web.UI.UserControl
    {
        [Inject]
        public ICursoServico CursoServico { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            curso.Value = CursoServico.Consultar(Request.QueryString["id"]).CursoId.ToString();
            sistema.Value = ConfigurationManager.AppSettings["IdSistemaDummies"].ToString();
        }
    }
}