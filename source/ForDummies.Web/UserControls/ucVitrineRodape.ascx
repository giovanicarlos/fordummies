﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucVitrineRodape.ascx.cs" Inherits="ForDummies.Web.UserControls.ucVitrineRodape" %>
<div id="inc-home-2">
  <div class="como-funciona"><div class="aba">COMO FUNCIONA?</div></div>
  <div class="passos">
    <div class="a">
      <div class="txt">Fa&ccedil;a sua assinatura!</div>
    </div>
    <div class="b">
      <div class="txt">Escolha <strong>1 curso</strong><br />diferente <strong>todo mês</strong></div>
    </div>
    <div class="c">
      <div class="txt">Após o término do curso,<br />adquira o seu <strong>certificado</strong>.</div> 
    </div>
    <div style="clear:both"></div>
  </div>
</div>

<script type="text/javascript">
    //carousel
    var s = true; //slide
    var loop = 0;
    var v = 0;
    var pausa = 0;
    var t;
    var t_hover;
    var pos_group = 0;

    function slide() {
        if (s) {
            v = 1500;
            pausa = 10000;
            if (pos_group == 1800) {
                pos_group = 0;
                jQuery('.group').css('marginLeft', -pos_group + 'px');
            }
            pos_group = pos_group + 900;
            jQuery('.group').stop().animate({
                marginLeft: -pos_group + 'px'
            }, v);
            clearTimeout(t);
            start();
        }
    }

    function start() {
        if (s) {
            pausa = 10000;
        } else {
            pausa = 0;
            s = true;
        }
        t = setTimeout(slide, pausa);
    }

    jQuery('.group').hover(function () {
        clearTimeout(t_hover);
        s = false;
    }, function () {
        clearTimeout(t_hover);
        t_hover = setTimeout(start, 5000);
    });

    start();

    jQuery('.carrousel .next').click(function () {
        clearTimeout(t_hover);
        s = false;
        if (pos_group == 1800) {
            pos_group = 0;
            jQuery('.group').css('marginLeft', -pos_group + 'px');
        }
        pos_group = pos_group + 900;
        jQuery('.group').stop().animate({
            marginLeft: -pos_group + 'px'
        }, 500);
        t_hover = setTimeout(start, 5000);
        return false;
    });

    jQuery('.carrousel .prev').click(function () {
        clearTimeout(t_hover);
        s = false;
        if (pos_group == 0) {
            pos_group = 1800;
            jQuery('.group').css('marginLeft', -pos_group + 'px');
        }
        pos_group = pos_group - 900;
        jQuery('.group').stop().animate({
            marginLeft: -pos_group + 'px'
        }, 500);
        t_hover = setTimeout(start, 5000);
        return false;
    });
    jQuery('body').css('background', '#FFF');
    jQuery('#centraliza').remove();
    setTimeout(apaga, 1000);
    function apaga() {
        jQuery('#centraliza').remove();
        jQuery('.bgbarrauol').remove();
    }
</script>
