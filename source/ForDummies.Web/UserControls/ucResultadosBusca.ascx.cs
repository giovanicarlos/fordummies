using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ninject;
using ForDummies.BL.Interface;
using ForDummies.Dominio;

namespace ForDummies.Web.UserControls
{
    public partial class ucResultadosBusca : System.Web.UI.UserControl
    {
        [Inject]
        public ICursoServico CursoServico { get; set; }

        private int paginaAtual
        {
            get
            {
                int pagina = 1;
                if (!string.IsNullOrEmpty(Request.QueryString["p"]))
                    if (!int.TryParse(Request.QueryString["p"], out pagina))
                        pagina = 1;
                return pagina;
            }
        }
        int registrosPorPagina = 10;
        int total = 0;
        int maiorNumVendas = 0;

        internal UOL _master
        {
            get { return (UOL)this.Page.Master; }
            set
            {
                UOL master = (UOL)this.Page.Master;
                master = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CarregaCursos();
                GerenciarResultados();

                if (!string.IsNullOrEmpty(Request.QueryString["tipo"]))
                {
                    if (Request.QueryString["tipo"].Equals("1"))
                    {
                        ddlTipoCurso.SelectedValue = "1";
                    }
                    else if (Request.QueryString["tipo"].Equals("0"))
                    {
                        ddlTipoCurso.SelectedValue = "0";
                    }
                }
            }
        }

        private void CarregaCursos()
        {
            if ((!string.IsNullOrEmpty(Request.QueryString["Titulo"])) || (!string.IsNullOrEmpty(Request.QueryString["Autor"])))
            {
                string Url = "/resultado-de-busca.aspx";

                if (!string.IsNullOrEmpty(Request.QueryString["Titulo"]))
                    Url += "&Titulo=" + Request.QueryString["Titulo"];

                int? formato = null;
                if (!string.IsNullOrEmpty(ddlTipoCurso.SelectedValue))
                {
                    formato = Convert.ToInt32(ddlTipoCurso.SelectedValue);
                }

                rptCursos.DataSource = CursoServico.ConsultarCursos(Request.QueryString["Titulo"], formato, paginaAtual, registrosPorPagina, out total);
                rptCursos.DataBind();

                if (total > 0)
                {
                    PreenchePaginacao(Url + "&p=");
                }
            }
        }

        private void GerenciarResultados()
        {
            string busca = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["Titulo"]))
                busca = Request.QueryString["Titulo"];

            if (!string.IsNullOrEmpty(busca))
            {
                int registros = registrosPorPagina;
                if (registrosPorPagina > total)
                    registros = total;
                if (total.Equals(0))
                {
                    pnlNenhumResultado.Visible = true;
                    ltrRegostros.Text = "Desculpe, nenhum resultado foi encontrado para " + busca;
                }
                else
                {
                    pnlNenhumResultado.Visible = false;
                    ltrRegostros.Text = "Resultados " + paginaAtual.ToString() + " - " + registros.ToString() + " de " + total.ToString() + " para " + busca;
                }
            }
            else
            {
                pnlNenhumResultado.Visible = true;
                ltrRegostros.Text = "Digite uma palavra chave para iniciar sua busca.";
                pnlVisualizar.Visible = false;
            }
        }

        private void PreenchePaginacao(string url)
        {
            UcPaginacao2.Paginar(paginaAtual, registrosPorPagina, total, url);
        }

        protected void rptCursos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string IdSistema = ConfigurationManager.AppSettings["IdSistemaDummies"];
                Curso toCurso = e.Item.DataItem as Curso;
                toCurso = CursoServico.GetById(toCurso.CursoId);
                Image imgCurso = (Image)e.Item.FindControl("imgCurso");
                HyperLink linkBotaoCurso = (HyperLink)e.Item.FindControl("linkBotaoCurso");
                Literal ltrIdioma = (Literal)e.Item.FindControl("ltrIdioma");

                imgCurso.ImageUrl = new Helpers.ImagemHelper(CursoServico).GetUrlImagemCurso(toCurso.CursoId);

                ltrIdioma.Text = "<span class=\"idiomaBusca\">" + _master.getIdiomaCurso(toCurso.Idioma) + "</span>";

                linkBotaoCurso.NavigateUrl = "/detalhe/" + toCurso.UrlSeo;
                if (Context.User.Identity.IsAuthenticated && Context.User.IsInRole("aluno"))
                    linkBotaoCurso.CssClass = "botao-resultado-curso-detalhes";
                else
                    linkBotaoCurso.CssClass = "botao-resultado-curso-assime-ja";
            }
        }

        protected void ddlTipoCurso_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(Request.QueryString["Autor"]))
                Response.Redirect("/resultado-de-busca.aspx?Autor=" + Request.QueryString["Autor"] + "&tipo=" + ddlTipoCurso.SelectedValue);
            else if (!string.IsNullOrEmpty(Request.QueryString["Titulo"]))
                Response.Redirect("/resultado-de-busca.aspx?Titulo=" + Request.QueryString["Titulo"] + "&tipo=" + ddlTipoCurso.SelectedValue);
        }

    }
}
