﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using ForDummies.BL.Interface;
using Ninject;
using ForDummies.Dominio;

namespace ForDummies.Web.Helpers
{
    public class ImagemHelper : IImagemHelper
    {
        private readonly ICursoServico cursoServico;

        public ImagemHelper(ICursoServico cursoServico)
        {
            this.cursoServico = cursoServico;
        }

        public string GetUrlImagemPreview(int cursoId, int ordem)
        {
            Curso curso = cursoServico.GetById(cursoId);
            string urlFinal = ConfigurationManager.AppSettings["urlViewImagemPreview"] +
                "?Id=" + curso.CodigoCursoCB.ToString() + "&sistema=" + ConfigurationManager.AppSettings["IdSistemaDummies"] +
                "&Ordem=" + ordem.ToString() + "&tamanho=2" + "&key=" + Guid.NewGuid().ToString();

            return urlFinal;
        }

        public string GetUrlImagemCurso(int cursoId)
        {
            Curso curso = cursoServico.GetById(cursoId);
            string urlFinal = ConfigurationManager.AppSettings["urlViewImagemCurso"] +
                "?Id=" + curso.CodigoCursoCB.ToString() + "&sistema=" + ConfigurationManager.AppSettings["IdSistemaDummies"] +
                "&tamanho=5";

            return urlFinal;
        }
        
    }

    public interface IImagemHelper
    {
        string GetUrlImagemPreview(int cursoId, int ordem);
        string GetUrlImagemCurso(int cursoId);
    }
}