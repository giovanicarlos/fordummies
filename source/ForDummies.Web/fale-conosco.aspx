<%@ Page Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="fale-conosco.aspx.cs"
    Inherits="ForDummies.Web.fale_conosco" Title="Fale Conosco - UOL Cursos Online" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Esclare�a suas d�vidas com a Central de Atendimento: UOL Cursos Online." />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
            <div class="corpo fale-conosco">
                <h1 class="titulo-faleconosco">FALE CONOSCO</h1>
                <div class="fordummies-faleconosco"></div>
                <div class="telefones">
                    <p>
                        Para falar com a nossa <strong>Central de Atendimento</strong>,<br />
                        utilize os n�meros abaixo:</p>
                    <p><strong class="tel">4003-6792</strong><br />
                        Capitais e regi�es metropolitanas</p>
                    <p><strong class="tel">0800-728-2052</strong><br />
                        Demais localidades</p>
                </div>
                <div class="links">
                    <p>
                        Para <strong>alterar dados da sua conta</strong> ou seus dados de cobran�a, clique abaixo:</p>
                    <h2>
                        Minha conta</h2>
                    <p>
                        <a target="_blank" href="http://sac.uol.com.br/usuario/senha.html">�Alterar senha</a></p>
                    <p>
                        <a target="_blank" href="http://sac.uol.com.br/info/esqueci-senha.jhtm">�Recuperar senha</a></p>
                    <p>
                        <a target="_blank" href="http://sac.uol.com.br/usuario/dados_pessoais.html">�Alterar
                            dados pessoais</a></p>
                    <p>
                        <a target="_blank" href="http://sac.uol.com.br/inscricao/endereco_acesso.html">�Alterar
                            endere�o de acesso</a></p>
                    <h2>
                        Minha cobran�a</h2>
                    <p>
                        <a target="_blank" href="http://sac.uol.com.br/conta/quitacao.html?id_acct=">�Quitar
                            d�bito</a></p>
                    <p>
                        <a target="_blank" href="http://sac.uol.com.br/inscricao/endereco_acesso.html">�Consultar
                            extrato</a></p>
                </div>
                <div style="clear: both;">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
