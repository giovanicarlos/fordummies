﻿jQuery.noConflict();
function CertificadoNome(){jQuery.blockUI({message:jQuery('#boxTrocaNome'),css:{color:'#ccc',background:'none',width:'600px',border:'none'}});jQuery("#boxTrocaNome").parent().appendTo(jQuery("form:first"));}
function closeFuncao(){jQuery.unblockUI()}
(function(jQuery){jQuery.fn.scrollFixed=function(params){params=jQuery.extend({appearAfterDiv:0,hideBeforeDiv:0},params);var element=jQuery(this);if(params.appearAfterDiv)var distanceTop=element.offset().top + jQuery(params.appearAfterDiv).outerHeight(true) + element.outerHeight(true);else var distanceTop = element.offset().top;if(params.hideBeforeDiv) var bottom = jQuery(params.hideBeforeDiv).offset().top - element.outerHeight(true) - 10;else var bottom = 200000;jQuery(window).scroll(function(){if(jQuery(window).scrollTop() > distanceTop && jQuery(window).scrollTop() < bottom){element.css({ 'position': 'fixed', 'top': '0px', 'z-index': '1000' });jQuery('.BarraFixa').fadeIn();}else{element.css({ 'position': 'static' });jQuery('.BarraFixa').hide();}});};})(jQuery);
jQuery(document).ready(function () {
    jQuery(".faixa-links").scrollFixed();
    jQuery('#slider').nivoSlider();
    jQuery("#email-news").click(function () {
        jQuery("#e-boletim").val(jQuery("#ctl00_ucRodape1_txtEmail").val());
        jQuery("#form-news").submit();
    });
    jQuery("#mask-check").click(function () {
        var destino = jQuery("#dest").val();
        if (jQuery("#mask-check").prop('class') == "css-label-checked") {
            destino = destino.replace("/?remember=true", "");
            jQuery("#dest").val(destino);
            jQuery("#mask-check").switchClass("css-label-checked", "css-label", 0, "easeInOutQuad");
        }
        else {
            destino += "/?remember=true"
            jQuery("#dest").val(destino);
            jQuery("#mask-check").switchClass("css-label", "css-label-checked", 0, "easeInOutQuad");
        }
    });
});
jQuery(function(){jQuery("#venda-assinatura").slides({preload:false,generatePagination:false,play:0,pause:0,hoverPause:true,next:'nexte',prev:'preve'});});