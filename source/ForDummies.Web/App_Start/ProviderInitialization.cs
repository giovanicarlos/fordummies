﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ForDummies.Web.Provider;
using Ninject.Web.Common;

namespace ForDummies.Web.App_Start
{
    public class ProviderInitialization : HttpApplicationInitializationHttpModule
    {
        public ProviderInitialization(MembershipForDummiesProvider membershipProvider)
        {
        }

        public void Init(HttpApplication context)
        {
        }

        public void Dispose()
        {
        }
    }
}