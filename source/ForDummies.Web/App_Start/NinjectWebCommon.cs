[assembly: WebActivator.PreApplicationStartMethod(typeof(ForDummies.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(ForDummies.Web.App_Start.NinjectWebCommon), "Stop")]

namespace ForDummies.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using ForDummies.Infra.Contexto;
    using ForDummies.Dominio.Interface;
    using ForDummies.Infra;
    using ForDummies.BL;
    using ForDummies.BL.Interface;
    using ForDummies.Web.Provider;
    using System.Web.Security;
    using libUOL.Interface;
    using libUOL.Implementacao;
    using ForDummies.Web.Helpers;

    public static class NinjectWebCommon 
    {
        public static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //base
            kernel.Bind<IGerenciaContexto>().To<GerenciaContexto>().InRequestScope();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            kernel.Bind<IUOLService>().To<UOLService>();
            kernel.Bind<IImagemHelper>().To<ImagemHelper>();

            //repositorio
            kernel.Bind<IAlunoRepositorio>().To<AlunoRepositorio>();
            kernel.Bind<ILogAcessoRepositorio>().To<LogAcessoRepositorio>();
            kernel.Bind<IClassificacaoRepositorio>().To<ClassificacaoRepositorio>();
            kernel.Bind<IConteudoRepositorio>().To<ConteudoRepositorio>();
            kernel.Bind<ICursoRepositorio>().To<CursoRepositorio>();
            kernel.Bind<IItemPedidoRepositorio>().To<ItemPedidoRepositorio>();
            kernel.Bind<IMatriculaRepositorio>().To<MatriculaRepositorio>();
            kernel.Bind<IPedidoRepositorio>().To<PedidoRepositorio>();
            kernel.Bind<IPessoaRepositorio>().To<PessoaRepositorio>();

            //servico           
            kernel.Bind<ILogAcessoServico>().To<LogAcessoServico>();
            kernel.Bind<IAlunoServico>().To<AlunoServico>();
            kernel.Bind<IClassificacaoServico>().To<ClassificacaoServico>();
            kernel.Bind<IConteudoServico>().To<ConteudoServico>();
            kernel.Bind<ICursoServico>().To<CursoServico>();
            kernel.Bind<IItemPedidoServico>().To<ItemPedidoServico>();
            kernel.Bind<IMatriculaServico>().To<MatriculaServico>();
            kernel.Bind<IPedidoServico>().To<PedidoServico>();
            kernel.Bind<IPessoaServico>().To<PessoaServico>();

        }        
    }
}
