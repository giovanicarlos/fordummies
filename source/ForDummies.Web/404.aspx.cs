using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace ForDummies.Web
{
    public partial class _04 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            verificaErro404();
        }

        private void verifica()
        {
            Session["ERRO404"] = Request.QueryString["aspxerrorpath"];
        }

        private void verificaErro404()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["aspxerrorpath"]))
            {
                if (Session["ERRO404"] != null)
                {
                    string Caminho = (string)Session["ERRO404"];
                    if (Caminho.Equals(Request.QueryString["aspxerrorpath"]))
                        Session.Remove("ERRO404");
                    else
                        verifica();
                }
                else
                {
                    verifica();
                }
            }
        }
    }
}
