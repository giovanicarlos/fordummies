using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using ForDummies.Web.Helpers;
using ForDummies.Web.Provider;
using Ninject;
using ForDummies.BL;
using ForDummies.Dominio;
using ForDummies.BL.Interface;

namespace ForDummies.Web
{
    public partial class Default : System.Web.UI.Page
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Form.Action = "/";
                if (Request.QueryString["id"] != null)
                {
                    UcCursos1.Visible = true;
                    UcMenu1.CategoriasVertical = true;
                    ucVitrine1.Visible = false;
                    ucVitrineRodape1.Visible = false;
                    pnlFaixaHome.Visible = false;
                    pnlCorpoHome.CssClass = "corpo-maior espaco-topo";
                }
                else
                {
                    this.Page.Title = "UOL For Dummies - Para Leigos";
                    HtmlMeta HtmlMetaKeyDescription = new HtmlMeta();
                    HtmlMetaKeyDescription.Name = "Description";
                    HtmlMetaKeyDescription.Content = "UOL For Dummies - Para Leigos";
                    Page.Header.Controls.Add(HtmlMetaKeyDescription);
                    UcMenu1.CategoriasVertical = true;
                    UcCursos1.Visible = false;
                }
                if (Request.QueryString["action"] == "login")
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "AbreDetalhes", "<script type=\"text/javascript\">jQuery(function() {jQuery(\"#sobe-desce\").attr(\"src\",\"/imagens/area-aluno-seta-cima.png\");});</script>");
            }
        }
    }
}
