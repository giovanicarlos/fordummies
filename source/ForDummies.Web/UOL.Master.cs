using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Net;
using System.IO;

namespace ForDummies.Web
{
    public partial class UOL : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ControlaPainelLogin(Context.User.Identity.IsAuthenticated);
            ControlaLinks();
        }

        private void ControlaPainelLogin(bool logado)
        {
            if (logado)
            {
                pnlLogin.Visible = false;
                lnkAssine.Visible = false;
                if (Page is Default)
                    ucAreaAlunoCursos.Visible = true;
            }
            else
            {
                pnlLogin.Visible = true;
            }
        }

        public string getIdiomaCurso(string idioma)
        {
            switch (idioma)
            {
                case "es-MX":
                    return "(Espanhol)";
                case "en-US":
                    return "(Ingl�s)";
                default:
                    break;
            }

            return string.Empty;
        }

        private void ControlaLinks()
        {
            ltrLogo.Text = "<div class=\"logo\">UOL Para Leigos For Dummies</div>";
            if (Page is Default)
            {
                lnkHome.CssClass = "active";
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                    ltrLogo.Text = "<div class=\"logo\">UOL Para Leigos For Dummies</div>";
                else
                    ltrLogo.Text = "<h1 class=\"logo\">UOL Para Leigos For Dummies</h1>";
            }
            if (Page is uol_for_dummies)
                lnkOQueE.CssClass = "active";
            if (Page is como_utilizar)
                lnkComoUtilizar.CssClass = "active";
            if (Page is duvidas)
                lnkDuvidas.CssClass = "active";
            if (Page is fale_conosco)
                lnkFaleConosco.CssClass = "active";
            if (Page is assine)
                lnkAssine.CssClass = "active";
        }

    }
}
