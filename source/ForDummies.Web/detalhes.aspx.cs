using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using System.Threading;
using ForDummies.Web.Helpers;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using Ninject;
using libUOL.Interface;

namespace ForDummies.Web
{
    public partial class detalhes : System.Web.UI.Page
    {
        [Inject]
        public ICursoServico cursoServico { get; set; }

        [Inject]
        public IImagemHelper imagemHelper { get; set; }

        [Inject]
        public IUOLService uolService { get; set; }

        [Inject]
        public IMatriculaServico matriculaServico { get; set; }

        [Inject]
        public IClassificacaoServico classificacaoServico { get; set; }

        [Inject]
        public IConteudoServico conteudoServico { get; set; }


        internal UOL _master
        {
            get { return (UOL)this.Master; }
            set
            {
                UOL master = (UOL)this.Master;
                master = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    CarregaCurso(Request.QueryString["id"]);

                    ConfigurarVendaAssinatura();
                    
                    if (Session["erro"] != null)
                    {
                        pnlAcabou.Visible = true;
                        Session.Remove("erro");
                    }
                }
                else
                    Response.Redirect("/?invalidKey=" + Request.Url.PathAndQuery);

            }
        }

        
        #region SEO e M�dias Sociais
        /*
        /// <summary>
        /// Este m�todo gera os scripts para SEO de dados do facebook , que s�o inseridos no HEAD de cada p�gina
        /// </summary>
        public void InserirScriptFacebook(TO.Curso curso)
        {
            // Aguardando defini��o
            //string idioma = string.Empty;
            //if (curso.Idioma.Contains("es"))
            //    idioma = " em Espanhol";
            //else if (curso.Idioma.Contains("en"))
            //    idioma = " em Ingl�s";

            //string titulo = "Curso Online de " + curso.Nome + idioma + " - UOL Cursos Online";
            //if (!string.IsNullOrEmpty(curso.TitleSeo))
            //{
            //    if(curso.TitleSeo.Length > 3)
            //        titulo = curso.TitleSeo + idioma + " - UOL Cursos Online";
            //}

            //ltr_ogdescription.Text = "<meta property='og:description' content='O UOL Cursos Online oferece curso de " + curso.Nome + idioma + " com Certificado. S�o mais de 450 cursos � sua escolha. Assine j�!'/>";
            //ltr_ogurl.Text = "<meta property='og:url' content='http://cursosonline.uol.com.br/" + curso.UrlFormacursoSeo +"'/>";
            //ltr_ogtitle.Text = "<meta property='og:title' content='" + titulo + "'/>";
            //ltr_ogimage.Text = "<meta property='og:image' content='http://cursosonline.uol.com.br/imagens/ViewImagens.aspx?Id=" + curso.Codigo + "&tamanho=3&tipo=1'/>";
        } */
        #endregion
        
        
        private void CarregaCurso(string IdCurso)
        {
            Curso curso = cursoServico.Consultar(Request.QueryString["id"]);
            if (curso != null)
            {
                if (curso.enmStatusCurso == enmStatusCurso.Bloqueado)
                    Response.Redirect("/");

                Page.Form.Action = curso.UrlSeo;

                string idioma = string.Empty;
                if (curso.Idioma.Contains("es"))
                    idioma = " em Espanhol";
                else if (curso.Idioma.Contains("en"))
                    idioma = " em Ingl�s";                

                Page.Title = "Curso Online de " + curso.Nome + idioma + " - UOL For Dummies"; ;

                Classificacao classificacao = classificacaoServico.Consultar(curso.CursoId);

                //ConsultarCursoTop5(classificacao, curso);

                hlkAreaClassificacao.Text = classificacao.Nome;
                hlkAreaClassificacao.NavigateUrl = "/categoria/" + classificacao.UrlSeo;

                HtmlMeta HtmlMetaKeyDescription = new HtmlMeta();
                HtmlMetaKeyDescription.Name = "Description";
                HtmlMetaKeyDescription.Content = "O UOL Cursos Online oferece curso de " + curso.Nome + idioma + " com Certificado. S�o mais de 450 cursos � sua escolha. Assine j�!";
                Page.Header.Controls.Add(HtmlMetaKeyDescription);

                ltrNomeCurso.Text = lblNomeCurso.Text = curso.Nome;

                ltrDuracao.Text = curso.CargaHoraria;

                ltrDescricao.Text = curso.Descricao;
                ltrRequisitos.Text = curso.Requisitos;


                List<Curso> lstCursoPreview = new List<Curso>();
                for (int i = 0; i < curso.QuantidadePreview; i++)
                {
                    lstCursoPreview.Add(curso);
                }

                List<Conteudo> conteudos = conteudoServico.ListarConteudos(curso.CursoId);
                dtlConteudos.DataSource = conteudos;
                dtlConteudos.DataBind();

                rptImagemConteudo.DataSource = lstCursoPreview;
                rptImagemConteudo.DataBind();
                //InserirScriptFacebook(curso);
            }
            else
                Response.Redirect("/");
             
        }

        protected void dtlConteudos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Conteudo conteudo = (Conteudo)e.Item.DataItem;
                Literal ltrTitulo = (Literal)e.Item.FindControl("ltrTitulo");
            }
        }

        protected void btnAssineJa_Click(object sender, ImageClickEventArgs e)
        {
            Curso curso = cursoServico.Consultar(Request.QueryString["id"]);
            Response.Redirect("/entrar/?curso=" + curso.CursoId.ToString());
        }


       protected void btnAssinante_Click(object sender, ImageClickEventArgs e)
       {
           if (Page.User.Identity.IsAuthenticated && Page.User.IsInRole("aluno"))
           {
               Curso curso = cursoServico.Consultar(Request.QueryString["id"]);
               if (curso != null)
               {
                   ltrNomeCursomodal.Text = curso.Nome;
                   ImageCursoModal.ImageUrl = new Helpers.ImagemHelper(cursoServico).GetUrlImagemCurso(curso.CursoId);
                   Page.ClientScript.RegisterStartupScript(GetType(), "janelinha", "<script type='text/javascript'>sucesso();</script>");
               }
           }
       }

       protected void btnMatriCular_Click(object sender, EventArgs e)
       {
            Curso curso = cursoServico.Consultar(Request.QueryString["id"]);
            if((curso != null) && (matriculaServico.PermiteMatricular(Convert.ToInt32(Page.User.Identity.Name), curso.FormatoId)))
            {
            Response.Redirect("/entrar/Matricular.aspx?curso=" + curso.CursoId.ToString() + "&sistema=" + ConfigurationManager.AppSettings["IdSistemaDummies"] + "&venda=" + (int)ForDummies.Web.entrar.TipoVendaUol.Assinatura);
            }
       }

       //private void ConsultarCursoTop5(TO.Classificacao classificacao, TO.Curso curso)
       //{
       //    List<TO.Curso> cursos = new BL.Curso().ConsultarCursosTopMaisVendidos(classificacao, curso, 4);
       //    if (cursos.Count > 0)
       //    {
       //        BoxTopCursos.Visible = true;
       //        dtlCursos.DataSource = cursos;
       //        dtlCursos.DataBind();
       //    }
       //    else
       //        BoxTopCursos.Visible = false;
       //}

       protected void dtlCursos_ItemDataBound(object sender, DataListItemEventArgs e)
       {
           if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
           {
               Curso curso = e.Item.DataItem as Curso;
               Image imgAudioCurso = (Image)e.Item.FindControl("imgAudioCurso");
               Image immBotaoCurso = (Image)e.Item.FindControl("immBotaoCurso");
               Image imgCurso = (Image)e.Item.FindControl("imgCurso");

             
               ImageCursoModal.ImageUrl = new Helpers.ImagemHelper(cursoServico).GetUrlImagemCurso(curso.CursoId);
            
               if (Context.User.Identity.IsAuthenticated && Context.User.IsInRole("aluno"))
               {
                   immBotaoCurso.ToolTip = "SAIBA MAIS";
                   immBotaoCurso.ImageUrl = "/imagens/botao-detalhes-curso.png";
               }
               else
               {
                   immBotaoCurso.ToolTip = "SAIBA MAIS";
                   immBotaoCurso.ImageUrl = "/imagens/botao-assinar-curso.png";
               }
           }
       }
       
        protected string getObjetivo(object objetivo)
        {
            return Util.getStringSemCortes((string)objetivo, 160);
        }
        
        private void ConfigurarVendaAssinatura()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("aluno"))
            {
                int idSistema = Convert.ToInt32(ConfigurationManager.AppSettings["IdSistemaDummies"].ToString());
                Curso curso = cursoServico.Consultar(Request.QueryString["id"]);
                if (curso != null)
                {
                    if (uolService.PossuiAssinatura())
                    {
                        if (matriculaServico.PermiteMatricular(Convert.ToInt32(Page.User.Identity.Name), curso.FormatoId))
                        {
                            if (curso.PermiteVendaAssinatura)
                            {
                                btnAssinante.Visible = true;
                            }
                        }
                        else
                        {
                            pnlAcabou.Visible = true;
                        }
                        ucMatriculeSe.Visible = false;
                    }
                    else
                    {
                        pnlAcabou.Visible = false;
                    }
                }
            }
        }
        
        int Ordem = 0;
        protected void rptImagemConteudo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {   
                Curso curso =  cursoServico.Consultar(Request.QueryString["id"]);
                Ordem += 1;
                Image imgCurso = (Image)e.Item.FindControl("imgCurso");
                imgCurso.ImageUrl = imagemHelper.GetUrlImagemPreview(curso.CursoId, Ordem);
            }
        }      
    }
}

