<%@ Page Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ForDummies.Web.Default" %>
<%@ Register Src="UserControls/ucCursos.ascx" TagName="ucCursos" TagPrefix="uc4" %>
<%@ Register Src="UserControls/ucMenu.ascx" TagName="ucMenu" TagPrefix="uc1" %>
<%@ Register src="UserControls/ucVitrine.ascx" tagname="ucVitrine" tagprefix="uc3" %>
<%@ Register src="UserControls/ucVitrineRodape.ascx" tagname="ucVitrineRodape" tagprefix="uc5" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <meta name="google-site-verification" content="uPoEEmwsmGUXmhtQSbPK1DFcI9TNUZB19rqvpshC4vw" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlFaixaHome" runat="server" CssClass="faixa-home"></asp:Panel>
    <asp:Panel ID="pnlCorpoHome" runat="server" CssClass="corpo-home">
        <uc1:ucMenu id="UcMenu1" runat="server"></uc1:ucMenu>
        <uc3:ucVitrine ID="ucVitrine1" runat="server" />
        <uc5:ucVitrineRodape ID="ucVitrineRodape1" runat="server" />
        <uc4:ucCursos id="UcCursos1" RepeatColumns="4" runat="server"></uc4:ucCursos>
        <div style="clear:both;"></div>
    </asp:Panel>
</asp:Content>
