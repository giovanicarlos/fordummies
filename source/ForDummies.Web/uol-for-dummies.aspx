﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UOL.Master" AutoEventWireup="true" CodeBehind="uol-for-dummies.aspx.cs" Inherits="ForDummies.Web.uol_for_dummies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="corpo-maior">
        <div class="corpo-maior-sub">
            <div class="container-o-que-e">
                <h1 class="titulo-oquee">O QUE É UOL FOR DUMMIES?</h1>
                <p class="texto-oquee">A série For Dummies surgiu em 1991 nos Estados Unidos e, desde então, já teve mais de 250 milhões de livros impressos.</p>
                <p class="texto-oquee">Distribuídos em cerca de 1800 títulos, os livros foram traduzidos para diversos idiomas e se tornaram populares por explicarem, de forma simples, como fazer tudo – cozinhar, planejar uma viagem, cuidar de um jardim e até mesmo administrar finanças.</p>
                <p class="texto-oquee">Agora, a famosa série chega no UOL em um formato digital e interativo. </p>
                <p class="texto-oquee">O UOL For Dummies oferece diversas opções de cursos online, voltados para quem deseja aprender algo novo.</p>
                <p class="texto-oquee">Isso tudo com o conforto de uma plataforma online, que permite o acesso ao conteúdo de qualquer computador ligado à internet, a qualquer hora.</p>
                <p class="texto-oquee">O que você gostaria de aprender? Comece agora!</p>            </div>
        </div>
    </div>
</asp:Content>