﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using libBrasilShop.Criptografia;
using System.Configuration;

namespace libUOL.CookieManager
{
    internal static class Cookie
    {
        #region Métodos Private

        private static string setValor(string valor)
        {
            using (Simetrica cripto = new Simetrica(TipoSimetrica.Rijndael))
            {
                cripto.Chave = ConfigurationManager.AppSettings["SecretKey"];
                return cripto.CriptografaQueryString(valor);
            }
        }

        private static string getValor(string valor)
        {
            using (Simetrica cripto = new Simetrica(TipoSimetrica.Rijndael))
            {
                cripto.Chave = ConfigurationManager.AppSettings["SecretKey"];
                return cripto.DescriptografaQueryString(valor);
            }
        }

        #endregion

        public static void RemoverCookie(string nomeCookie)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public static void SetCookie(string nomeCookie, string valor)
        {
            HttpCookie cookie = new HttpCookie(nomeCookie);
            cookie.Value = setValor(valor);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void SetCookie(string nomeCookie, string parametro, string valor)
        {
            parametro = setValor(parametro);
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                if (!string.IsNullOrEmpty(cookie.Values.Get(parametro)))
                    cookie.Values.Set(parametro, setValor(valor));
                else
                    cookie.Values.Add(parametro, setValor(valor));
            }
            else
            {
                cookie = new HttpCookie(nomeCookie);
                cookie.Values.Add(parametro, setValor(valor));
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static string GetCookie(string nomeCookie)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
                return getValor(cookie.Value);
            return string.Empty;
        }

        public static string GetCookie(string nomeCookie, string parametro)
        {
            parametro = setValor(parametro);
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                if (!string.IsNullOrEmpty(cookie.Values.Get(parametro)))
                    return getValor(cookie.Values.Get(parametro));
            }
            return string.Empty;
        }
    }
}
