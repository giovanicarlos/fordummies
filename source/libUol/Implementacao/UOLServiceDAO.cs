using System;
using System.Collections.Generic;
using System.Text;
using libBrasilShop.BancoDados;
using System.Data;

namespace libUOL.Implementacao
{
    public class UOLServiceDAO
    {
        public int ConsultarTotalMatriculaDisponivel(TO.enmTipoFormato tipoFormato, TO.Aluno toAluno, TO.Sistema toSistema)
        {
            DateTime Inicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime Fim = Inicio.AddMonths(1).AddDays(-1);
            using (BancoDados db = new BancoDados("st_ConsultarUolQtdMatriculas", CommandType.StoredProcedure))
            {
                db.CriaParametro("@TipoFormato", SqlDbType.Int, Convert.ToInt32(tipoFormato));
                db.CriaParametro("@DataInicio", SqlDbType.DateTime, Inicio);
                db.CriaParametro("@DataFim", SqlDbType.DateTime, Fim);
                db.CriaParametro("@IdAluno", SqlDbType.Int, toAluno.Id);
                db.CriaParametro("@IdSistema", SqlDbType.Int, toSistema.Id);
                object retorno = db.RetornaScalar();
                if (retorno != DBNull.Value)
                    return Convert.ToInt32(retorno);
                return 0;
            }
        }

        public int CadastrarLogErroMatriculaUol(int idPedido, int idPessoa, int idSistema, string mensagem, string exception)
        {
            using (BancoDados db = new BancoDados("st_CadastrarLogErroMatriculaUol", CommandType.StoredProcedure))
            {
                db.CriaParametro("@IdPedido", SqlDbType.Int, idPedido);
                db.CriaParametro("@IdPessoa", SqlDbType.Int, idPessoa);
                db.CriaParametro("@IdSistema", SqlDbType.Int, idSistema);
                db.CriaParametro("@Mensagem", SqlDbType.NVarChar, mensagem);
                db.CriaParametro("@Exception", SqlDbType.NVarChar, exception);
                db.CriaParametro("@Data", SqlDbType.DateTime, DateTime.Now);
                db.CriaParametroOutput("@IdLogErroMatriculaUol", SqlDbType.Int);
                if (db.ExecutaComando())
                    return Convert.ToInt32(db.ValorParametro("@IdLogErroMatriculaUol"));
                return 0;
            }
        }

        public int ConsultarCodigoCurso(int IdCurso)
        {
            using (BancoDados db = new BancoDados("st_ConsultarCodigoCursoUol", CommandType.StoredProcedure))
            {
                db.CriaParametro("@IdCurso", SqlDbType.Int, IdCurso);
                object retorno = db.RetornaScalar();
                if (retorno != DBNull.Value)
                    return Convert.ToInt32(retorno);
                return 0;
            }
        }
    }
}
