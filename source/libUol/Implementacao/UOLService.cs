using System;
using System.Collections.Generic;
using System.Text;
using libUOL.Interface;
using System.Threading;
using CrescaBrasilBL;
using System.Net.Mail;
using System.Configuration;
using libUOL.Security;

namespace libUOL.Implementacao
{
    public class UOLService : IUOLService
    {
        public bool ValidarUsuario(string valorCookie, TO.Sistema toSistema, out TO.Aluno toAluno)
        {
            toAluno = null;
            CrescaBrasilBL.Aluno blAluno = new CrescaBrasilBL.Aluno();

            toAluno = new CrescaBrasilBL.Aluno().ConsultarPorCodigoSistema(valorCookie, toSistema);
            if (toAluno != null)
            {
                return true;
            }
            else
            {
                toAluno = new TO.Aluno();
                toAluno.Nome = " ";
                toAluno.Cpf = "000.000.000-00";
                toAluno.Email = valorCookie + "@uol.com.br";
                toAluno.Sexo = 0;
                toAluno.Senha = valorCookie;
                toAluno.Codigo = valorCookie;
                toAluno.DataNascimento = DateTime.Now;
                toAluno.DataCadastro = DateTime.Now;
                toAluno.Enderecos = new List<TO.Endereco>();
                TO.Endereco end = new TO.Endereco();
                end.TituloEndereco = "Endere�o Principal";
                end.Logradouro = " ";
                end.Numero = " ";
                end.Bairro = " ";
                end.Complemento = " ";
                end.Cep = " ";
                end.Cidade = " ";
                end.Estado = " ";
                end.Pais = new TO.Pais(30);
                end.CodigoIBGE = " ";
                toAluno.Enderecos.Add(end);

                if (blAluno.Cadastrar(toAluno, toSistema))
                    if (blAluno.Cadastrar(toAluno, new TO.Sistema(true)))
                        return true;
                    else
                    {
                        toAluno = null;
                        return false;
                    }
            }
            return false;
        }

        public bool PermiteEfetuarMatricula(TO.enmTipoFormato TipoFormato, TO.Aluno toAluno, TO.Sistema toSistema)
        {
            if (PossuiAssinatura())
            {
                return QtdeMatriculaDisponivelParaAtivacao(TipoFormato, toAluno, toSistema) > 0;
            }
            return false;
        }

        public int QtdeMatriculaDisponivelParaAtivacao(TO.enmTipoFormato TipoFormato, TO.Aluno toAluno, TO.Sistema toSistema)
        {
            if (PossuiAssinatura())
            {
                return new UOLServiceDAO().ConsultarTotalMatriculaDisponivel(TipoFormato, toAluno, toSistema);
            }
            return 0;
        }

        public bool Matricular(TO.Pedido toPedido, TO.Sistema toSistema)
        {
            try
            {
                if (MatriculaUol(toPedido, toSistema, TO.enmTipoPedido.VendaAssinaturaUol))
                    return true;
                LogErroMatriculaUol(toPedido, toSistema, null);
                return false;
            }
            catch (Exception ex)
            {
                LogErroMatriculaUol(toPedido, toSistema, ex);
                throw;
            }
        }

        private bool MatriculaUol(TO.Pedido toPedido, TO.Sistema toSistema, TO.enmTipoPedido tipo)
        {
            TO.Matricula toMatricula = new TO.Matricula();
            if (new CrescaBrasilBL.Pedido().ComprarCursoOnlineUol(toPedido, toSistema, tipo, out toMatricula))
            {
                toPedido.DataPagamento = DateTime.Now;
                if (!new CrescaBrasilBL.Pedido().ConfirmarPagamento(toPedido, false))
                    return false;
                return true;
            }
            return false;
        }

        private void LogErroMatriculaUol(TO.Pedido toPedido, TO.Sistema toSistema, Exception ex)
        {
            int Idlog = 0;
            if (ex != null)
                Idlog = new UOLServiceDAO().CadastrarLogErroMatriculaUol(toPedido.Id, toPedido.Pessoa.Id, toSistema.Id, ex.Message, ex.StackTrace);
            else
                Idlog = new UOLServiceDAO().CadastrarLogErroMatriculaUol(toPedido.Id, toPedido.Pessoa.Id, toSistema.Id, "Ocorreu um erro nos processos de cadastro, mais que n�o gerou um Exception!", string.Empty);
            List<object> objetos = new List<object>();
            new EmailErro().EnviarEmailErroMatriculaDoUol(Idlog);
        }

        public bool ComprarOneShot(TO.Pedido toPedido, TO.Sistema toSistema, out string jsonEncoded)
        {
            jsonEncoded = string.Empty;
            try
            {
                TO.Matricula toMatricula = new TO.Matricula();
                if (new CrescaBrasilBL.Pedido().ComprarCursoOnlineUol(toPedido, toSistema, TO.enmTipoPedido.VendaCursoProduto, out toMatricula))
                {
                    Integracao.UolIntegracao uol = new Integracao.UolIntegracao();
                    if (uol.Comprar(toPedido, out jsonEncoded))
                    {
                        return true;
                    }
                }
                LogErroMatriculaUol(toPedido, toSistema, null);
                return false;
            }
            catch (Exception ex)
            {
                LogErroMatriculaUol(toPedido, toSistema, ex);
                throw;
            }
        }

        public void SetPermissaoUol(System.Web.HttpRequest httpRequest)
        {
            CookieManager.Cookie.SetCookie("ttyxkl", "OneShot", false.ToString());
            CookieManager.Cookie.SetCookie("ttyxkl", "Assinatura", false.ToString());
            string OneShot = ConfigurationManager.AppSettings["OpenIdOneShot"];
            string Assinatura = ConfigurationManager.AppSettings["OpenIdAssinatura"];

            string retornoUOL = string.Empty;

            foreach (var item in httpRequest.QueryString.AllKeys)
            {
                if (item.Contains("status." + OneShot))
                {
                    if (httpRequest.QueryString[item] == "1")
                        CookieManager.Cookie.SetCookie("ttyxkl", "OneShot", true.ToString());
                }
                if (item.Contains("status." + Assinatura))
                {
                    if (httpRequest.QueryString[item] == "1")
                        CookieManager.Cookie.SetCookie("ttyxkl", "Assinatura", true.ToString());
                }
                retornoUOL += "|" + item;
            }
            CookieManager.Cookie.SetCookie("dbgKJHYKK", retornoUOL);
        }

        public bool PossuiAssinatura()
        {
            string parametro = CookieManager.Cookie.GetCookie("ttyxkl", "Assinatura");
            if (!string.IsNullOrEmpty(parametro))
            {
                return Convert.ToBoolean(parametro);
            }
            return false;
        }

        public bool PossuiOneShot()
        {
            string parametro = CookieManager.Cookie.GetCookie("ttyxkl", "OneShot");
            if (!string.IsNullOrEmpty(parametro))
            {
                return Convert.ToBoolean(parametro);
            }
            return false;
        }

        public bool ValidaUsuarioSACA(string valorCookie, TO.Sistema toSistema, out TO.UOLSac toUolSac)
        {
            toUolSac = null;
            string chave = ConfigurationManager.AppSettings["HashKey"];
            string[] vCookie = UolCryptography.ToBase64Decode(valorCookie).Split('|');
            string login = vCookie[0];
            string date = vCookie[1];
            string auth = vCookie[2];
            string validationHash = vCookie[3];
            string data = string.Join("|", new string[3] { login, date, auth });
            string dataHash = UolCryptography.ToSha256Hash(data, chave);
            bool PermissaoSupervisor = false;
            bool PermissaoAtendente = false;

            if (auth.Contains("DUM_SACA_SUPERVISOR"))
                PermissaoSupervisor = true;
            else if (auth.Contains("DUM_SACA_ATENDENTE"))
                PermissaoAtendente = true;

            validarHash(validationHash, dataHash, UolCryptography.ToBase64Decode(valorCookie));
            validaPermissao(PermissaoSupervisor, PermissaoAtendente, UolCryptography.ToBase64Decode(valorCookie));

            CrescaBrasilBL.UOLSac blUolSac = new CrescaBrasilBL.UOLSac();

            toUolSac = new CrescaBrasilBL.UOLSac().ConsultarPorCodigoSistema(login, toSistema);
            if (toUolSac != null)
            {
                if (PermissaoSupervisor && !toUolSac.TipoPermissao)
                {
                    toUolSac.TipoPermissao = true;
                    new CrescaBrasilBL.UOLSac().Alterar(toUolSac);
                }
                else if (PermissaoAtendente && toUolSac.TipoPermissao)
                {
                    toUolSac.TipoPermissao = false;
                    new CrescaBrasilBL.UOLSac().Alterar(toUolSac);
                }
                return true;
            }
            else
            {
                toUolSac = new TO.UOLSac();
                toUolSac.Nome = " ";
                toUolSac.Cpf = "000.000.000-00";
                toUolSac.Email = login + "@uol.com.br";
                toUolSac.Sexo = 0;
                toUolSac.Senha = login;
                toUolSac.Codigo = login;
                toUolSac.DataNascimento = DateTime.Now;
                toUolSac.DataCadastro = DateTime.Now;
                toUolSac.Enderecos = new List<TO.Endereco>();
                TO.Endereco end = new TO.Endereco();
                end.TituloEndereco = "Endere�o Principal";
                end.Logradouro = " ";
                end.Numero = " ";
                end.Bairro = " ";
                end.Complemento = " ";
                end.Cep = " ";
                end.Cidade = " ";
                end.Estado = " ";
                end.Pais = new TO.Pais(30);
                end.CodigoIBGE = " ";
                toUolSac.Enderecos.Add(end);
                //Se n�o for supervisor cadastra 0
                toUolSac.TipoPermissao = PermissaoSupervisor;

                if (blUolSac.Cadastrar(toUolSac, toSistema))
                    return true;
                else
                {
                    toUolSac = null;
                    return false;
                }
            }
        }

        private void validarHash(string hashPassado, string hashGerado, string cookie)
        {
            if (hashPassado != hashGerado)
            {
                string msg = "<u>Hash inv�lido</u>" + Environment.NewLine;
                msg += "Hash passado: " + hashPassado + Environment.NewLine;
                msg += "Hash gerado: " + hashGerado + Environment.NewLine;
                msg += "Cookie: " + cookie;
                throw new Exception(msg);
            }
        }

        private void validaPermissao(bool Supervisor, bool Atendente, string cookie)
        {
            if (!Supervisor && !Atendente)
            {
                throw new Exception("<u>Usu�rio sem permiss�o</u>" + Environment.NewLine + "Valor Cookie: " + cookie);
            }
        }

    }
}
