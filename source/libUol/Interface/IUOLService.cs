using System;
using System.Collections.Generic;
using System.Text;

namespace libUOL.Interface
{
    public interface IUOLService
    {
        /// <summary>
        /// Verifica a exist�ncia do aluno atrav�s do valorCookie salvo no campo C�digo da tblAluno.
        /// Caso o aluno exista retorna true, sen�o o aluno � cadastrado.
        /// </summary>
        bool ValidarUsuario(string valorCookie, TO.Sistema toSistema, out TO.Aluno toAluno);
        /// <summary>
        /// Verifica a quantidade de matricula que o aluno pode efetuar para o tipo formato informado.
        /// Retorna true para a quantidade > 0.
        /// </summary>
        bool PermiteEfetuarMatricula(TO.enmTipoFormato TipoFormato, TO.Aluno toAluno, TO.Sistema toSistema);

        /// <summary>
        /// Retorna a quantidade de matriculas que o aluno pode efetuar de acordo com o tipo de formato do curso.
        /// </summary>
        int QtdeMatriculaDisponivelParaAtivacao(TO.enmTipoFormato TipoFormato, TO.Aluno toAluno, TO.Sistema toSistema);

        /// <summary>
        /// Efetua a matricula no pedido
        /// </summary>
        /// <param name="toPedido"></param>
        /// <param name="toSistema"></param>
        /// <returns></returns>
        bool Matricular(TO.Pedido toPedido, TO.Sistema toSistema);

        bool ComprarOneShot(TO.Pedido toPedido, TO.Sistema toSistema, out string jsonEncoded);

        void SetPermissaoUol(System.Web.HttpRequest httpRequest);

        bool PossuiAssinatura();

        bool PossuiOneShot();

        bool ValidaUsuarioSACA(string valorCookie, TO.Sistema toSistema, out TO.UOLSac toUolSac);
    }
}
