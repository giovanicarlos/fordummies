﻿using System;
using System.Collections.Generic;
using System.Text;

namespace libUOL.Dominio
{
    internal class Order
    {
        public List<OrderItem> items { get; set; }
        public string callback { get; set; }
        public string error_callback { get; set; }
        public List<OrderItem> additions { get; set; }
        public List<OrderItem> discounts { get; set; }
        public Dictionary<string, string> custom { get; set; }

        public Order()
        {

        }

        public Order(TO.Pedido toPedido, string urlRetorno, string urlRetornoErro)
        {
            this.callback = urlRetorno;
            this.error_callback = urlRetornoErro;
            
            this.custom = new Dictionary<string, string>();
            this.custom.Add("IdPedido", toPedido.Id.ToString());

            this.items = new List<OrderItem>();
            foreach (TO.ItemPedidoCurso item in toPedido.Itens)
            {
                this.items.Add(new OrderItem(item));
            }
        }
    }
}
