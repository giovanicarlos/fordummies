﻿using System;
using System.Collections.Generic;
using System.Text;
using libUOL.Implementacao;

namespace libUOL.Dominio
{
    internal class OrderItem
    {
        public int id { get; set; }
        public string description { get; set; }
        public decimal value { get; set; }
        public Dictionary<string, string> custom { get; set; }

        public OrderItem()
        {

        }

        public OrderItem(TO.ItemPedidoCurso item)
        {
            this.id = new UOLServiceDAO().ConsultarCodigoCurso(item.Curso.Id);
            this.description = item.Curso.Nome;
            this.value = item.Valor;

            this.custom = new Dictionary<string, string>();
            this.custom.Add("IdItemPedido", item.Id.ToString());
        }
    }
}
