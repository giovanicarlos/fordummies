﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;
using LinqKit;

namespace ForDummies.Infra
{
    public class MatriculaRepositorio : BaseRepositorio<Matricula>, IMatriculaRepositorio
    {
        public MatriculaRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }

        public IEnumerable<Matricula> ConsultarMatriculas(int alunoId)
        {
            return this.GetAll().Where(x => x.AlunoId == alunoId);
        }

        /// <summary>
        /// Retornar as matriculas menos cancelada, bloqueada e pendente cresça ou filtra por status
        /// </summary>
        /// <param name="alunoId"></param>
        /// <param name="statusMatricula"></param>
        /// <returns></returns>
        public IEnumerable<Matricula> ConsultarMatriculasVisiveisCliente(int alunoId, enmStatusMatricula? statusMatricula)
        {
            var predicate = PredicateBuilder.True<Matricula>();

            predicate = predicate.And(x => x.AlunoId.Equals(alunoId) && x.Status != (byte)enmStatusMatricula.Bloqueado && x.Status != (byte)enmStatusMatricula.Cancelado && x.Status != (byte)enmStatusMatricula.PendenteCriarCresca);

            if (statusMatricula != null)
                predicate = predicate.And(x => x.Status == (byte)statusMatricula);

            return dbset.AsExpandable().Where(predicate).OrderByDescending(x => x.DataCadastro);
        }

        public IEnumerable<Matricula> ConsultarMatriculas(int alunoId, enmStatusMatricula statusMatricula)
        {
            return this.dbset.Where(x => x.AlunoId.Equals(alunoId) && x.Status == (byte)statusMatricula);
        }

        public IEnumerable<Matricula> ConsultarMatriculas(int[] matriculaIdCB)
        {
            return this.GetAll().Where(x => matriculaIdCB.Contains(x.MatriculaIdCB));
        }
    }
}
