﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Infra.Contexto;
using ForDummies.Dominio.Interface;

namespace ForDummies.Infra
{
    public class ItemPedidoOneShotRepositorio: BaseRepositorio<ItemPedidoOneShot>, IItemPedidoOneShotRepositorio
    {
        public ItemPedidoOneShotRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }
    }
}
