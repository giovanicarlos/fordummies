﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.Infra
{
    public class ClassificacaoRepositorio : BaseRepositorio<Classificacao>, IClassificacaoRepositorio
    {
        public ClassificacaoRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }
    }
}
