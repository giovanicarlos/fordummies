﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.Infra
{
    public class PessoaRepositorio: BaseRepositorio<Pessoa>, IPessoaRepositorio
    {
        public PessoaRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }
    }
}
