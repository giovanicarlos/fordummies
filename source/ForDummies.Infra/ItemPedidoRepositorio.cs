﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;
using ForDummies.Dominio;

namespace ForDummies.Infra
{
    public class ItemPedidoRepositorio : BaseRepositorio<ItemPedido>, IItemPedidoRepositorio
    {
        public ItemPedidoRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }
    }
}
