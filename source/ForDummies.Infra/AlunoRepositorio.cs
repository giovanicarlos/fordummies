﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Infra.Contexto;
using ForDummies.Dominio.Interface;

namespace ForDummies.Infra
{
    public class AlunoRepositorio : BaseRepositorio<Aluno>, IAlunoRepositorio
    {

        public AlunoRepositorio(IGerenciaContexto gerenciaContexto) : base(gerenciaContexto)
        {

        }

    }
}
