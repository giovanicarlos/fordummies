﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Infra.Contexto;
using ForDummies.Dominio;
using ForDummies.Dominio.Interface;

namespace ForDummies.Infra
{
    public class CursoRepositorio : BaseRepositorio<Curso>, ICursoRepositorio
    {

        private readonly IClassificacaoRepositorio classificacaoRepositorio;
        private readonly IMatriculaRepositorio matriculaRespositorio;

        public CursoRepositorio(IGerenciaContexto gerenciaContexto, IClassificacaoRepositorio classificacaoRepositorio, IMatriculaRepositorio matriculaRespositorio)
            : base(gerenciaContexto)
        {
            this.classificacaoRepositorio = classificacaoRepositorio;
            this.matriculaRespositorio = matriculaRespositorio;
        }

        public IEnumerable<Curso> ConsultarCursos(int classificacaoId, int formatoId, bool destaque, int paginaAtual, int totalRegistrosPagina)
        {
            Classificacao classificacao = classificacaoRepositorio.GetById(classificacaoId);

            return classificacao.Cursos.Where(x => x.Status == (byte)enmStatusCurso.Ativo && x.FormatoId == formatoId && x.Destaque == destaque).
                OrderBy(x => x.CursoId).
                Skip((paginaAtual - 1) * totalRegistrosPagina).
                Take(totalRegistrosPagina);
        }

        public IEnumerable<Curso> Consultarcursos(int formatoId, bool destaque, int paginaAtual, int totalRegistrosPagina)
        {
            return this.GetAll().Where(x => x.Status == (byte)enmStatusCurso.Ativo && x.FormatoId == formatoId && x.Destaque == destaque).
                OrderBy(x => x.CursoId).
                Skip((paginaAtual - 1) * totalRegistrosPagina).
                Take(totalRegistrosPagina);
        }

        public IEnumerable<Curso> Consultarcursos(bool destaque, int paginaAtual, int totalRegistrosPagina)
        {
            return this.GetAll().Where(x => x.Status == (byte)enmStatusCurso.Ativo && x.Destaque == destaque).
                OrderBy(x => x.CursoId).
                Skip((paginaAtual - 1) * totalRegistrosPagina).
                Take(totalRegistrosPagina);
        }

        public IQueryable<Curso> ConsultarTop(int quantidade, int? classificacaoId)
        {
            /*ForDummiesContexto _contexto = base.getContexto;

            var query = (from curso in _contexto.Curso
                         join itemPedido in _contexto.ItemPedido on curso.CursoId equals itemPedido.CursoId
                         join classificacao in _contexto.Classificacao on curs
                         where curso.Status == (byte)enmStatusCurso.Ativo && curso.Classificacoes.Where(
                         group curso by new { curso } into resultado
                         let total = resultado.Count()
                         orderby total descending
                         select new Curso
                         {
                             CursoId = resultado.Key.curso.CursoId
                         }).Take(quantidade);

            return query.Distinct().AsQueryable();*/

            return this.GetAll().OrderByDescending(x => x.DataAtivacao).Take(5);
        }

        public IEnumerable<Curso> ConsultarCursos(string textoBusca, int? formato, int paginaAtual, int totalRegistrosPagina, out int total)
        {
            IQueryable<Curso> cursos;
            if (formato != null)
            {
                cursos = this.GetAll().Where(x => x.Nome.Contains(textoBusca) || x.Descricao.Contains(textoBusca) && x.Status == (byte)enmStatusCurso.Ativo && x.FormatoId == formato).
                    OrderBy(x => x.CursoId).
                    Skip((paginaAtual - 1) * totalRegistrosPagina).
                    Take(totalRegistrosPagina);
            }
            else
            {
                cursos = this.GetAll().Where(x => x.Nome.Contains(textoBusca) || x.Descricao.Contains(textoBusca) && x.Status == (byte)enmStatusCurso.Ativo).
                    OrderBy(x => x.CursoId).
                    Skip((paginaAtual - 1) * totalRegistrosPagina).
                    Take(totalRegistrosPagina);
            }

            total = cursos.Count();
            return cursos;
            
        }

        public Curso Consultar(int matriculaId)
        {
            Matricula matricula = matriculaRespositorio.GetById(matriculaId);
            return matricula.Curso;
        }
    }
}
