﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Infra.Contexto
{
    public interface IGerenciaContexto
    {
        ForDummiesContexto Get();
    }
}
