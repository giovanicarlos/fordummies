﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Infra.Contexto
{
    public class UnitOfWork : IUnitOfWork 
    {
        private readonly IGerenciaContexto databaseFactory;
        private ForDummiesContexto dataContext;

        public UnitOfWork(IGerenciaContexto databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        protected ForDummiesContexto DataContext
        {
            get { return dataContext ?? (dataContext = (ForDummiesContexto)databaseFactory.Get()); }
        }

        public void Commit()
        {
            DataContext.Commit();
        }
    }
}
