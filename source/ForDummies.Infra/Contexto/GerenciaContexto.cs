﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.Infra.Contexto
{
    public class GerenciaContexto : IGerenciaContexto
    {
        private ForDummiesContexto _contexto;

        public ForDummiesContexto Get()
        {
            if (_contexto == null)
            {
                _contexto = new ForDummiesContexto();
            }

            return _contexto;
        }
    }
}
