﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using ForDummies.Dominio;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ForDummies.Infra.Contexto
{
    public class ForDummiesContexto : DbContext, IDisposable
    {
        public DbSet<Aluno> Aluno { get; set; }
        public DbSet<Classificacao> Classificacao { get; set; }
        public DbSet<Conteudo> Conteudo { get; set; }
        public DbSet<Curso> Curso { get; set; }
        public DbSet<ItemPedido> ItemPedido { get; set; }
        public DbSet<Matricula> Matricula { get; set; }
        public DbSet<Pessoa> Pessoa { get; set; }
        public DbSet<LogAcesso> LogAcesso { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
        public DbSet<Formato> Formato { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        public new void Dispose()
        {
            base.Dispose();
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
                        
            modelBuilder.Entity<Aluno>().ToTable("Aluno");
            
            modelBuilder.Entity<Curso>().HasMany(x => x.Classificacoes).WithMany(x => x.Cursos).Map(x => x.MapLeftKey("CursoId").MapRightKey("ClassificacaoId").ToTable("CursosClassificacoes"));
                        
            modelBuilder.Entity<Aluno>().HasMany(x => x.Pedidos).WithRequired(x=> x.Aluno).HasForeignKey(x => x.AlunoId);
            modelBuilder.Entity<Aluno>().HasMany(x => x.Matriculas).WithRequired(x=> x.Aluno).HasForeignKey(x => x.AlunoId);
                                    
            modelBuilder.Entity<Matricula>().HasRequired(x => x.Aluno).WithMany().HasForeignKey(x => x.AlunoId);
            modelBuilder.Entity<Pedido>().HasRequired(x => x.Aluno).WithMany().HasForeignKey(x => x.AlunoId);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
