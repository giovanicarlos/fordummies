﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.Infra
{
    public class ConteudoRepositorio : BaseRepositorio<Conteudo>, IConteudoRepositorio
    {
        public ConteudoRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }

        public IEnumerable<Conteudo> ListarConteudos(int cursoId)
        {
            return dbset.Where(x => x.CursoId == cursoId);
        }
    }
}
