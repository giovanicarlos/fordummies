﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.Infra
{
    public class ItemPedidoAssinaturaRepositorio: BaseRepositorio<ItemPedidoAssinatura>, IItemPedidoAssinaturaRepositorio
    {

        public ItemPedidoAssinaturaRepositorio(IGerenciaContexto gerenciaContexto)
            : base(gerenciaContexto)
        {

        }

    }
}
