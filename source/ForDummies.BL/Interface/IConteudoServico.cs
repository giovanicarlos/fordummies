﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;

namespace ForDummies.BL.Interface
{
    public interface IConteudoServico : IBaseServico<Conteudo>
    {
        List<Conteudo> ListarConteudos(int cursoId);
    }
}
