﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace ForDummies.BL.Interface
{
    public interface IBaseServico<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(int Id);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Func<T, object> order, int pageSize, int pageIndex, out int totalPages);
        IEnumerable<T> GetAllDescending(Func<T, object> order, int pageSize, int pageIndex, out int totalPages);
        void Reload(T entity);
        int Count(Func<T, bool> where);
        void Reload(T entity, Expression<Func<T, object>> attribute);
    }
}
