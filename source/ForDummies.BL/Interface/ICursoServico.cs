﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;

namespace ForDummies.BL.Interface
{
    public interface ICursoServico : IBaseServico<Curso>
    {
        List<Curso> ConsultarCursos(int classificacaoId, int formato, bool destaque, int paginaAtual, int totalRegistrosPagina);
        List<Curso> ConsultarCursos(int formato, bool destaque, int paginaAtual, int totalRegistrosPagina);
        List<Curso> ConsultarCursos(bool destaque, int paginaAtual, int totalRegistrosPagina);
        List<Curso> ConsultarCursos(string textoBusca, int? formato, int paginaAtual, int totalRegistrosPagina, out int total);
        Curso Consultar(string urlSeo);
        Curso Consultar(int matriculaId);
        List<Curso> ConsultarTop(int quantidade, int? classificacaoId);
        Classificacao ConsultarClassificacao(int cursoId);
    }
}
