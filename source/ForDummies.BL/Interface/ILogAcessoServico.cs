﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;

namespace ForDummies.BL.Interface
{
    public interface ILogAcessoServico : IBaseServico<LogAcesso>
    {
        void LogarAcesso(int alunoId, string ip);
        bool ValidaAcessoAluno();
        void FinalizarSessao(int alunoId, string ip);
    }
}
