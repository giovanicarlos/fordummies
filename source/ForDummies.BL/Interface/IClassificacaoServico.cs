﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;

namespace ForDummies.BL.Interface
{
    public interface IClassificacaoServico : IBaseServico<Classificacao>
    {
        List<Classificacao> ConsultarClassificacoes();
        Classificacao Consultar(string urlSeo);
        Classificacao Consultar(int cursoId);
    }
}
