﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;

namespace ForDummies.BL.Interface
{
    public interface IMatriculaServico : IBaseServico<Matricula>
    {
        int QuantidadeMatricula(int alunoId, int formatoId, DateTime dataInicial, DateTime dataFinal);
        List<Matricula> ConsultarMatriculas(int alunoId);
        List<Matricula> ConsultarMatriculas(int alunoId, enmStatusMatricula enmStatusMatricula);
        int QuantidadeMatriculaPermitida(int alunoId, int formatoId, DateTime dataInicial, DateTime dataFinal);
        int QuantidadeMatriculaMesAtual(int alunoId, int formatoId);
        int QuantidadeMatriculaPermitidaMesAtual(int alunoId, int formatoId);
        bool Matricular(int pedidoId);
        bool PermiteMatricular(int alunoId, int formatoId);
        void AtualizarMatriculas(int alunoId);
    }
}
