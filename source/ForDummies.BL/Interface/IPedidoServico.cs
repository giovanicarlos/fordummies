﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;

namespace ForDummies.BL.Interface
{
    public interface IPedidoServico : IBaseServico<Pedido>
    {
        Pedido CriarPedidoAssinatura(int alunoId, int cursoId);
    }
}
