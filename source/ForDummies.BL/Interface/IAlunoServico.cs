﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL;

namespace ForDummies.BL.Interface
{
    public interface IAlunoServico : IBaseServico<Aluno>
    {
        Aluno Validar(string email, string senha);
        Aluno ConsultarPorCodigo(string codigo);
    }
}
