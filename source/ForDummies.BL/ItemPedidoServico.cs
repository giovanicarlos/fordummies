﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class ItemPedidoServico : BaseServico<ItemPedido>, IItemPedidoServico
    {
        private readonly IItemPedidoRepositorio itemPedidoRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public ItemPedidoServico(IItemPedidoRepositorio itemPedidoRepositorio, IUnitOfWork unitOfWork)
            : base(itemPedidoRepositorio, unitOfWork)
        {
            this.itemPedidoRepositorio = itemPedidoRepositorio;
            this.unitOfwork = unitOfWork;
        }
    }
}
