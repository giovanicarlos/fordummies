﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class ConteudoServico : BaseServico<Conteudo>, IConteudoServico
    {
        private readonly IConteudoRepositorio alunoRepositorio;
     
        private readonly IUnitOfWork unitOfwork;

        public ConteudoServico(IConteudoRepositorio alunoRepositorio, IUnitOfWork unitOfWork)
            : base(alunoRepositorio, unitOfWork)
        {
            
            this.alunoRepositorio = alunoRepositorio;
            this.unitOfwork = unitOfWork;
        }

        public List<Conteudo> ListarConteudos(int cursoId)
        {
            return new List<Conteudo>();
        }
    }
}
