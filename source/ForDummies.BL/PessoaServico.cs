﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class PessoaServico : BaseServico<Pessoa>, IPessoaServico
    {
        private readonly IPessoaRepositorio pessoaRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public PessoaServico(IPessoaRepositorio pessoaRepositorio, IUnitOfWork unitOfWork)
            : base(pessoaRepositorio, unitOfWork)
        {
            this.pessoaRepositorio = pessoaRepositorio;
            this.unitOfwork = unitOfWork;
        }

    }
}
