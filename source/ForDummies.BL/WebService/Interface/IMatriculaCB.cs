﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService.Interface
{
    public interface IMatriculaCB
    {
        string CadastrarMatricula(int versao, string xml);
        string CancelarMatricula(int versao, string xml);
        string ConsultarMatricula(int versao, string xml);
        string ConsultarMatriculas(int versao, string xml);
    }
}
