﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService.Interface
{
    public static class FactoryMatriculaCB
    {
        public static IMatriculaCB GetMatriculaCB()
        {
            string servidor = System.Configuration.ConfigurationManager.AppSettings["servidorcb"];
            string email = System.Configuration.ConfigurationManager.AppSettings["emailcb"];
            string senha = System.Configuration.ConfigurationManager.AppSettings["senhacb"];

            switch (servidor)
            {
                case "local":
                    return new MatriculaCBLocal(email, senha);
                case "dev":
                    return new MatriculaCBDev(email, senha);
                case "beta":
                    return new MatriculaCBBeta(email, senha);
                case "producao":
                    return new MatriculaCBProducao(email, senha);
                default:
                    return new MatriculaCBProducao(email, senha);
            }
        }
    }
}
