﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.BL.api.crescabrasil.matricula.dev;

namespace ForDummies.BL.WebService.Interface
{
    public class MatriculaCBDev: IMatriculaCB
    {
        private Matricula _matricula;

        public MatriculaCBDev(string email, string senha)
        {
            this._matricula = new Matricula();
            AutenticacaoServico autenticacao = new AutenticacaoServico();
            autenticacao.email = email;
            autenticacao.senha = senha;

            this._matricula.AutenticacaoServicoValue = autenticacao;
        }

        public string CadastrarMatricula(int versao, string xml)
        {
            return _matricula.CadastrarMatricula(versao, xml).OuterXml;
        }

        public string CancelarMatricula(int versao, string xml)
        {
            return _matricula.CancelarMatricula(versao, xml).OuterXml;
        }

        public string ConsultarMatricula(int versao, string xml)
        {
            return _matricula.ConsultarMatricula(versao, xml).OuterXml;
        }

        public string ConsultarMatriculas(int versao, string xml)
        {
            return _matricula.ConsultarMatriculas(versao, xml).OuterXml;
        }
    }
}
