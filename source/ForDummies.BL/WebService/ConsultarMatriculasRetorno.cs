﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("matriculas", Namespace = "", IsNullable = false)]
    public class ConsultarMatriculasRetorno
    {
        private MatriculaRetorno[] matriculasField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("matricula")]
        public MatriculaRetorno[] Matriculas
        {
            get
            {
                return this.matriculasField;
            }
            set
            {
                this.matriculasField = value;
            }
        }

        public int[] GetMatriculasCBId()
        {
            if(Matriculas.Length > 0){
                int[] idMatriculas = new int[Matriculas.Length];

                for (int i = 0; i < Matriculas.Length; i++)
                {
                    idMatriculas[i] = Matriculas[i].IdMatricula;
                }
                return idMatriculas;
            }
            return null;
        }
    }

    public class MatriculaRetorno
    {
        private int idMatriculaField;
        private bool avaliacaoDisponivelField;
        private bool certificadoDisponivelField;
        private bool respondeuPesquisaField;
        private string notaField;
        private string dataExpiracaoField;
        private string percentualConcluidoField;
        private Status statusField;

        [System.Xml.Serialization.XmlElementAttribute("id_matricula")]
        public int IdMatricula
        {
            get
            {
                return this.idMatriculaField;
            }
            set
            {
                this.idMatriculaField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("avaliacao_disponivel")]
        public bool AvaliacaoDisponivel
        {
            get
            {
                return this.avaliacaoDisponivelField;
            }
            set
            {
                this.avaliacaoDisponivelField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("certificado_disponivel")]
        public bool CertificadoDisponivel
        {
            get
            {
                return this.certificadoDisponivelField;
            }
            set
            {
                this.certificadoDisponivelField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute("respondeu_pesquisa")]
        public bool RespondeuPesquisa
        {
            get
            {
                return this.respondeuPesquisaField;
            }
            set
            {
                this.respondeuPesquisaField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("nota")]
        public string Nota
        {
            get
            {
                return this.notaField;
            }
            set
            {
                this.notaField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("data_expiracao")]
        public string DataExpiracao
        {
            get
            {
                return this.dataExpiracaoField;
            }
            set
            {
                this.dataExpiracaoField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("percentual_concluido")]
        public string PercentualConcluido
        {
            get
            {
                return this.percentualConcluidoField;
            }
            set
            {
                this.percentualConcluidoField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("status")]
        public Status Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    public class Status
    {
        private int codigoField;
        private string descricaoField;

        [System.Xml.Serialization.XmlElementAttribute("codigo")]
        public int Codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("descricao")]
        public string Descricao
        {
            get
            {
                return this.descricaoField;
            }
            set
            {
                this.descricaoField = value;
            }
        }
    }
}
