﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService
{
    public class ValidacaoXML
    {
        private bool _sucesso;
        private StringBuilder _erros;

        public ValidacaoXML()
        {
            _erros = new StringBuilder();
            _sucesso = false;
        }

        /// <summary>
        /// foi validado com sucesso?
        /// </summary>
        public bool Sucesso
        {
            get { return _sucesso; }
            set { _sucesso = value; }
        }

        /// <summary>
        /// string com o possivel erro na validacao
        /// </summary>
        public string Erros
        {
            get
            {
                return _erros.ToString();
            }
        }

        public void AdicionarErro(string erro)
        {
            _erros.Append(erro);
            _erros.Append("\n");
        }
    }
}
