﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.BL.api.crescabrasil.matricula;
using System.Xml;
using ForDummies.BL.WebService.Interface;
using ForDummies.Dominio;

namespace ForDummies.BL.WebService
{
    public class MatriculaCBServico
    {
        private IMatriculaCB _matricula;
        private ServicoXML _servicoXML;
        private int _versao;

        public MatriculaCBServico()
        {
            _matricula = FactoryMatriculaCB.GetMatriculaCB();
            this._versao = 7;
            this._servicoXML = new ServicoXML();       
        }

        public CadastrarMatriculaRetorno CadastrarMatricula(CadastrarMatricula cadastrarMatricula)
        {
            string xml = _servicoXML.SerializarObjeto<CadastrarMatricula>(cadastrarMatricula);

            string xmlRetorno = _matricula.CadastrarMatricula(_versao, xml);
            try
            {
                return _servicoXML.DeserializarObjeto<CadastrarMatriculaRetorno>(xmlRetorno);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Retorno Web Service -" + xmlRetorno);
            }
        }

        public ConsultarMatriculasRetorno ConsultarMatriculas(ConsultarMatriculas consultarMatriculas)
        {
            string xml = _servicoXML.SerializarObjeto<ConsultarMatriculas>(consultarMatriculas);

            string xmlRetorno = _matricula.ConsultarMatriculas(_versao, xml);
            try
            {
                return _servicoXML.DeserializarObjeto<ConsultarMatriculasRetorno>(xmlRetorno);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro Retorno Web Service -" + xmlRetorno);
            }
        }
    }
}
