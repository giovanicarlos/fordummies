﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("matricula", Namespace = "", IsNullable = false)]
    public class CadastrarMatricula
    {

        private int id_cursoField;

        private int formatoField;

        private AlunoCB alunoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("id_curso")]
        public int IdCurso
        {
            get
            {
                return this.id_cursoField;
            }
            set
            {
                this.id_cursoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("formato")]
        public int Formato
        {
            get
            {
                return this.formatoField;
            }
            set
            {
                this.formatoField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("aluno")]
        public AlunoCB Aluno
        {
            get
            {
                return this.alunoField;
            }
            set
            {
                this.alunoField = value;
            }
        }

        public CadastrarMatricula(AlunoCB aluno, int cursoIdCB, int formatoId)
        {
            this.Aluno = aluno;
            this.IdCurso = cursoIdCB;
            SetFormatoCB(formatoId);
        }

        private void SetFormatoCB(int formatoId)
        {
            switch(formatoId){
                case 1:
                 this.Formato = 0;
                break;

            }
        }

        public CadastrarMatricula()
        {

        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public class AlunoCB
    {

        private string nomeField;

        private string cpfField;

        private string logradouroField;

        private string numeroField;

        private string complementoField;

        private string bairroField;

        private string cidadeField;

        private string ufField;

        private string cepField;

        private string telefoneField;

        private string emailField;

        private string senhaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("nome")]
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("cpf")]
        public string CPF
        {
            get
            {
                return this.cpfField;
            }
            set
            {
                this.cpfField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("logradouro")]
        public string Logradouro
        {
            get
            {
                return this.logradouroField;
            }
            set
            {
                this.logradouroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("numero")]
        public string Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("complemento")]
        public string Complemento
        {
            get
            {
                return this.complementoField;
            }
            set
            {
                this.complementoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("bairro")]
        public string Bairro
        {
            get
            {
                return this.bairroField;
            }
            set
            {
                this.bairroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("cidade")]
        public string Cidade
        {
            get
            {
                return this.cidadeField;
            }
            set
            {
                this.cidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("uf")]
        public string UF
        {
            get
            {
                return this.ufField;
            }
            set
            {
                this.ufField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("cep")]
        public string CEP
        {
            get
            {
                return this.cepField;
            }
            set
            {
                this.cepField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("telefone")]
        public string Telefone
        {
            get
            {
                return this.telefoneField;
            }
            set
            {
                this.telefoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("email")]
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("senha")]
        public string Senha
        {
            get
            {
                return this.senhaField;
            }
            set
            {
                this.senhaField = value;
            }
        }

        public AlunoCB()
        {

        }


        public AlunoCB(string nome, string email, string senha)
        {
            this.Nome = nome;
            this.CPF = "000.000.000-00";
            this.Email = email;
            this.Senha = senha;
            this.Logradouro = " ";
            this.Numero = "0";
            this.Bairro = " ";
            this.Complemento = " ";
            this.CEP = " ";
            this.Cidade = " ";
            this.Telefone = " ";
            this.UF = " ";
        }
    }
}
