﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("matricula", Namespace = "", IsNullable = false)]
    public class CadastrarMatriculaRetorno
    {

        private int id_matriculaField;

        private int id_alunoField;

        private string url_avaField;

        private string url_certificadoField;

        private string url_pesquisaField;

        private bool retornoField;

        private bool respondeuPesquisaField;

        private string mensagemField;

        [System.Xml.Serialization.XmlElementAttribute("id_aluno")]
        public int IdAluno
        {
            get
            {
                return this.id_alunoField;
            }
            set
            {
                this.id_alunoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("id_matricula")]
        public int IdMatricula
        {
            get
            {
                return this.id_matriculaField;
            }
            set
            {
                this.id_matriculaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("url_ava")]
        public string URLAva
        {
            get
            {
                return this.url_avaField;
            }
            set
            {
                this.url_avaField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("url_certificado")]
        public string UrlCertificado
        {
            get
            {
                return this.url_certificadoField;
            }
            set
            {
                this.url_certificadoField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("url_pesquisa")]
        public string UrlPesquisa
        {
            get
            {
                return this.url_pesquisaField;
            }
            set
            {
                this.url_pesquisaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("respondeu_pesquisa")]
        public bool RespondeuPesquisa
        {
            get
            {
                return this.respondeuPesquisaField;
            }
            set
            {
                this.respondeuPesquisaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("retorno")]
        public bool Retorno
        {
            get
            {
                return this.retornoField;
            }
            set
            {
                this.retornoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mensagem")]
        public string Mensagem
        {
            get
            {
                return this.mensagemField;
            }
            set
            {
                this.mensagemField = value;
            }
        }

        public override string ToString()
        {
            return "Matricula: " + id_matriculaField + "  Retorno: " + retornoField;
        }
    }
}
