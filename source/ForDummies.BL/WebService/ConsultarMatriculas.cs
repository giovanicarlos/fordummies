﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForDummies.BL.WebService
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("matricula", Namespace = "", IsNullable = false)]
    public class ConsultarMatriculas
    {

        private int IdAlunoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("id_aluno")]
        public int IdAluno
        {
            get
            {
                return this.IdAlunoField;
            }
            set
            {
                this.IdAlunoField = value;
            }
        }

        public ConsultarMatriculas()
        {

        }

        public ConsultarMatriculas(int idAluno)
        {
            this.IdAlunoField = idAluno;
        }
    }
}
