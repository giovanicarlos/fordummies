﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace ForDummies.BL.WebService
{
    public class ServicoXML
    {
        public string SerializarObjeto<T>(T objeto)
        {
            return SerializarTipoObject(objeto, Encoding.UTF8);
        }

        public string SerializarObjeto<T>(T objeto, Encoding encode)
        {
            return SerializarTipoObject(objeto, encode);
        }

        public string SerializarTipoObject(object classe)
        {
            return SerializarTipoObject(classe, Encoding.UTF8);
        }

        public string SerializarTipoObject(object classe, Encoding encode)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(classe.GetType());
                System.IO.Stream stream = new System.IO.MemoryStream();
                System.Xml.XmlTextWriter xtWriter = new System.Xml.XmlTextWriter(stream, encode);
                serializer.Serialize(xtWriter, classe);
                xtWriter.Flush();
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                System.IO.StreamReader reader = new System.IO.StreamReader(stream, encode);
                string result = reader.ReadToEnd();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi Possível Serializar o Objeto", ex);
            }
        }

        public T DeserializarObjeto<T>(string xmlParametros)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlParametros);
                byte[] b = Encoding.UTF8.GetBytes(xmlParametros);
                MemoryStream m = new MemoryStream(b);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(m);
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi Possível Deserializar o Objeto", ex);
            }
        }

        public ValidacaoXML validarXML(string nameSpaceXSD, string pathXSD, string xmlParametros)
        {
            ValidacaoXML validacaoXML = new ValidacaoXML();
            try
            {
                StringReader stringReaderxml = new StringReader(xmlParametros);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add(null, pathXSD);
                settings.ValidationType = ValidationType.Schema;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(stringReaderxml);
                XmlReader xmlReader = XmlReader.Create(new StringReader(xmlDocument.InnerXml), settings);
                while (xmlReader.Read()) { }
                validacaoXML.Sucesso = true;
            }
            catch (Exception ex)
            {
                validacaoXML.AdicionarErro(ex.Message);
            }
            return validacaoXML;
        }
    }
}
