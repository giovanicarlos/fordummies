﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;
using System.Linq.Expressions;

namespace ForDummies.BL
{
    public class BaseServico<T> : IBaseServico<T> where T : class
    {
        private readonly IBaseRepositorio<T> baseRepositorio;
        private readonly IUnitOfWork unitOfWork;

        public BaseServico(IBaseRepositorio<T> baseRepositorio, IUnitOfWork unitOfWork)
        {
            this.baseRepositorio = baseRepositorio;
            this.unitOfWork = unitOfWork;
        }

        public void Add(T entity)
        {
            baseRepositorio.Add(entity);
            unitOfWork.Commit();
        }

        public void Update(T entity)
        {
            baseRepositorio.Update(entity);
            unitOfWork.Commit();
        }

        public void Delete(T entity)
        {
            baseRepositorio.Delete(entity);
            unitOfWork.Commit();
        }

        public T GetById(int Id)
        {
            return baseRepositorio.GetById(Id);
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return baseRepositorio.GetMany(where);
        }

        public IEnumerable<T> GetAll()
        {
            return baseRepositorio.GetAll();
        }

        public IEnumerable<T> GetAll(Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            return baseRepositorio.GetAll(order, pageSize, pageIndex, out totalPages);
        }

        public IEnumerable<T> GetAllDescending(Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            return baseRepositorio.GetAllDescending(order, pageSize, pageIndex, out totalPages);
        }

        public int Count(Func<T, bool> where)
        {
            return baseRepositorio.Count(where);
        }

        public void Reload(T entity)
        {
            baseRepositorio.Reload(entity);
        }

        public void Reload(T entity, Expression<Func<T, object>> attribute)
        {
            baseRepositorio.Reload(entity, attribute);
        }
    }
}
