﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;
using ForDummies.BL.Interface;
using ForDummies.Dominio;
using System.Web;
using System.Web.Security;

namespace ForDummies.BL
{
    public class LogAcessoServico : BaseServico<LogAcesso>, ILogAcessoServico
    {
        private readonly ILogAcessoRepositorio logAcessoRepositorio;
        private readonly IUnitOfWork unitOfwork;
        private readonly string nomeCookie = "ac_dummies";

        public LogAcessoServico(ILogAcessoRepositorio logAcessoRepositorio, IUnitOfWork unitOfWork)
            : base(logAcessoRepositorio, unitOfWork)
        {
            this.logAcessoRepositorio = logAcessoRepositorio;
            this.unitOfwork = unitOfWork;
        }

        public void LogarAcesso(int alunoId, string ip)
        {
            FinalizaAcessosAbertos(alunoId, ip);

            LogAcesso logAcesso = new LogAcesso(alunoId, DateTime.Now, ip);

            logAcessoRepositorio.Add(logAcesso);

            unitOfwork.Commit();

            HttpContext.Current.Response.Cookies.Add(new HttpCookie(nomeCookie, logAcesso.LogAcessoId.ToString()));
        }

        private void FinalizaAcessosAbertos(int alunoId, string ip)
        {
            IQueryable<LogAcesso> logsAbertos = logAcessoRepositorio.GetMany(x => x.AlunoId.Equals(alunoId) && x.DataLogoff == null);

            foreach (var item in logsAbertos)
            {
                item.DataLogoff = DateTime.Now;
                item.Ip = ip;
            }

            unitOfwork.Commit();
        }

        private LogAcesso GetLogAcesso(int logAcessoId)
        {
            return logAcessoRepositorio.GetById(logAcessoId);
        }

        public bool ValidaAcessoAluno()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                LogAcesso logAcesso = GetLogAcesso(Convert.ToInt32(cookie.Value));

                if (logAcesso.DataLogoff != null)
                {
                    cookie = HttpContext.Current.Request.Cookies["ac_dummies"];
                    if (cookie != null)
                    {
                        cookie.Expires = DateTime.Now.AddDays(-1);
                    }
                }
                else
                    return true;
            }
            return false;
        }

        public void FinalizarSessao(int alunoId, string ip)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if(cookie != null)
                cookie.Expires = DateTime.Now.AddDays(-1);

            FinalizaAcessosAbertos(alunoId, ip);
            FormsAuthentication.SignOut();
        }

    }
}
