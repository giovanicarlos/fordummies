﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class ItemPedidoAssinaturaServico : BaseServico<ItemPedidoAssinatura>, IItemPedidoAssinaturaServico
    {
        private readonly IItemPedidoAssinaturaRepositorio itemPedidoAssinaturaRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public ItemPedidoAssinaturaServico(IItemPedidoAssinaturaRepositorio itemPedidoAssinaturaRepositorio,
            IUnitOfWork unitOfwork)
            : base(itemPedidoAssinaturaRepositorio, unitOfwork)
        {
            this.itemPedidoAssinaturaRepositorio = itemPedidoAssinaturaRepositorio;
            this.unitOfwork = unitOfwork;
        }

    }
}
