﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class ClassificacaoServico : BaseServico<Classificacao>, IClassificacaoServico
    {
        private readonly IClassificacaoRepositorio classificacaoRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public ClassificacaoServico(IClassificacaoRepositorio classificacaoRepositorio, IUnitOfWork unitOfWork)
            : base(classificacaoRepositorio, unitOfWork)
        {
            this.classificacaoRepositorio = classificacaoRepositorio;
            this.unitOfwork = unitOfWork;
        }

        public List<Classificacao> ConsultarClassificacoes()
        {
            return classificacaoRepositorio.GetAll().ToList();
        }

        public Classificacao Consultar(string urlSeo)
        {
            return classificacaoRepositorio.Get(x => x.UrlSeo.Equals(urlSeo + ".aspx") && x.Status == (byte)enmStatusClassificacao.Ativo);
        }

        public Classificacao Consultar(int cursoId)
        {
            return classificacaoRepositorio.Get(x => x.Cursos.Where(y => y.CursoId.Equals(cursoId)).Count() > 0);
        }
    }
}
