﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class CursoServico : BaseServico<Curso>, ICursoServico
    {
        private readonly ICursoRepositorio cursoRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public CursoServico(ICursoRepositorio cursoRepositorio, 
            IUnitOfWork unitOfWork)
            : base(cursoRepositorio, unitOfWork)
        {
            this.cursoRepositorio = cursoRepositorio;
            this.unitOfwork = unitOfWork;
        }

        public Curso Consultar(string urlSeo)
        {
            return cursoRepositorio.Get(x => x.UrlSeo.Equals(urlSeo + ".aspx") && x.Status == (byte)enmStatusCurso.Ativo);
        }

        public List<Curso> ConsultarCursos(int classificacaoId, int formato, bool destaque, int paginaAtual, int totalRegistrosPagina)
        {
            return cursoRepositorio.ConsultarCursos(classificacaoId, formato, destaque, paginaAtual, totalRegistrosPagina).ToList();
        }

        public List<Curso> ConsultarCursos(int formato, bool destaque, int paginaAtual, int totalRegistrosPagina)
        {
            return cursoRepositorio.Consultarcursos(formato, destaque, paginaAtual, totalRegistrosPagina).ToList();
        }

        public List<Curso> ConsultarCursos(bool destaque, int paginaAtual, int totalRegistrosPagina)
        {
            return cursoRepositorio.Consultarcursos(destaque, paginaAtual, totalRegistrosPagina).ToList();
        }        


        public List<Curso> ConsultarCursos(string textoBusca, int? formato, int paginaAtual, int totalRegistrosPagina, out int total)
        {
            return cursoRepositorio.ConsultarCursos(textoBusca, formato, paginaAtual, totalRegistrosPagina, out total).ToList();
        }

        public List<Curso> ConsultarTop(int quantidade, int? classificacaoId)
        {
            return cursoRepositorio.ConsultarTop(quantidade, classificacaoId).ToList();
        }

        public Curso Consultar(int matriculaId)
        {
            return cursoRepositorio.Consultar(matriculaId);
        }

        public Classificacao ConsultarClassificacao(int cursoId)
        {
            return cursoRepositorio.Get(x => x.CursoId == cursoId).Classificacoes.FirstOrDefault();
        }
    }
}
