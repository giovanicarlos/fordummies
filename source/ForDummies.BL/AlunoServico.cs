﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Infra;
using ForDummies.Dominio;
using ForDummies.Infra.Contexto;
using ForDummies.Dominio.Interface;
using ForDummies.BL.Interface;

namespace ForDummies.BL
{
    public class AlunoServico : BaseServico<Aluno>, IAlunoServico
    {
        private readonly IAlunoRepositorio alunoRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public AlunoServico(IAlunoRepositorio alunoRepositorio, IUnitOfWork unitOfWork) : base(alunoRepositorio, unitOfWork)
        {
            this.alunoRepositorio = alunoRepositorio;
            this.unitOfwork = unitOfWork;
        }

        public Aluno Validar(string email, string senha)
        {
            return alunoRepositorio.Get(x => x.Email.Equals(email) && x.Senha.Equals(senha));
        }

        public Aluno ConsultarPorCodigo(string codigo)
        {
            return alunoRepositorio.Get(x => x.Codigo.Equals(codigo));
        }
    }
}
