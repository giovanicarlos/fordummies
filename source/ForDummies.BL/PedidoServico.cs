﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class PedidoServico : BaseServico<Pedido>, IPedidoServico
    {
        private readonly ICursoServico cursoServico;
        private readonly IPedidoRepositorio pedidoRepositorio;
        private readonly IAlunoServico alunoServico;
        private readonly IUnitOfWork unitOfwork;

        public PedidoServico(IPedidoRepositorio pedidoRepositorio, 
            ICursoServico cursoServico,
            IUnitOfWork unitOfWork,
            IAlunoServico alunoServico)
            : base(pedidoRepositorio, unitOfWork)
        {
            this.pedidoRepositorio = pedidoRepositorio;
            this.cursoServico = cursoServico;
            this.unitOfwork = unitOfWork;
            this.alunoServico = alunoServico;
        }

        public Pedido CriarPedidoAssinatura(int alunoId, int cursoId)
        {
            try
            {
                Curso curso = cursoServico.GetById(cursoId);
                if (curso != null)
                {
                    Pedido pedido = new Pedido(alunoId, enmTipoPedido.Assinatura);
                    
                    pedido.AddItens(new ItemPedido(curso.Valor, pedido, curso.CursoId));

                    pedidoRepositorio.Add(pedido);
                    unitOfwork.Commit();
                    
                    pedidoRepositorio.Reload(pedido, x=> x.Aluno);

                    return pedido;
                }
                else
                    throw new Exception("O curso informado não é valido");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
    }
}
