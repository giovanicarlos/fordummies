﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;

namespace ForDummies.BL
{
    public class ItemPedidoOneShotServico : BaseServico<ItemPedidoOneShot>, IItemPedidoOneShotServico
    {
        private readonly IItemPedidoOneShotRepositorio itemPedidoOneShotRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public ItemPedidoOneShotServico(IItemPedidoOneShotRepositorio itemPedidoOneShotRepositorio, 
            IUnitOfWork unitOfwork) : base(itemPedidoOneShotRepositorio, unitOfwork)
        {
            this.itemPedidoOneShotRepositorio = itemPedidoOneShotRepositorio;
            this.unitOfwork = unitOfwork;
        }

    }
}
