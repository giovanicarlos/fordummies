﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForDummies.Dominio;
using ForDummies.BL.Interface;
using ForDummies.Dominio.Interface;
using ForDummies.Infra.Contexto;
using System.Configuration;
using ForDummies.BL.WebService;

namespace ForDummies.BL
{
    public class MatriculaServico : BaseServico<Matricula>, IMatriculaServico
    {
        private readonly ICursoServico cursoServico;
        private readonly IPedidoServico pedidoServico;
        private readonly IMatriculaRepositorio matriculaRepositorio;
        private readonly IUnitOfWork unitOfwork;

        public MatriculaServico(IMatriculaRepositorio matriculaRepositorio, ICursoServico cursoServico, IPedidoServico pedidoServico, IUnitOfWork unitOfWork)
            : base(matriculaRepositorio, unitOfWork)
        {
            this.matriculaRepositorio = matriculaRepositorio;
            this.cursoServico = cursoServico;
            this.pedidoServico = pedidoServico;
            this.unitOfwork = unitOfWork;
        }

        public int QuantidadeMatriculaMesAtual(int alunoId, int formatoId)
        {
            DateTime dataInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime dataFinal = new DateTime(dataInicial.Year, dataInicial.Month, DateTime.DaysInMonth(dataInicial.Year, dataInicial.Month));

            return QuantidadeMatricula(alunoId, formatoId, dataInicial, dataFinal);
        }

        public int QuantidadeMatricula(int alunoId, int formatoId, DateTime dataInicial, DateTime dataFinal)
        {
            return matriculaRepositorio.Count(x => x.AlunoId.Equals(alunoId) && x.Curso.FormatoId.Equals(formatoId) && x.Status != (byte)enmStatusMatricula.Bloqueado
                && x.Status != (byte)enmStatusMatricula.Cancelado && x.DataCadastro >= dataInicial && x.DataCadastro <= dataFinal);
        }

        public List<Matricula> ConsultarMatriculas(int alunoId)
        {
            return matriculaRepositorio.ConsultarMatriculas(alunoId).ToList();
        }


        public List<Matricula> ConsultarMatriculas(int alunoId, enmStatusMatricula enmStatusMatricula)
        {
            return matriculaRepositorio.ConsultarMatriculas(alunoId, enmStatusMatricula).ToList();
        }

        public int QuantidadeMatriculaPermitidaMesAtual(int alunoId, int formatoId)
        {
            DateTime dataInicial = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            DateTime dataFinal = new DateTime(dataInicial.Year, dataInicial.Month, DateTime.DaysInMonth(dataInicial.Year, dataInicial.Month));

            return QuantidadeMatriculaPermitida(alunoId, formatoId, dataInicial, dataFinal);
        }

        public int QuantidadeMatriculaPermitida(int alunoId, int formatoId, DateTime dataInicial, DateTime dataFinal)
        {
            int totalPermitido = Convert.ToInt32(ConfigurationManager.AppSettings["qtdMatriculasPermitidasPorMes"]);

            return (totalPermitido - matriculaRepositorio.Count(x => x.AlunoId.Equals(alunoId) &&
                x.Curso.FormatoId.Equals(formatoId) && x.Status != (byte)enmStatusMatricula.Bloqueado
                && x.Status != (byte)enmStatusMatricula.Cancelado && x.Status != (byte)enmStatusMatricula.PendenteCriarCresca && x.DataCadastro >= dataInicial && x.DataCadastro <= dataFinal));
        }

        public bool PermiteMatricular(int alunoId, int formatoId)
        {
            return QuantidadeMatriculaPermitidaMesAtual(alunoId, formatoId) > 0;
        }

        public bool Matricular(int pedidoId)
        {
            Pedido pedido = pedidoServico.GetById(pedidoId);

            if (pedido != null)
            {
                int totalSucesso = 0;
                foreach (var item in pedido.Itens)
                {
                    Matricula mat = new Matricula(pedido.Aluno, item.Curso, item);
                    MatricularCB(mat);
                    totalSucesso++;
                    Add(mat);
                }

                return totalSucesso.Equals(pedido.Itens.Count);
            }
            return false;
        }

        private void MatricularCB(Matricula matricula)
        {
            MatriculaCBServico matriculaCBServico = new MatriculaCBServico();

            AlunoCB alunoCB = new AlunoCB(matricula.Aluno.Nome, matricula.Aluno.Email, matricula.Aluno.Senha);

            CadastrarMatricula matriculaWs = new CadastrarMatricula(alunoCB, matricula.Curso.CodigoCursoCB, matricula.Curso.FormatoId);
            CadastrarMatriculaRetorno retornoWs = matriculaCBServico.CadastrarMatricula(matriculaWs);

            matricula.AtualizarMatricula(retornoWs.IdMatricula, retornoWs.IdAluno, retornoWs.URLAva, retornoWs.UrlCertificado, retornoWs.UrlPesquisa);
        }

        public List<Matricula> Consultar(int alunoId, enmStatusMatricula? statusMatricula)
        {
            return matriculaRepositorio.ConsultarMatriculasVisiveisCliente(alunoId, statusMatricula).ToList();
        }

        public void AtualizarMatriculas(int alunoId)
        {
            List<Matricula> matriculas = Consultar(alunoId, null);

            if (matriculas.Count() > 0)
                AtualizarMatriculasCB(matriculas[0].AlunoIdCB);
        }

        private void AtualizarMatriculasCB(int alunoIdCB)
        {
            MatriculaCBServico matriculaCBServico = new MatriculaCBServico();

            ConsultarMatriculas matriculaWs = new ConsultarMatriculas(alunoIdCB);
            ConsultarMatriculasRetorno matriculaWsRetorno = matriculaCBServico.ConsultarMatriculas(matriculaWs);

            int[] matriculasId = matriculaWsRetorno.GetMatriculasCBId();
            List<Matricula> matriculas = matriculaRepositorio.ConsultarMatriculas(matriculasId).ToList();

            AtualizarMatriculasRetornoWs(matriculas, matriculaWsRetorno);
        }

        private void AtualizarMatriculasRetornoWs(List<Matricula> matriculas, ConsultarMatriculasRetorno matriculasCB)
        {
            for (int i = 0; i < matriculasCB.Matriculas.Length; i++)
            {
                Matricula matricula = matriculas.Find(x => x.MatriculaIdCB == matriculasCB.Matriculas[i].IdMatricula);
                if (matricula != null)
                    AtualizarUmaMatricula(matricula, matriculasCB.Matriculas[i]);
            }
        }

        private void AtualizarUmaMatricula(Matricula matricula, MatriculaRetorno matriculaRetornoWs)
        {
            matricula.CertificadoDisponivel = matriculaRetornoWs.CertificadoDisponivel;
            matricula.PesquisaRespondida = matriculaRetornoWs.RespondeuPesquisa;
            matricula.DataExpiracao = Convert.ToDateTime(matriculaRetornoWs.DataExpiracao);
            matricula.PorcentagemConclusao = Convert.ToDecimal(matriculaRetornoWs.PercentualConcluido);
            matricula.Status = (byte)GetStatusMatricula(matriculaRetornoWs.Status.Codigo);
            Update(matricula);
        }

        private enmStatusMatricula GetStatusMatricula(int statusId)
        {
            switch (statusId)
            {
                case 0:
                    return enmStatusMatricula.Bloqueado;
                case 1:
                    return enmStatusMatricula.EmAndamento;
                case 2:
                    return enmStatusMatricula.EmAndamento;
                case 3:
                    return enmStatusMatricula.Concluido;
                case 4:
                    return enmStatusMatricula.Cancelado;
                case 5:
                    return enmStatusMatricula.PendenteCriarCresca;
                case 6:
                    return enmStatusMatricula.Cancelado;
                case 7:
                    return enmStatusMatricula.Expirado;
                default:
                    return enmStatusMatricula.Bloqueado;
            }
        }
    }
}
