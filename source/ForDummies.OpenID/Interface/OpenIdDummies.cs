﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetOpenAuth.OpenId.RelyingParty;
using System.Configuration;
using System.Web;
using DotNetOpenAuth.OpenId.Extensions;
using DotNetOpenAuth.Messaging;
using Ninject;
using ForDummies.Dominio;

namespace libUOL.Interface
{
    public class OpenIdDummiesPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {     
        }        

        #region OpenId

        protected void EntrarComOpenId()
        {
            using (OpenIdRelyingParty openid = new OpenIdRelyingParty())
            {
                IAuthenticationRequest request = openid.CreateRequest(ConfigurationManager.AppSettings["OpenIdUrl"]);

                var ui = new UolUiRequest
                {
                    skin = ConfigurationManager.AppSettings["OpenIdSkin"]
                };

                var authorizationRequest = new UolAuthorizationRequest
                {
                    type = ConfigurationManager.AppSettings["OpenIdType"],
                    id = ConfigurationManager.AppSettings["OpenIdOneShot"] + "|" + ConfigurationManager.AppSettings["OpenIdAssinatura"],
                    unique = ConfigurationManager.AppSettings["OpenIdUnique"],
                };

                request.AddExtension(ui);
                request.AddExtension(authorizationRequest);

                request.RedirectToProvider();
            }
        }

        protected RetornoOpenId VerificaAutenticacaoOpenId()
        {
            OpenIdRelyingParty openid = new OpenIdRelyingParty();
            RetornoOpenId retorno = null;
            var response = openid.GetResponse();
            if (response != null)
            {
                switch (response.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        SetPermissaoUol(this.Request);
                        string url = response.ClaimedIdentifier;
                        string identificador = url.Replace(url.Substring(0, url.IndexOf("?id=")) + "?id=", "");
                        retorno = new RetornoOpenId(true, identificador, "/?action=ok");
                        break;
                    case AuthenticationStatus.Canceled:
                        retorno = new RetornoOpenId(false, "", "/?action=login");
                        break;
                    case AuthenticationStatus.Failed:
                        retorno = new RetornoOpenId(false, "", "/?action=login");
                        break;
                }
            }

            return retorno;
        }  
      
        private void SetPermissaoUol(HttpRequest httpRequest)
        {
            ForDummies.CookieManager.Cookie.SetCookie("ttyxkl", "OneShot", false.ToString());
            ForDummies.CookieManager.Cookie.SetCookie("ttyxkl", "Assinatura", false.ToString());
            string OneShot = ConfigurationManager.AppSettings["OpenIdOneShot"];
            string Assinatura = ConfigurationManager.AppSettings["OpenIdAssinatura"];

            string retornoUOL = string.Empty;

            foreach (var item in httpRequest.QueryString.AllKeys)
            {
                if (item.Contains("status." + OneShot))
                {
                    if (httpRequest.QueryString[item] == "1")
                        ForDummies.CookieManager.Cookie.SetCookie("ttyxkl", "OneShot", true.ToString());
                }
                if (item.Contains("status." + Assinatura))
                {
                    if (httpRequest.QueryString[item] == "1")
                        ForDummies.CookieManager.Cookie.SetCookie("ttyxkl", "Assinatura", true.ToString());
                }
                retornoUOL += "|" + item;
            }
            ForDummies.CookieManager.Cookie.SetCookie("dbgKJHYKK", retornoUOL);
        }
    }

    public class RetornoOpenId
    {
        public bool Sucesso { get; set; }
        public string Identificador { get; set; }
        public string UrlRedirect { get; set; }

        public RetornoOpenId(bool sucesso, string identificador, string urlRedirect)
        {
            this.Sucesso = sucesso;
            this.Identificador = identificador;
            this.UrlRedirect = urlRedirect;
        }
    }


    public class UolUiRequest : ExtensionBase
    {
        public UolUiRequest()
            : base(new Version(1, 0), "http://specs.openid.net/extensions/ui/1.0", null)
        {

        }


        [MessagePart("skin", IsRequired = true, AllowEmpty = false)]
        public string skin { get; set; }
    }

    public class UolAuthorizationRequest : ExtensionBase
    {
        public UolAuthorizationRequest()
            : base(new Version(1, 0), "http://openid.uol.com.br/srv/autho/1.0", null)
        {

        }

        [MessagePart("type", IsRequired = true, AllowEmpty = false)]
        public string type { get; set; }

        [MessagePart("id", IsRequired = true, AllowEmpty = false)]
        public string id { get; set; }

        [MessagePart("unique", IsRequired = true, AllowEmpty = false)]
        public string unique { get; set; }
    }

    #endregion
}
