using System;
using System.Collections.Generic;
using System.Text;
using ForDummies.Dominio;

namespace libUOL.Interface
{
    public interface IUOLService
    {
        /// <summary>
        /// Verifica a exist�ncia do aluno atrav�s do valorCookie salvo no campo C�digo da tblAluno.
        /// Caso o aluno exista retorna true, sen�o o aluno � cadastrado.
        /// </summary>
        bool CadastrarUsuario(string valorCookie, out Aluno aluno);
        bool PossuiAssinatura();      
    }
}
