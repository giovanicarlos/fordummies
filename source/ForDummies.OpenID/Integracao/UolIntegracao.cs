﻿using System;
using System.Collections.Generic;
using System.Text;
using libUOL.Dominio;
using System.Configuration;
using Newtonsoft.Json;
using System.Web;
using libUOL.Security;
using ForDummies.Dominio;

namespace libUOL.Integracao
{
    internal class UolIntegracao
    {
        private int CodigoParceiro = Convert.ToInt32(ConfigurationManager.AppSettings["CodigoParceiro"]);
        private string ChaveParceiro = ConfigurationManager.AppSettings["ChaveParceiro"];
        private string SkinParceiro = ConfigurationManager.AppSettings["SkinParceiro"];
        private string urlRetorno = ConfigurationManager.AppSettings["UrlRetornoUol"];
        private string urlRetornoErro = ConfigurationManager.AppSettings["UrlRetornoErroUol"];

        internal bool Comprar(Pedido pedido, out string jsonEncoded)
        {
            jsonEncoded = string.Empty;
            JSON json = new JSON(pedido, CodigoParceiro, ChaveParceiro, SkinParceiro, urlRetorno, urlRetornoErro);
            if (json != null)
            {
                jsonEncoded = UolCryptography.ToBase64Encode(JsonConvert.SerializeObject(json));
                return true;
            }
            return false;
        }

    }
}
