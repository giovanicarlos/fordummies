﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using libUOL.Security;
using ForDummies.Dominio;

namespace libUOL.Dominio
{
    internal class JSON
    {
        public Order order { get; set; }
        public int partner_id { get; set; }
        public string hash { get; set; }
        public string skin { get; set; }

        public JSON()
        {

        }

        public JSON(Pedido pedido, int codigoParceiro, string chaveParceiro, string skinParceiro, string urlRetorno, string urlRetornoErro)
        {
            this.order = new Order(pedido, urlRetorno, urlRetornoErro);
            this.partner_id = codigoParceiro;
            this.skin = skinParceiro;
            string json = JsonConvert.SerializeObject(this.order);
            this.hash = UolCryptography.ToSha256Hash(json, chaveParceiro);
        }
    }
}
