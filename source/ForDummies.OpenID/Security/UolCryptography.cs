﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace libUOL.Security
{
    public static class UolCryptography
    {
        public static string ToSha256Hash(string value, string key)
        {
            HMACSHA256 signingKey = new HMACSHA256();

            byte[] secretKeyByte = Encoding.UTF8.GetBytes(key);
            byte[] canonicalStringByte = Encoding.UTF8.GetBytes(value);

            signingKey.Key = secretKeyByte;
            byte[] cryptographedInByteArray = signingKey.ComputeHash(canonicalStringByte);

            string hash = string.Empty;
            foreach (byte element in cryptographedInByteArray)
            {
                hash += element.ToString("x2");
            }

            return hash;
        }

        public static string ToBase64Encode(string value)
        {
            byte[] encData_byte = new byte[value.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(value);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }

        public static string ToBase64Decode(string value)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();

            byte[] todecode_byte = Convert.FromBase64String(value);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }
    }
}
