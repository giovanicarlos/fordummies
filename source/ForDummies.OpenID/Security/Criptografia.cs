﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace ForDummies.OpenID.Security
{
    /// <summary>
    /// Tipo de Criptografia
    /// </summary>
    /// <remarks>
    ///   
    /// </remarks>
    public enum TipoSimetrica
    {
        /// <summary>
        /// Representa a classe base para implementações criptografia dos algoritmos simétricos Rijndael.
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        Rijndael,
        /// <summary>
        /// Representa a classe base para implementações do algoritmo RC2.
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        RC2
    }

    /// <summary>
    /// Classe auxiliar com métodos para criptografia de dados.
    /// </summary>
    /// <remarks>
    /// <example>
    /// <code>
    /// Criptografando:
    /// 
    /// using(Simetrica crip = new Simetrica(TipoSimetrica.Rijndael))
    /// {
    ///     textBox2.Text = crip.Criptografa(textBox1.Text, "ChaveBrasilShop");
    /// }
    /// 
    /// 
    /// </code>
    /// <code>
    /// Descriptografando:
    ///  using (Simetrica descrip = new Simetrica(TipoSimetrica.Rijndael))
    ///  {
    ///      textBox2.Text = descrip.Descriptografa(textBox2.Text, "BrasilShopSoft07BrasilShopSoft07");
    ///  }
    /// 
    /// </code>
    /// </example>
    /// </remarks>
    public class Simetrica : IDisposable
    {
        #region Atributos

        private string _chave = string.Empty;
        private TipoSimetrica _Tipo;
        private SymmetricAlgorithm _algoritmo;


        #endregion

        #region Propriedades
        /// <summary>
        /// Chave secreta para o algoritmo simétrico de criptografia.
        /// TipoSimetrica = Rijndael --> Chave = 16, 32, 64 bytes 
        /// TipoSimetrica = RC2 --> Chave = 8, 16, 32, 64 bytes 
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        public string Chave
        {
            get { return _chave; }
            set { _chave = value; }
        }

        #endregion

        #region Construtores
        /// <summary>
        /// Contrutor padrão da classe, é setado um tipo de criptografia padrão - RijndaelManaged.
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        public Simetrica()
        {
            _algoritmo = new RijndaelManaged();
            _algoritmo.Mode = CipherMode.CBC;
            _Tipo = TipoSimetrica.Rijndael;
        }

        /// <summary>
        /// Construtor com o tipo de criptografia a ser usada.
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        /// <param name="Tipo">Tipo de criptografia.</param>
        public Simetrica(TipoSimetrica Tipo)
        {
            // Seleciona algoritmo simétrico
            switch (Tipo)
            {
                case TipoSimetrica.Rijndael:
                    _algoritmo = new RijndaelManaged();
                    _Tipo = TipoSimetrica.Rijndael;
                    break;
                case TipoSimetrica.RC2:
                    _algoritmo = new RC2CryptoServiceProvider();
                    _Tipo = TipoSimetrica.RC2;
                    break;
            }
            _algoritmo.Mode = CipherMode.CBC;
        }
        #endregion

        #region Métodos


        /// <summary>
        /// Método que coloca o IV no algoritmo de acordo com o tamanho permitido nele
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        private void SetIV()
        {
            switch (_Tipo)
            {
                case TipoSimetrica.Rijndael:
                    _algoritmo.IV = System.Text.Encoding.Default.GetBytes("brasilshopsoft07");
                    break;
                default:
                    _algoritmo.IV = System.Text.Encoding.Default.GetBytes("bshop-07");
                    break;
            }
        }

        /// <summary>
        /// Encripta o dado solicitado.
        /// </summary>
        /// <param name="valor">Texto a ser criptografado.</param>
        /// <param name="chave">Chave para criptografia
        /// TipoSimetrica = Rijndael --> Chave = 16, 32, 64 bytes 
        /// TipoSimetrica = RC2 --> Chave = 8, 16, 32, 64 bytes 
        /// </param>
        /// <returns>Texto criptografado.</returns>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// 
        /// using(Simetrica crip = new Simetrica(TipoSimetrica.Rijndael))
        /// {
        ///     textBox2.Text = crip.Criptografa(textBox1.Text, "ChaveBrasilShop");
        /// }
        /// 
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public virtual string Criptografa(string valor, string chave)
        {
            byte[] ByteValor = System.Text.Encoding.UTF8.GetBytes(valor);

            // Seta a chave privada
            _algoritmo.Key = System.Text.Encoding.Default.GetBytes(chave);
            SetIV();

            // Interface de criptografia / Cria objeto de criptografia
            ICryptoTransform cryptoTransform = _algoritmo.CreateEncryptor();

            MemoryStream _memoryStream = new MemoryStream();

            CryptoStream _cryptoStream = new CryptoStream(_memoryStream, cryptoTransform, CryptoStreamMode.Write);

            // Grava os dados criptografados no MemoryStream
            _cryptoStream.Write(ByteValor, 0, ByteValor.Length);
            _cryptoStream.FlushFinalBlock();

            // Busca o tamanho dos bytes encriptados
            byte[] cryptoByte = _memoryStream.ToArray();

            // Converte para a base 64 string para uso posterior em um xml
            return Convert.ToBase64String(cryptoByte, 0, cryptoByte.GetLength(0));
        }

        /// <summary>
        /// Encripta o dado solicitado.
        /// </summary>
        /// <param name="valor">Texto a ser criptografado.</param>
        /// <returns>Texto criptografado.</returns>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// 
        /// using(Simetrica crip = new Simetrica(TipoSimetrica.Rijndael))
        /// {
        ///     crip.Chave = "ChaveBrasilShop";
        ///     textBox2.Text = crip.Criptografa(textBox1.Text);
        /// }
        /// 
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public virtual string Criptografa(string valor)
        {
            return this.Criptografa(valor, this.Chave);
        }

        /// <summary>
        /// Desencripta o dado solicitado.
        /// </summary>
        /// <param name="valor">Texto a ser descriptografado.</param>
        /// <param name="chave">chave para criptografia</param>
        /// <returns>Texto descriptografado.</returns>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// 
        ///  using (Simetrica descrip = new Simetrica(TipoSimetrica.Rijndael))
        ///  {
        ///      textBox2.Text = descrip.Descriptografa(textBox2.Text,"ChaveBrasilShop");
        ///  }
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public virtual string Descriptografa(string valor, string chave)
        {
            // Converte a base 64 string em num array de bytes
            byte[] cryptoByte = Convert.FromBase64String(valor);

            // Seta a chave privada
            _algoritmo.Key = System.Text.Encoding.Default.GetBytes(chave);
            SetIV();

            // Interface de criptografia / Cria objeto de descriptografia
            ICryptoTransform cryptoTransform = _algoritmo.CreateDecryptor();
            try
            {
                MemoryStream _memoryStream = new MemoryStream(cryptoByte, 0, cryptoByte.Length);

                CryptoStream _cryptoStream = new CryptoStream(_memoryStream, cryptoTransform, CryptoStreamMode.Read);

                // Busca resultado do CryptoStream
                StreamReader _streamReader = new StreamReader(_cryptoStream);
                return _streamReader.ReadToEnd();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Desencripta o dado solicitado.
        /// </summary>
        /// <param name="valor">Texto a ser descriptografado.</param>
        /// <returns>Texto descriptografado.</returns>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// 
        ///  using (Simetrica descrip = new Simetrica(TipoSimetrica.Rijndael))
        ///  {
        ///      descript.Chave = "ChaveBrasilShop"; 
        ///      textBox2.Text = descrip.Descriptografa(textBox2.Text);
        ///  }
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public virtual string Descriptografa(string valor)
        {
            return Descriptografa(valor, this.Chave);
        }

        /// <summary>
        /// Encripta o dado solicitado para uso em querystring.
        /// Caso não seja setado a chave de criptografia, a classe usa uma padrão.
        /// </summary>
        /// <param name="valor">Texto a ser criptografado.</param>
        /// <returns>Texto criptografado.</returns>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// 
        /// using(Simetrica crip = new Simetrica(TipoSimetrica.Rijndael))
        /// {
        ///     crip.Chave = "ChaveBrasilShop";
        ///     textBox2.Text = crip.Criptografa(textBox1.Text);
        /// }
        /// 
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public virtual string CriptografaQueryString(string valor)
        {
            this._chave = string.IsNullOrEmpty(this._chave) ? "ChavePadraoQueryString" : this._chave;
            return Util.Util.base64Encode(this.Criptografa(valor, this.Chave));
        }


        /// <summary>
        /// Desencripta o dado solicitado para uso em querystring.
        /// </summary>
        /// <param name="valor">Texto a ser descriptografado.</param>
        /// <returns>Texto descriptografado.</returns>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// 
        ///  using (Simetrica descrip = new Simetrica(TipoSimetrica.Rijndael))
        ///  {
        ///      descript.Chave = "ChaveBrasilShop"; 
        ///      textBox2.Text = descrip.Descriptografa(textBox2.Text);
        ///  }
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public virtual string DescriptografaQueryString(string valor)
        {
            this._chave = string.IsNullOrEmpty(this._chave) ? "ChavePadraoQueryString" : this._chave;
            return this.Descriptografa(Util.Util.base64Decode(valor), this.Chave);
        }


        #endregion

        #region IDisposable Members

        /// <summary>
        /// Quando se usa o bloco using é chamado automaticamente após o fechamento de chaves
        /// O Dispose() retira da memória o objeto em questão
        /// </summary>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// using(Simetrica crip = new Simetrica(TipoSimetrica.Rijndael))
        /// {
        ///     textBox2.Text = crip.Criptografa(textBox1.Text, "ChaveBrasilShop");
        /// } //aqui é tirado da memória
        /// 
        /// </code>
        /// <code>
        /// 
        /// Simetrica crip = new Simetrica(TipoSimetrica.Rijndael);
        /// textBox2.Text = crip.Criptografa(textBox1.Text, "ChaveBrasilShop");
        /// crip.Dispose();//aqui objeto é tirado da memória.
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public void Dispose()
        {
            this._algoritmo.Clear();
            GC.ReRegisterForFinalize(this);
        }

        #endregion
    }

    /// <summary>
    /// Enum com os tipos possiveis de Hash
    /// </summary>
    /// <remarks>
    ///   
    /// </remarks>
    public enum TipoHash
    {
        /// <summary>
        /// Hash SHA-1
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        SHA1,
        /// <summary>
        /// Hash SHA256
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        SHA256,
        /// <summary>
        /// Hash SHA384
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        SHA384,
        /// <summary>
        /// Hash SHA512
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        SHA512,
        /// <summary>
        /// Hash MD5
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        MD5
    }


    /// <summary>
    /// Classe que guarda todas as funcionalidades do Hash
    /// </summary>
    /// <remarks>
    /// <example>
    /// <code>
    /// using (Hash hd = new Hash(TipoHash.SHA1))
    /// {
    ///      textBox2.Text = hd.GetHash("joão");
    /// }
    /// </code>
    /// </example>
    /// </remarks>
    public class Hash : IDisposable
    {
        #region Atributos

        private HashAlgorithm _algoritmo;

        #endregion

        #region Construtores

        /// <summary>
        /// Contrutor padrão da classe, é setado um tipo de hash padrão SHA-1.
        /// </summary>
        /// <remarks>
        /// <example>
        /// <code>
        /// string valor = "Caio";
        /// string Cript = "";
        /// using(Hash hd = new Hash())
        /// {
        ///     Cript = hd.RetornaHash(valor);
        /// }
        /// </code>
        /// </example>
        /// </remarks>
        public Hash()
        {
            _algoritmo = new SHA1Managed();
        }

        /// <summary>
        /// Construtor com o tipo de hash a ser usado.
        /// </summary>
        /// <remarks>
        /// <example>
        /// <code>
        /// string valor = "Caio";
        /// string Cript = "";
        /// using(Hash hd = new Hash(TipoHash.MD5))
        /// {
        ///     Cript = hd.RetornaHash(valor);
        /// }
        /// </code>
        /// </example>
        /// </remarks>
        /// <param name="Tipo">Tipo de Hash.</param>
        public Hash(TipoHash Tipo)
        {
            switch (Tipo)
            {
                case TipoHash.MD5:
                    _algoritmo = new MD5CryptoServiceProvider();
                    break;
                case TipoHash.SHA1:
                    _algoritmo = new SHA1Managed();
                    break;
                case TipoHash.SHA256:
                    _algoritmo = new SHA256Managed();
                    break;
                case TipoHash.SHA384:
                    _algoritmo = new SHA384Managed();
                    break;
                case TipoHash.SHA512:
                    _algoritmo = new SHA512Managed();
                    break;
            }
        }

        #endregion

        #region Métodos
        /// <summary>
        /// Monta hash para algum valor texto.
        /// </summary>
        /// <remarks>
        /// <example>
        /// <code>
        /// string valor = "Caio";
        /// string Cript = "";
        /// using(Hash hd = new Hash())
        /// {
        ///     Cript = hd.RetornaHash(valor);
        /// }
        /// </code>
        /// </example>
        /// </remarks>
        /// <param name="valor">Texto a ser criado o hash.</param>
        /// <returns>Hash do texto inserido.</returns>
        public string RetornaHash(string valor)
        {
            byte[] cryptoByte = _algoritmo.ComputeHash(ASCIIEncoding.UTF8.GetBytes(valor));

            return Convert.ToBase64String(cryptoByte, 0, cryptoByte.Length);
        }
        /// <summary>
        /// Cria hash para algum tipo de stream (arquivo).
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        /// <param name="fileStream">Stream a ser criado o hash.</param>
        /// <returns>Hash do stream inserido.</returns>
        public string RetornaHash(FileStream fileStream)
        {
            byte[] cryptoByte;

            cryptoByte = _algoritmo.ComputeHash(fileStream);
            fileStream.Close();

            return Convert.ToBase64String(cryptoByte, 0, cryptoByte.Length);
        }
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Quando se usa o bloco using é chamado automaticamente após o fechamento de chaves
        /// O Dispose() retira da memória o objeto em questão
        /// </summary>
        /// <remarks>
        /// <example>
        /// <code>
        /// Exemplo:
        /// 
        /// using (Hash hd = new Hash(TipoHash.SHA1))
        /// {
        ///      textBox2.Text = hd.GetHash("joão");
        /// }//aqui é chamdo o Dispose
        /// </code>
        /// <code>
        /// 
        /// Hash hd = new Hash(TipoHash.SHA1);
        /// textBox2.Text = hd.GetHash("joão");
        /// hd.Dispose();
        /// 
        /// </code>
        /// </example>
        /// </remarks>
        public void Dispose()
        {
            this._algoritmo.Clear();
            GC.ReRegisterForFinalize(this);
        }

        #endregion
    }
}
