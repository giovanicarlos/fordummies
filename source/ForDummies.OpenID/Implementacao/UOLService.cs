using System;
using System.Collections.Generic;
using System.Text;
using libUOL.Interface;
using System.Threading;
using System.Net.Mail;
using System.Configuration;
using libUOL.Security;
using ForDummies.Dominio;
using ForDummies.BL.Interface;

namespace libUOL.Implementacao
{
    public class UOLService : IUOLService
    {
        private readonly IAlunoServico alunoServico;
        private readonly IMatriculaServico matriculaServico;

        public UOLService (IAlunoServico alunoServico, IMatriculaServico matriculaServico)
	    {
            this.alunoServico = alunoServico;
            this.matriculaServico = matriculaServico;
	    }

        public bool CadastrarUsuario(string valorCookie, out Aluno aluno)
        {
            aluno = null;

            try
            {
                aluno = alunoServico.ConsultarPorCodigo(valorCookie);

                if (aluno == null)
                {
                    aluno = new Aluno();
                    aluno.Nome = "";
                    aluno.Codigo = valorCookie;
                    aluno.Email = valorCookie + "@uol.com.br";
                    aluno.Senha = valorCookie + "XPTO";

                    alunoServico.Add(aluno);
                    
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool PossuiAssinatura()
        {
            string parametro = ForDummies.CookieManager.Cookie.GetCookie("ttyxkl", "Assinatura");
            if (!string.IsNullOrEmpty(parametro))
            {
                return Convert.ToBoolean(parametro);
            }
            return false;
        }        

        #region SACA
        /*
        public bool ValidaUsuarioSACA(string valorCookie, TO.Sistema toSistema, out TO.UOLSac toUolSac)
        {
            toUolSac = null;
            string chave = ConfigurationManager.AppSettings["HashKey"];
            string[] vCookie = UolCryptography.ToBase64Decode(valorCookie).Split('|');
            string login = vCookie[0];
            string date = vCookie[1];
            string auth = vCookie[2];
            string validationHash = vCookie[3];
            string data = string.Join("|", new string[3] { login, date, auth });
            string dataHash = UolCryptography.ToSha256Hash(data, chave);
            bool PermissaoSupervisor = false;
            bool PermissaoAtendente = false;

            if (auth.Contains("DUM_SACA_SUPERVISOR"))
                PermissaoSupervisor = true;
            else if (auth.Contains("DUM_SACA_ATENDENTE"))
                PermissaoAtendente = true;

            validarHash(validationHash, dataHash, UolCryptography.ToBase64Decode(valorCookie));
            validaPermissao(PermissaoSupervisor, PermissaoAtendente, UolCryptography.ToBase64Decode(valorCookie));

            CrescaBrasilBL.UOLSac blUolSac = new CrescaBrasilBL.UOLSac();

            toUolSac = new CrescaBrasilBL.UOLSac().ConsultarPorCodigoSistema(login, toSistema);
            if (toUolSac != null)
            {
                if (PermissaoSupervisor && !toUolSac.TipoPermissao)
                {
                    toUolSac.TipoPermissao = true;
                    new CrescaBrasilBL.UOLSac().Alterar(toUolSac);
                }
                else if (PermissaoAtendente && toUolSac.TipoPermissao)
                {
                    toUolSac.TipoPermissao = false;
                    new CrescaBrasilBL.UOLSac().Alterar(toUolSac);
                }
                return true;
            }
            else
            {
                toUolSac = new TO.UOLSac();
                toUolSac.Nome = " ";
                toUolSac.Cpf = "000.000.000-00";
                toUolSac.Email = login + "@uol.com.br";
                toUolSac.Sexo = 0;
                toUolSac.Senha = login;
                toUolSac.Codigo = login;
                toUolSac.DataNascimento = DateTime.Now;
                toUolSac.DataCadastro = DateTime.Now;
                toUolSac.Enderecos = new List<TO.Endereco>();
                TO.Endereco end = new TO.Endereco();
                end.TituloEndereco = "Endere�o Principal";
                end.Logradouro = " ";
                end.Numero = " ";
                end.Bairro = " ";
                end.Complemento = " ";
                end.Cep = " ";
                end.Cidade = " ";
                end.Estado = " ";
                end.Pais = new TO.Pais(30);
                end.CodigoIBGE = " ";
                toUolSac.Enderecos.Add(end);
                //Se n�o for supervisor cadastra 0
                toUolSac.TipoPermissao = PermissaoSupervisor;

                if (blUolSac.Cadastrar(toUolSac, toSistema))
                    return true;
                else
                {
                    toUolSac = null;
                    return false;
                }
            }
        }

        private void validarHash(string hashPassado, string hashGerado, string cookie)
        {
            if (hashPassado != hashGerado)
            {
                string msg = "<u>Hash inv�lido</u>" + Environment.NewLine;
                msg += "Hash passado: " + hashPassado + Environment.NewLine;
                msg += "Hash gerado: " + hashGerado + Environment.NewLine;
                msg += "Cookie: " + cookie;
                throw new Exception(msg);
            }
        }

        private void validaPermissao(bool Supervisor, bool Atendente, string cookie)
        {
            if (!Supervisor && !Atendente)
            {
                throw new Exception("<u>Usu�rio sem permiss�o</u>" + Environment.NewLine + "Valor Cookie: " + cookie);
            }
        }

         */
        #endregion
    }
}
