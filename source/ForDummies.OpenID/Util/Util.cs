﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections;
using System.Globalization;

namespace ForDummies.OpenID.Util
{
    /// <summary>
    /// Classe de utilidades no desenvolvimento de software
    /// </summary>
    /// <remarks>
    /// Essa classe é usada diariamente em todos os sistemas.
    /// Para simplificar seu uso é uma classe statica para que não se tenha necessidade de instacia-la.
    /// </remarks>
    public static class Util
    {

        /// <summary>
        /// Método para uso de mensagens com o usuário
        /// </summary>
        /// <remarks>
        /// Mensagens estilos mensagens do windows
        /// <example>
        /// <code>
        /// //uso do método Alert da classe Util
        /// Util.Alert("Cadastro efetuado com sucesso!", this.Page);
        /// </code>
        /// </example>
        /// </remarks>
        /// <param name="Mensagem">Mensagem a ser enviada para o cliente</param>
        /// <param name="Pagina">Página atual que a mensagem será exibida</param>
        public static void Alert(string Mensagem, Page Pagina)
        {
            Pagina.ClientScript.RegisterStartupScript(Pagina.GetType(), "alert", "<SCRIPT language='javascript' type='text/javascript'>window.alert('" + Mensagem + "');</SCRIPT>");
        }

        /// <summary>
        /// Método para dar mensagem e redirecionar usuário.
        /// </summary>
        /// <code>
        /// Redirecionar(this.Page, "Detalhes.aspx")
        /// </code>
        /// <param name="Pagina"></param>
        public static void Redirecionar(Page Pagina, string Mensagem, string PaginaDestino)
        {
            Pagina.ClientScript.RegisterStartupScript(Pagina.GetType(), "redirect", "<SCRIPT language='javascript' type='text/javascript'>alert(\'" + Mensagem + "\');document.location.href=\'" + PaginaDestino + "\';</SCRIPT>");
        }

        /// <summary>
        /// Método que substitui o \r\n pela tag BR
        /// </summary>
        /// <remarks>
        /// Esse método pode ser usado quando o texto deve ser convertido para HTML
        /// <example>
        /// <code>
        /// //uso do método da classe util IncluiTagBr
        /// txtHtml.Text = Util.IncluiTagBr(txtHtml.Text);
        /// </code>
        /// </example>
        /// </remarks>
        /// <param name="Text">String a ser colocada a tag BR</param>
        /// <returns>Retorna uma String com a tag BR incluida</returns>
        public static string IncluiTagBr(string Text)
        {
            return Text.Replace("\r\n", "<br>");
        }

        /// <summary>
        /// Método que substitui a tag BR por \r\n 
        /// </summary>
        /// <remarks>
        /// Esse método pode ser usado quando o texto deve ser convertido de HTML para Texto normal
        /// <example>
        /// <code>
        /// //uso do método da classe util RemoveTagBr
        /// txtHtml.Text = Util.RemoveTagBr(txtHtml.Text);
        /// </code>
        /// </example>
        /// </remarks>
        /// <param name="Text">String a ser retirada a tag BR</param>
        /// <returns>Retorna uma String sem a tag BR incluida</returns>
        public static string RemoveTagBr(string Text)
        {
            return Text.Replace("<br>", "\r\n");
        }

        /// <summary>
        /// Verifica se o parametro passado com String é um número
        /// </summary>
        /// <remarks>
        /// Esse método faz uso do namespace System.Text.RegularExpressions 
        /// para verificar se realmente a string passada é um número
        /// <example>
        /// <code>
        /// //Uso do Método IsNumber da classe Util
        /// if(Util.IsNumber(Request.QueryString["Codigo"].ToString))
        /// {
        ///     Util.Alert("A QueryString realmente é um número!", this.Page);
        /// }
        /// else
        /// {
        ///     Util.Alert("A QueryString não é um número!", this.Page);
        /// }
        /// </code>
        /// </example>
        /// </remarks>
        /// <param name="text">String a ser verificada</param>
        /// <returns>Retorna True se é um número e false caso contrário</returns>
        public static bool IsNumber(string text)
        {
            if (string.IsNullOrEmpty(text))
                return false;
            else
                return Regex.IsMatch(text, "^[0-9]+$");
        }

        /// <summary>
        /// Retorna true se o arquivo for .doc e se o tamanho for menor que o tamanho máximo
        /// </summary>
        /// <remarks>
        ///   
        /// </remarks>
        /// <param name="postedFile">Arquivo a ser testado</param>
        /// <param name="tamanhoMaximoKB">Tamanho Máximo permitido do arquivo em KB</param>
        /// <returns></returns>
        public static bool TestaArquivoDOC(HttpPostedFile postedFile, int tamanhoMaximoKB)
        {
            bool retorno = false;
            if (postedFile.ContentType == "application/msword" && postedFile.ContentLength <= tamanhoMaximoKB * 1024)
                retorno = true;
            return retorno;
        }



        public static void PageClose(Page Pagina)
        {
            Pagina.ClientScript.RegisterStartupScript(Pagina.GetType(), "close", "<SCRIPT language='javascript'>window.close();</SCRIPT>");
        }

        public static void PopUp(Page paginaAtual, int Altura, int Largura, string Pagina, bool barraRolagem, bool MenuBar)
        {
            string menu = "no";
            string barra = "no";

            if (barraRolagem)
                barra = "yes";

            if (MenuBar)
                menu = "yes";


            paginaAtual.ClientScript.RegisterStartupScript(paginaAtual.GetType(), "popup", "<SCRIPT language='javascript'>if(!window.open('" + Pagina + "', '', 'left=1, top=1, height=" + Altura + ", width=" + Largura + ", menubar=" + menu + ", resizable=no,scrollbars=" + barra + ", toolbar=" + menu + "'))alert('Verifique o seu bloqueador de popup!');</SCRIPT>");

        }

        public static void PopUp(string Nome, Page paginaAtual, int Altura, int Largura, string Pagina, bool barraRolagem, bool MenuBar, bool Resizable)
        {
            string menu = "no";
            string barra = "no";
            string resizable = "no";

            if (barraRolagem)
                barra = "yes";

            if (MenuBar)
                menu = "yes";
            if (Resizable)
                resizable = "yes";

            paginaAtual.ClientScript.RegisterStartupScript(paginaAtual.GetType(), "popup", "<SCRIPT language='javascript'>window.open('" + Pagina + "', '" + Nome + "', 'left=1, top=1, height=" + Altura + ", width=" + Largura + ", menubar=" + menu + ", resizable=" + resizable + ",scrollbars=" + barra + ", toolbar=no');</SCRIPT>");

        }


        public static string base64Encode(string data)
        {
            byte[] encData_byte = new byte[data.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }

        public static string base64Decode(string data)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();

            byte[] todecode_byte = Convert.FromBase64String(data);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }

        public static bool VerificaImagem(String Tipo)
        {
            return (Tipo == "image/jpeg" || Tipo == "image/bmp" || Tipo == "image/x-png" || Tipo == "image/pjpeg" || Tipo == "image/png" || Tipo == "image/gif");
        }

        public static bool VerificaDoc(String Tipo)
        {
            return (Tipo == "application/msword");
        }

        public static bool VerificaFlash(String Tipo)
        {
            return (Tipo == "application/x-shockwave-flash");
        }          

        public static string RequestByGET(string uri)
        {
            return RequestByGET(uri, Encoding.UTF8);
        }

        public static string RequestByGET(string uri, Encoding encode)
        {
            HttpWebResponse response = null;
            StreamReader reader = null;
            try
            {
                //cria requisição
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Timeout = 30000;
                //executa a requisição
                response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                //Exibe os caracteres especiais
                reader = new StreamReader(responseStream, encode);
                //Aloca o buffer
                Char[] buffer = new Char[256];
                int count = reader.Read(buffer, 0, buffer.Length);
                // monta string com o retorno
                StringBuilder retorno = new StringBuilder();
                while (count > 0)
                {
                    retorno.Append(new String(buffer, 0, count));
                    count = reader.Read(buffer, 0, buffer.Length);
                }
                return retorno.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                //Fecho tudo
                if (response != null)
                    response.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        public static string RequestByPOST(string uri, Dictionary<string, string> parametros)
        {
            return RequestByPOST(uri, parametros, Encoding.UTF8);
        }

        public static string RequestByPOST(string uri, Dictionary<string, string> parametros, Encoding encode)
        {
            // Declarações necessárias
            Stream requestStream = null;
            HttpWebResponse response = null;
            StreamReader reader = null;
            try
            {
                //cria requisição
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                //setando o metodo e tempo limite
                request.Method = WebRequestMethods.Http.Post;
                request.Timeout = 30000;
                // Neste ponto, você está setando a propriedade ContentType da página 
                // para urlencoded para que o comando POST seja enviado corretamente 
                request.ContentType = "application/x-www-form-urlencoded";
                //montando os parametros
                StringBuilder urlEncoded = new StringBuilder();
                if (parametros.Count > 0)
                {
                    //percorre por todos paramentros
                    IDictionaryEnumerator idic = parametros.GetEnumerator();
                    while (idic.MoveNext())
                        urlEncoded.Append(idic.Key + "=" + idic.Value + "&");
                    // codificando em UTF8 (evita que sejam mostrados códigos malucos em caracteres especiais 
                    byte[] byteBuffer = encode.GetBytes(urlEncoded.ToString());
                    request.ContentLength = byteBuffer.Length;
                    requestStream = request.GetRequestStream();
                    requestStream.Write(byteBuffer, 0, byteBuffer.Length);
                    requestStream.Close();
                }
                else
                {
                    request.ContentLength = 0;
                }
                // Dados recebidos 
                response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                // Codifica os caracteres especiais para que possam ser exibidos corretamente 
                System.Text.Encoding encoding = encode;
                // Preenche o reader
                reader = new StreamReader(responseStream, encoding);
                Char[] charBuffer = new Char[256];
                int count = reader.Read(charBuffer, 0, charBuffer.Length);
                StringBuilder Dados = new StringBuilder();
                // Lê cada byte para preencher meu stringbuilder 
                while (count > 0)
                {
                    Dados.Append(new String(charBuffer, 0, count));
                    count = reader.Read(charBuffer, 0, charBuffer.Length);
                }
                return Dados.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
            finally
            {
                // Fecha tudo 
                if (requestStream != null)
                    requestStream.Close();
                if (response != null)
                    response.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        public static string GetNomeMes(int mes)
        {
            switch (mes)
            {
                case 1: return "Janeiro";
                case 2: return "Fevereiro";
                case 3: return "Março";
                case 4: return "Abril";
                case 5: return "Maio";
                case 6: return "Junho";
                case 7: return "Julho";
                case 8: return "Agosto";
                case 9: return "Setembro";
                case 10: return "Outubro";
                case 11: return "Novembro";
                case 12: return "Dezembro";
                default: return "Mês inválido";
            }
        }

        public static string getStringSemCortes(string str, int tamanho)
        {
            string saida = string.Empty;
            string[] palavras;
            palavras = str.Split(' ');
            int count;
            for (count = 0; count < palavras.Length; count++)
            {
                if ((palavras[count].Length < (tamanho - 3)) &&
                    (saida.Length + 1 + palavras[count].Length) < (tamanho - 3))
                {
                    saida += " " + palavras[count];
                }
                else
                {
                    saida += "...";
                    break;
                }
            }
            return saida;
        }

        private static DateTime GetLastDayOfMonth(DateTime dtDate)
        {
            DateTime dtTo = dtDate;
            dtTo = dtTo.AddMonths(1);
            dtTo = dtTo.AddDays(-(dtTo.Day));
            dtTo = new DateTime(dtTo.Year, dtTo.Month, dtTo.Day, 23, 59, 59);
            return dtTo;
        }

        private static DateTime? getData(string data)
        {
            //CultureInfo provider = CultureInfo.CreateSpecificCulture("pt-BR");
            CultureInfo provider = CultureInfo.CurrentCulture;
            DateTime? dt = null;
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    dt = DateTime.Parse(data, provider);
                    //dt = DateTime.Parse(data);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return dt;
        }

        public static DateTime? dataFinal(string dt)
        {
            try
            {
                DateTime? dtData = getData(dt);
                if (dt != null)
                    return new DateTime(dtData.Value.Year, dtData.Value.Month, dtData.Value.Day, 23, 59, 59);
                return null;
            }
            catch (Exception ex) { return null; }
        }

        public static DateTime? dataInicial(string dt)
        {
            try
            {
                DateTime? dtData = getData(dt);
                if (dt != null)
                    return new DateTime(dtData.Value.Year, dtData.Value.Month, dtData.Value.Day, 00, 00, 00);
                return null;
            }
            catch (Exception ex) { return null; }
        }

        public static DateTime[] dataPorIntervaloMes(int mes)
        {
            DateTime[] datas = new DateTime[2];

            datas[0] = new DateTime(DateTime.Now.Year, Convert.ToInt32(mes), 1);
            datas[1] = GetLastDayOfMonth(new DateTime(DateTime.Now.Year, Convert.ToInt32(mes), 1));

            return datas;
        }

        public static string GetRandomString(int tamanho)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGH­JKLMNOPQRSTUVWXYZ0123456789";
            Random rNum = new Random();
            string NewPassWord = "";
            for (int i = 0; i < tamanho; i++)
            {
                NewPassWord += allowedChars[rNum.Next(0, allowedChars.Length)];
            }
            return NewPassWord;
        }

        /// <summary>
        /// Retira acentos de palavras
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RetirarAcentuacao(string texto)
        {
            string textor = string.Empty;
            for (int i = 0; i < texto.Length; i++)
            {
                if (texto[i].ToString() == "ã") textor += "a";
                else if (texto[i].ToString() == "á") textor += "a";
                else if (texto[i].ToString() == "à") textor += "a";
                else if (texto[i].ToString() == "â") textor += "a";
                else if (texto[i].ToString() == "ä") textor += "a";
                else if (texto[i].ToString() == "é") textor += "e";
                else if (texto[i].ToString() == "è") textor += "e";
                else if (texto[i].ToString() == "ê") textor += "e";
                else if (texto[i].ToString() == "ë") textor += "e";
                else if (texto[i].ToString() == "í") textor += "i";
                else if (texto[i].ToString() == "ì") textor += "i";
                else if (texto[i].ToString() == "ï") textor += "i";
                else if (texto[i].ToString() == "õ") textor += "o";
                else if (texto[i].ToString() == "ó") textor += "o";
                else if (texto[i].ToString() == "ò") textor += "o";
                else if (texto[i].ToString() == "ö") textor += "o";
                else if (texto[i].ToString() == "ú") textor += "u";
                else if (texto[i].ToString() == "ù") textor += "u";
                else if (texto[i].ToString() == "ü") textor += "u";
                else if (texto[i].ToString() == "ç") textor += "c";
                else if (texto[i].ToString() == "Ã") textor += "A";
                else if (texto[i].ToString() == "Á") textor += "A";
                else if (texto[i].ToString() == "À") textor += "A";
                else if (texto[i].ToString() == "Â") textor += "A";
                else if (texto[i].ToString() == "Ä") textor += "A";
                else if (texto[i].ToString() == "É") textor += "E";
                else if (texto[i].ToString() == "È") textor += "E";
                else if (texto[i].ToString() == "Ê") textor += "E";
                else if (texto[i].ToString() == "Ë") textor += "E";
                else if (texto[i].ToString() == "Í") textor += "I";
                else if (texto[i].ToString() == "Ì") textor += "I";
                else if (texto[i].ToString() == "Ï") textor += "I";
                else if (texto[i].ToString() == "Õ") textor += "O";
                else if (texto[i].ToString() == "Ó") textor += "O";
                else if (texto[i].ToString() == "Ò") textor += "O";
                else if (texto[i].ToString() == "Ö") textor += "O";
                else if (texto[i].ToString() == "Ú") textor += "U";
                else if (texto[i].ToString() == "Ù") textor += "U";
                else if (texto[i].ToString() == "Ü") textor += "U";
                else if (texto[i].ToString() == "Ç") textor += "C";
                else textor += texto[i];
            }
            return textor;
        }


    }
}
